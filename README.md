# Prepare toolchain

1. Install `adb`
  ```bash
  sudo apt install adb
  ```
1. Download and install Android [command-line tools](https://developer.android.com/studio#command-tools)
1. Choose SDK install directory and export it in your `.profile` for future sessions
  ```bash
  echo "export ANDROID_SDK_ROOT=<sdk_install_dir>" >> "$HOME/.profile"
  ```
1. Install Android SDK and NDK using the commands
  ```bash
  echo -n 'y\n' | sdkmanager --sdk_root="$ANDROID_SDK_ROOT" --install 'build-tools;33.0.0'
  ```
1. Clone project
  ```bash
  git clone --recursive https://gitlab.com/matsievskiysv/sencillo.git
  ```
  If you didn't use `--recursive` when cloning, use the following command from the project directory to download submodules
  ```bash
  git submodule update --init --recursive
  ```
1. [Optional] install and set up [Kotlin language server](https://github.com/fwcd/kotlin-language-server)
1. Initial run of './gradlew'] will download [Gradle](https://gradle.org/) and library dependencies
  ```bash
  ./gradlew tasks
  ```

# Build

## Build APK

Issue command to build APK archives
```bash
./gradlew assembleDebug
```

## Build APK and install on the device

There is a shortcut command to build APK and install it onto your device
```bash
./gradlew installDebug
```

## Build documentation

```bash
./gradlew installDebug
```
