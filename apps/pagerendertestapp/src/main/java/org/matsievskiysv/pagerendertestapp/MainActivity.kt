package org.matsievskiysv.pagerenderviewtestapp

import android.Manifest
import android.content.pm.PackageManager
import android.content.Context
import android.content.ContentResolver
import android.view.Menu
import android.view.MenuItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.os.Bundle
import android.app.Dialog

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.delay

import androidx.lifecycle.lifecycleScope
import androidx.activity.result.contract.ActivityResultContracts

import android.widget.Toast

import android.net.Uri
import android.util.Log
import android.util.Size

import java.io.FileInputStream

import org.matsievskiysv.mupdfbackend.MuPdfDocument
import org.matsievskiysv.mupdfbackend.MuPdfReadOnlyStream
import org.matsievskiysv.mupdfbackend.ReferenceOverlay
import org.matsievskiysv.mupdfbackend.ExternalLink
import org.matsievskiysv.mupdfbackend.CrossReference
import org.matsievskiysv.pagerenderview.PageRenderView
import org.matsievskiysv.pagerenderview.OverlayAction
import org.matsievskiysv.pagerenderview.parcel.PageRenderViewParcel
import org.matsievskiysv.pagerenderview.parcel.RectIParcel
import org.matsievskiysv.pagerenderview.interfaces.OverlayItem
import org.matsievskiysv.pagerenderview.geometry.RectI

import org.matsievskiysv.pagerenderviewtestapp.databinding.ViewerBinding
import org.matsievskiysv.pagerenderviewtestapp.databinding.PaddingBinding
import org.matsievskiysv.pagerenderviewtestapp.databinding.NumberBinding

class MainActivity: AppCompatActivity(), CoroutineScope by MainScope() {
    private val logName = "PageRenderTestApp"
    private lateinit var binding: ViewerBinding
    private var uri: Uri? = null
    private var doc: MuPdfDocument? = null
    private var viewState: PageRenderViewParcel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(logName, "onCreate action")
        super.onCreate(savedInstanceState)
        title = getString(R.string.app_name)
        binding = ViewerBinding.inflate(layoutInflater)
        setSupportActionBar(binding.viewerToolbar)
        setContentView(binding.root)
        uri = savedInstanceState?.getParcelable("uri")
        viewState = savedInstanceState?.getParcelable("viewState")
        Log.d(logName, "onCreate action")
        if (uri != null) {
            viewFile(uri)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.viewer_menu_right, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        super.onPrepareOptionsMenu(menu)
        var item = menu.findItem(R.id.viewer_menu_crop)
        item.setChecked(binding.pageRenderView.crop)
        item = menu.findItem(R.id.viewer_menu_left_to_right)
        item.setChecked(binding.pageRenderView.leftToRight)
        item = menu.findItem(R.id.viewer_menu_top_to_bottom)
        item.setChecked(binding.pageRenderView.topToBottom)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.viewer_menu_open -> {
                openFile.launch(arrayOf("application/pdf"))
                true
            }
            R.id.viewer_menu_page -> {
                NumberDialogFragment.newInstance(getString(R.string.page),
                                                 binding.pageRenderView.page + 1).apply {
                    accept = { binding.pageRenderView.page = it - 1 }
                    show(supportFragmentManager, NumberDialogFragment.TAG)
                }
                true
            }
            R.id.viewer_menu_crop -> {
                binding.pageRenderView.crop = !binding.pageRenderView.crop
                true
            }
            R.id.viewer_menu_padding -> {
                PaddingDialogFragment.newInstance(getString(R.string.padding),
                                                  RectIParcel(binding.pageRenderView.padding)).apply {
                    accept = { binding.pageRenderView.padding = it }
                    show(supportFragmentManager, PaddingDialogFragment.TAG)
                }
                true
            }
            R.id.viewer_menu_page_flow_horizontal -> {
                binding.pageRenderView.pageFlow = 0
                true
            }
            R.id.viewer_menu_page_flow_vertical -> {
                binding.pageRenderView.pageFlow = 1
                true
            }
            R.id.viewer_menu_left_to_right -> {
                binding.pageRenderView.leftToRight = !binding.pageRenderView.leftToRight
                true
            }
            R.id.viewer_menu_top_to_bottom -> {
                binding.pageRenderView.topToBottom = !binding.pageRenderView.topToBottom
                true
            }
            R.id.viewer_menu_columns -> {
                NumberDialogFragment.newInstance(getString(R.string.columns),
                                                 binding.pageRenderView.columns).apply {
                    accept = { binding.pageRenderView.columns = it }
                    show(supportFragmentManager, NumberDialogFragment.TAG)
                }
                true
            }
            R.id.viewer_menu_rows -> {
                NumberDialogFragment.newInstance(title = getString(R.string.rows),
                                                 binding.pageRenderView.rows).apply {
                    accept = { binding.pageRenderView.rows = it }
                    show(supportFragmentManager, NumberDialogFragment.TAG)
                }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }

    override fun onSaveInstanceState(outState: Bundle) {
        Log.d(logName, "onSaveInstanceState action")
        super.onSaveInstanceState(outState)
        outState.putParcelable("uri", uri)
        viewState = binding.pageRenderView.writeToParcel()
        outState.putParcelable("viewState", viewState)
    }

    override fun onStop() {
        super.onStop()
    }

    private val requestPermission = registerForActivityResult(
        ActivityResultContracts.RequestPermission()) {
        isGranted ->
            if (!isGranted) {
                Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_LONG).show()
            }
    }

    private val openFile = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        uri: Uri? ->
            this.uri = uri
            viewState = null
            viewFile(uri)
    }

    fun viewFile(uri: Uri?) {
        requestPermission.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {

            val stream = getContentResolver().openInputStream(uri!!) as FileInputStream
            doc = MuPdfDocument(MuPdfReadOnlyStream(stream.getChannel()), "application/pdf")

            val activity = this
            lifecycleScope.launch(Dispatchers.Default) {
                doc?.withBlock {
                    document ->
                    val refs = ReferenceOverlay(R.string.cross_reference).apply {
                        callback = {
                            item, action ->
                                lifecycleScope.launch(Dispatchers.Main) {
                                    when (action) {
                                        OverlayAction.SINGLETAP -> {
                                            binding.pageRenderView.page = (item as CrossReference).pageTo - 1
                                        }
                                        else -> {
                                            Toast.makeText(
                                                activity,
                                                when (action) {
                                                    OverlayAction.DOUBLETAP ->
                                                        "double tap ${(item as CrossReference).pageTo}"
                                                    OverlayAction.LONGPRESS ->
                                                        "long press ${(item as CrossReference).pageTo}"
                                                    else -> ""
                                                },
                                                Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                }
                        }
                    }
                    val links = ReferenceOverlay(R.string.link).apply {
                        callback = {
                            item, action ->
                                lifecycleScope.launch(Dispatchers.Main) {
                                    Toast.makeText(activity,
                                                   when (action) {
                                                       OverlayAction.SINGLETAP -> "single tap ${(item as ExternalLink).uri}"
                                                       OverlayAction.DOUBLETAP -> "double tap ${(item as ExternalLink).uri}"
                                                       OverlayAction.LONGPRESS -> "long press ${(item as ExternalLink).uri}"
                                                   },
                                                   Toast.LENGTH_SHORT).show()
                                }
                        }
                    }
                    for (i in 0 until document.pageNum()) {
                        val page = document.page(i)
                        page.withBlock {
                            Log.d(logName, "get page ${i} links")
                            page.putCrossRefs(refs)
                            page.putExternalLinks(links)
                        }
                    }
                    if (!refs.empty) {
                        Log.i(logName, "add crossref overlay")
                        refs.createActionMap()
                        binding.pageRenderView.addOverlay(500, refs)
                    }
                    if (!links.empty) {
                        links.createActionMap()
                        Log.i(logName, "add external link overlay")
                        binding.pageRenderView.addOverlay(700, links)
                    }
                }
            }

            binding.pageRenderView.apply {
                bitmapCacheSize = 32L * 1024L * 1024L
                lifeCycle = getLifecycle()
                coroutineScope = lifecycleScope
                setDocument(doc!!, viewState)
                onCropAvailable = {
                    lifecycleScope.launch(Dispatchers.Main) {
                        Toast.makeText(activity, R.string.crop_available, Toast.LENGTH_LONG).show()
                    }
                }
                onCropNotAvailable = {
                    lifecycleScope.launch(Dispatchers.Main) {
                        Toast.makeText(activity, R.string.crop_not_available, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    class PaddingDialogFragment : DialogFragment() {
        private lateinit var binding: PaddingBinding
        public var accept: (RectI) -> Unit = {_ -> }
        public var decline: (RectI) -> Unit = {_ -> }

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.padding, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            binding = PaddingBinding.bind(view)
            val title: String = arguments!!.getString("title")!!
            val padding: RectIParcel = arguments!!.getParcelable("padding")!!
            binding.pdLeft.setText("${padding.rect.left}")
            binding.pdTop.setText("${padding.rect.top}")
            binding.pdRight.setText("${padding.rect.right}")
            binding.pdBottom.setText("${padding.rect.bottom}")
            binding.title.setText(title)
            binding.decline.setOnClickListener {
                try {
                    val l = Integer.parseInt(binding.pdLeft.getText().toString())
                    val t = Integer.parseInt(binding.pdTop.getText().toString())
                    val r = Integer.parseInt(binding.pdRight.getText().toString())
                    val b = Integer.parseInt(binding.pdBottom.getText().toString())
                    decline(RectI(l, t, r, b))
                } catch (_: Throwable) {}
                dismiss()
            }
            binding.accept.setOnClickListener {
                try {
                    val l = Integer.parseInt(binding.pdLeft.getText().toString())
                    val t = Integer.parseInt(binding.pdTop.getText().toString())
                    val r = Integer.parseInt(binding.pdRight.getText().toString())
                    val b = Integer.parseInt(binding.pdBottom.getText().toString())
                    accept(RectI(l, t, r, b))
                } catch (_: Throwable) {}
                dismiss()
            }
        }

        override fun onStart() {
            super.onStart()
        }

        companion object {
            const val TAG = "PaddingDialog"

            fun newInstance(title: String, padding: RectIParcel): PaddingDialogFragment {
                val args = Bundle()
                args.putString("title", title)
                args.putParcelable("padding", padding)
                val fragment = PaddingDialogFragment()
                fragment.arguments = args
                return fragment
            }
        }
    }

    class NumberDialogFragment : DialogFragment() {
        private lateinit var binding: NumberBinding
        public var accept: (Int) -> Unit = { _ -> }
        public var decline: (Int) -> Unit = { _ -> }

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.number, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            binding = NumberBinding.bind(view)
            val title = arguments!!.getString("title")
            val number = arguments!!.getInt("number")
            binding.title.setText(title)
            binding.numberInput.setText("${number}")
            binding.decline.setOnClickListener {
                try {
                    val n = Integer.parseInt(binding.numberInput.getText().toString())
                    decline(n)
                } catch (_: Throwable) {}
                dismiss()
            }
            binding.accept.setOnClickListener {
                try {
                    val n = Integer.parseInt(binding.numberInput.getText().toString())
                    accept(n)
                } catch (_: Throwable) {}
                dismiss()
            }
        }

        override fun onStart() {
            super.onStart()
        }

        companion object {
            const val TAG = "NumberDialog"

            fun newInstance(title: String, number: Int): NumberDialogFragment {
                val args = Bundle()
                args.putString("title", title)
                args.putInt("number", number)
                val fragment = NumberDialogFragment()
                fragment.arguments = args
                return fragment
            }
        }
    }
}
