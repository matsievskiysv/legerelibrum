package org.matsievskiysv.sencillo

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup

import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope

import java.io.File

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.flow.collect

import org.matsievskiysv.sencillo.databinding.ViewerBinding
import org.matsievskiysv.sencillo.document.MuPdfDocument
import org.matsievskiysv.pagedview.PagedView

class ViewerActivity: AppCompatActivity(), CoroutineScope by MainScope() {
    private lateinit var binding: ViewerBinding
    private lateinit var uiDb: UiDatabase
    private lateinit var docDb: DocumentDatabase
    private lateinit var uiItem: UiItem
    private lateinit var docItem: DocumentItem
    private lateinit var doc: MuPdfDocument
    private lateinit var pagedView: PagedView
    private lateinit var uri: Uri

    private val requestPermission = registerForActivityResult(
        ActivityResultContracts.RequestPermission()) {
        isGranted ->
            if (!isGranted) {
                finish()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ViewerBinding.inflate(layoutInflater)
        setSupportActionBar(binding.viewerToolbar)

        binding.viewerToolbar.setNavigationOnClickListener {
            when (binding.viewerDrawer.isDrawerOpen(binding.viewerNav)) {
                true -> binding.viewerDrawer.closeDrawer(binding.viewerNav)
                false -> binding.viewerDrawer.openDrawer(binding.viewerNav)
            }
        }

        var loadError = false
        requestPermission.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            AlertDialog.Builder(this).apply {
                setTitle(getString(R.string.cannot_open_document_permission_denied_scoped))
                setPositiveButton(getString(R.string.ok)) { _, _ -> finish() }
            }.show()
            loadError = true
        }

        val intent = getIntent()
        uri = intent.getData()!!

        val path = RealPathUtil.getRealPath(this, uri)
        if ((path == null) or (!File(path!!).canRead())) {
            AlertDialog.Builder(this).apply {
                setTitle(getString(R.string.cannot_open_document_permission_denied))
                setPositiveButton(getString(R.string.ok)) { _, _ -> finish() }
            }.show()
            loadError = true
        }

        if (!loadError) {
            uiDb = UiDatabaseBuilder.getInstance(this)
            docDb = DocDatabaseBuilder.getInstance(this)
            runBlocking {
                launch(Dispatchers.IO) {
                    uiItem = uiDb.uiItemDao().getByPath(path) ?: UiItem(uri.toString(), path)
                    docItem = docDb.documentItemDao().getByPath(path)
                    ?: DocumentItem(uri.toString(), path, ViewerActivity.basename(path).second)
                    docItem.atime = System.currentTimeMillis()
                }
            }
            MainScope().launch(Dispatchers.IO) {
                uiDb.uiItemDao().insertItem(uiItem)
                docDb.documentItemDao().insertItem(docItem)
            }
            val parent =  binding.pagedView.getParent() as ViewGroup
            val index = parent.indexOfChild(binding.pagedView)
            parent.removeView(binding.pagedView)
            pagedView = PagedView(this, null)
            parent.addView(pagedView, index)
            pagedView.apply {
                registerLifecycle(getLifecycle(), lifecycleScope)
                doc = MuPdfDocument(path)
                initialize(doc)
            }
        }

        setContentView(binding.root)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.viewer_menu_right, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.viewer_menu_settings -> {
                val intent = Intent(this, DocSettingsActivity::class.java)
                intent.setData(uri)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch(Dispatchers.IO) {
            doc.withBlock {
                docItem.pages = it.pageNum()
            }
        }
        lifecycleScope.launch(Dispatchers.IO) {
            uiDb.uiItemDao().getByPathFlow(uiItem.path).collect {
                if (it != null) {
                    pagedView.setUiOptions(topToBottom = it.topToBottom,
                                           leftToRight = it.leftToRight)
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        // do not use lifecycleScope here
        // we don't need this job to be killed
        MainScope().launch(Dispatchers.IO) {
            docItem.atime = System.currentTimeMillis()
            docDb.documentItemDao().updateItem(docItem)
        }
    }

    companion object {
        fun basename(str: String): Triple<String, String, String> {
            var folder = str.substringBeforeLast('/')
            if (folder.length == 0) {
                folder = "/"
            }
            val file = str.substringAfterLast('/')
            val basename = file.substringBeforeLast('.')
            var ext = file.substringAfterLast('.')
            if (basename.length == ext.length) {
                ext = ""
            }
            return Triple(folder, basename, ext)
        }
    }
}
