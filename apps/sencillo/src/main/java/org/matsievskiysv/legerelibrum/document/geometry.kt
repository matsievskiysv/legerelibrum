package org.matsievskiysv.sencillo.document

import com.artifex.mupdf.fitz.Point as MuPdfPoint
import com.artifex.mupdf.fitz.Rect as MuPdfRect
import com.artifex.mupdf.fitz.RectI as MuPdfRectI

import org.matsievskiysv.pagedview.geometry.*

fun PointF(other: MuPdfPoint): PointF {
    return PointF(other.x, other.y)
}

fun RectF(other: MuPdfRect): RectF {
    return RectF(other.x0, other.y0, other.x1, other.y1)
}

fun RectF(other: MuPdfRectI): RectI {
    return RectI(other.x0, other.y0, other.x1, other.y1)
}
