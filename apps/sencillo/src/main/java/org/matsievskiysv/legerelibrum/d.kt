// package org.matsievskiysv.sencillo

// import android.content.Context
// import android.os.Bundle
// import androidx.appcompat.app.AppCompatActivity

// import kotlinx.coroutines.CoroutineScope
// import kotlinx.coroutines.MainScope
// import kotlinx.coroutines.launch
// import kotlinx.coroutines.delay

// import androidx.lifecycle.lifecycleScope

// import org.matsievskiysv.sencillo.document.MuPdfDocument
// import org.matsievskiysv.pagedview.PagedView
// import org.matsievskiysv.pagedview.helpers.Logger

// import org.matsievskiysv.sencillo.databinding.ViewerBinding

// class DocListActivity: AppCompatActivity(), CoroutineScope by MainScope() {
//     private lateinit var logger: Logger
//     private lateinit var binding: ViewerBinding

//     override fun onCreate(savedInstanceState: Bundle?) {
//         super.onCreate(savedInstanceState)
//         binding = ViewerBinding.inflate(layoutInflater)
//         val view = binding.root
//         setContentView(view)

// 	    val sharedPref = getSharedPreferences("MAIN", Context.MODE_PRIVATE)
//         with (sharedPref.edit()) {
//             putInt("LOGLEVEL", Logger.VERBOSE)
//             commit()
//         }

// 		logger = Logger(this, "sencillo")
// 		logger.d("setup logger")

//         binding.pagedView.apply {
//             registerLifecycle(getLifecycle(), lifecycleScope)
//             initialize(MuPdfDocument("/sdcard/1.pdf"))
//             moveTo(0, 0.0f, 0.0f)
//         }
//     }

//     // override fun onResume() {
//     //     super.onResume()
//     //     lifecycleScope.launch {
//     //         delay(1000)
//     //         binding.pagedView.moveTo(120)
//     //     }
//     // }

// }
