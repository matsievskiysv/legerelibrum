package org.matsievskiysv.sencillo

// import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.fragment.app.Fragment

import android.util.Log

import androidx.viewpager2.adapter.FragmentStateAdapter

import androidx.core.content.PermissionChecker

// import kotlinx.coroutines.CoroutineScope
// import kotlinx.coroutines.MainScope
// import kotlinx.coroutines.launch
// import kotlinx.coroutines.delay

// import androidx.lifecycle.lifecycleScope

// import org.matsievskiysv.sencillo.document.MuPdfDocument
// import org.matsievskiysv.pagedview.PagedView
// import org.matsievskiysv.pagedview.helpers.Logger

import org.matsievskiysv.sencillo.databinding.WelcomeBinding

class WelcomeActivity: AppCompatActivity() {
    private lateinit var binding: WelcomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val doneStrings = resources.getStringArray(R.array.welcome_done_text)
        binding = WelcomeBinding.inflate(layoutInflater)
        binding.viewPager.adapter = WelcomeCollectionAdapter(
            getSupportFragmentManager(), lifecycle,
            mapOf(
                0 to
                Action(
                    getString(R.string.welcome_read_text),
                    getString(R.string.welcome_read_label),
                    { _ ->
                          Log.e("!!!", "read click ${PermissionChecker.checkSelfPermission(this, "READ_EXTERNAL_STORAGE") == PermissionChecker.PERMISSION_GRANTED}")
                    }
                ),
                1 to
                Action(
                    getString(R.string.welcome_write_text),
                    getString(R.string.welcome_write_label),
                    { _ -> Log.e("!!!", "write click") }),
                2 to
                Action(
                    doneStrings[(0..(doneStrings.size-1)).random()],
                    getString(R.string.welcome_done_label),
                    { _ -> Log.e("!!!", "done click") }),
            )
        )
        setContentView(binding.root)
    }

    // override fun onBackPressed() {
    //     if (parentBinding.viewPager.currentItem == 0) {
    //         // If the user is currently looking at the first step, allow the system to handle the
    //         // Back button. This calls finish() on this activity and pops the back stack.
    //         super.onBackPressed()
    //     } else {
    //         // Otherwise, select the previous step.
    //         parentBinding.viewPager.currentItem = parentBinding.viewPager.currentItem - 1
    //     }
    // }

    data class Action(val text: String, val buttonLabel: String, val action: (Any) -> Unit)

    private class WelcomeCollectionAdapter(fm: FragmentManager, lc: Lifecycle,
                                           val actions: Map<Int, Action>) : FragmentStateAdapter(fm, lc) {
        override fun getItemCount(): Int = actions.size

        override fun createFragment(position: Int): Fragment {
            val fg = WelcomeFragment()
            val a = actions[position]!!
            fg.setArgs(a.text, a.buttonLabel, a.action)
            return fg
        }
    }
}
