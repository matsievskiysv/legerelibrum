package org.matsievskiysv.sencillo

import android.Manifest
import android.app.SearchManager
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup

import android.widget.Filter
import android.widget.Filterable
import android.widget.PopupMenu
import android.widget.SearchView
import android.widget.Toast

import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

import org.matsievskiysv.sencillo.databinding.DoclistBinding
import org.matsievskiysv.sencillo.databinding.FragmentDoclistItemBinding

class DoclistActivity: AppCompatActivity(), CoroutineScope by MainScope() {
    private lateinit var binding: DoclistBinding
    private lateinit var db: DocumentDatabase
    private lateinit var prefs: SharedPreferences

    private val requestPermission = registerForActivityResult(
        ActivityResultContracts.RequestPermission()) {
        isGranted ->
            if (!isGranted) {
                Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_LONG).show()
            }
        }

    private val openFile = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        uri: Uri? -> viewFile(uri)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DoclistBinding.inflate(layoutInflater)
        setSupportActionBar(binding.doclistToolbar)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        db = DocDatabaseBuilder.getInstance(this)
        binding.doclistRecycler.adapter = DoclistRecyclerAdapter(this)
        binding.doclistRecycler.layoutManager = LinearLayoutManager(this)

        setContentView(binding.root)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.doclist_menu_right, menu)
        val searchView = menu.findItem(R.id.doclist_menu_search).getActionView() as SearchView
        searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(query: String): Boolean {
                    val adapter = binding.doclistRecycler.adapter as DoclistRecyclerAdapter
                    adapter.getFilter().filter(query)
                    return true
                }
                override fun onQueryTextSubmit(query: String): Boolean = false
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.doclist_menu_open -> {
                requestPermission.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
                    openFile.launch(arrayOf("application/pdf"))
                }
                true
            }
            R.id.doclist_menu_settings -> {
                val intent = Intent(this, GlobalSettingsActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    fun viewFile(uri: Uri?) {
        if (uri != null) {
            requestPermission.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(this, ViewerActivity::class.java)
                intent.setData(uri)
                startActivity(intent)
            }
        }
    }

    class DoclistRecyclerAdapter(private val dla: DoclistActivity):
        RecyclerView.Adapter<DoclistRecyclerAdapter.DoclistViewHolder>(), Filterable {
        private var items = listOf<DocumentItem>()
        private var itemsFiltered = items

        init {
            dla.lifecycleScope.launch(Dispatchers.IO) {
                dla.db.documentItemDao().getAllFlow().collect{
                    // FIXME: get sorted list from SQL
                    items = it.sortedByDescending { it.atime }
                    // FIXME: apply or reset filter
                    itemsFiltered = items
                    dla.runOnUiThread {
                        notifyDataSetChanged()
                    }
                }
            }
        }

        class DoclistViewHolder(itemView: View,
                                var binding: FragmentDoclistItemBinding) : RecyclerView.ViewHolder(itemView) {
                var title = ""
                    set(value) {
                        field=value
                        binding.title.text=value
                    }
                var pages = ""
                    set(value) {
                        field=value
                        binding.pages.text=value
                    }
                // TODO: add thumbnail
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): DoclistViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.fragment_doclist_item, viewGroup, false)
            val binding = FragmentDoclistItemBinding.bind(view)

            return DoclistViewHolder(view, binding)
        }

        override fun onBindViewHolder(viewHolder: DoclistViewHolder, position: Int) {
            val cItem = itemsFiltered[position]
            viewHolder.title = cItem.name
            viewHolder.pages = String.format(dla.getString(R.string.docview_pages_format), cItem.page, cItem.pages)
            viewHolder.binding.card.setOnClickListener {
                dla.viewFile(Uri.parse(cItem.uri))
            }
            viewHolder.binding.card.setOnLongClickListener {
                val menu = PopupMenu(dla, viewHolder.itemView)
                menu.inflate(R.menu.doclist_menu_dropdown)
                menu.setOnMenuItemClickListener {
                    dla.lifecycleScope.launch(Dispatchers.IO) {
                        val item = dla.db.documentItemDao().getByPath(cItem.path)
                        if (item != null) {
                            dla.db.documentItemDao().deleteItem(item)
                        }
                    }
                    menu.show()
                    true
                }
                true
            }
        }

        override fun getItemCount() = itemsFiltered.size

        override fun getFilter(): Filter {
            return object : Filter() {
                override fun performFiltering(constraint: CharSequence): Filter.FilterResults {
                    return Filter.FilterResults().apply{
                        when (constraint) {
                            "" -> values = items
                            else -> values = items.filter { it.name.contains(constraint, ignoreCase=true) }
                        }
                    }
                }
                override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
                    @Suppress("UNCHECKED_CAST")
                    itemsFiltered = results.values as List<DocumentItem>
                    notifyDataSetChanged()
                }
            }
        }
    }
}
