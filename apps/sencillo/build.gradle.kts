import org.gradle.api.JavaVersion

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android")
}


android {
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)
    ndkVersion = AppConfig.ndkVersion

    kotlinOptions {
        jvmTarget = Versions.jvm
    }

    compileOptions {
        sourceCompatibility = JavaVersion.values().filter { it.toString() == Versions.jvm } .first()
        targetCompatibility = JavaVersion.values().filter { it.toString() == Versions.jvm } .first()
    }

    defaultConfig {
        applicationId = "org.matsievskiysv.sencillo"
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        vectorDrawables.useSupportLibrary = true
        setProperty("archivesBaseName", "sencillo-${AppConfig.versionName}")

        testInstrumentationRunner = AppConfig.androidTestInstrumentation

        kapt {
            arguments {
                arg("room.schemaLocation", "$projectDir/schemas")
            }
        }
    }

    sourceSets {
        getByName("main").jniLibs.srcDirs("libs")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("debug") {
            isMinifyEnabled = false
        }
    }

    lintOptions {
        isAbortOnError = AppConfig.lintAbortOnError
        isWarningsAsErrors = AppConfig.lintWarningsAsErrors
    }

    packagingOptions {
        resources.excludes.add("/META-INF/*")
        resources.excludes.add("app/jni/djvu/djvulibre")
        resources.excludes.add("app/jni/mupdf/mupdf")
    }

    splits {
        abi {
            isEnable = true
            reset()
            include("armeabi-v7a", "arm64-v8a", "x86", "x86_64")
            isUniversalApk = false
        }
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs",
                                  "include" to listOf("*.jar"))))
    // project libs
    implementation(project(":libraries:pagedview"))
    implementation(project(":libraries:mupdfbackend"))
    //app libs
    kapt(AppDependencies.appKapt)
    implementation(AppDependencies.sharedLibraries)
    implementation(AppDependencies.appLibraries)
    //test libs
    testImplementation(AppDependencies.testLibraries)
    androidTestImplementation(AppDependencies.androidTestLibraries)
}
