package org.matsievskiysv.pagerenderview

import kotlin.math.*

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.*
import io.kotest.matchers.booleans.*
import io.kotest.matchers.ints.*
import io.kotest.matchers.floats.*
import io.kotest.matchers.comparables.*
import io.kotest.property.checkAll
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import io.kotest.assertions.throwables.*

import org.matsievskiysv.pagerenderview.geometry.*


class GeometryTest : StringSpec({

    "clipnum" {
        checkAll<Int, Int, Int> {
            a, b, c ->
                if (b <= c) {
                    clipnum(a, b, c).shouldBeBetween(b, c)
                } else {
                    shouldThrow<IllegalArgumentException> {
                        clipnum(a, b, c)
                    }
                }
        }
    }
    "PointI" {
        val pointArb = arbitrary { rs ->
            val a = Arb.int(-1000, 1000).next(rs)
            var b = Arb.int(-1000, 1000).next(rs)
            PointI(a, b)
        }

        val rectArb = arbitrary { rs ->
            val a = Arb.int(-1000, 1000).next(rs)
            var b = Arb.int(-1000, 1000).next(rs)
            while(b == a) {
                b = Arb.int(-1000, 1000).next(rs)
            }
            val c = Arb.int(-1000, 1000).next(rs)
            var d = Arb.int(-1000, 1000).next(rs)
            while(c == d) {
                d = Arb.int(-1000, 1000).next(rs)
            }
            val (l, r) = listOf(a, b).sorted()
            val (t, bb) = listOf(c, d).sorted()
            RectI(l, t, r, bb)
        }

        checkAll(pointArb) {
            p -> p shouldBe p
        }
        checkAll(pointArb, pointArb, pointArb) {
            p1, p2, p3 ->
                if (p2.x <= p3.x && p2.y <= p3.y) {
                    val (first, second) = p1.clip(p2, p3)
                    first.shouldBeBetween(p2.x, p3.x)
                    second.shouldBeBetween(p2.y, p3.y)
                } else {
                    shouldThrow<IllegalArgumentException> {
                        p1.clip(p2, p3)
                    }
                }
        }
        checkAll(pointArb, Arb.int()) {
            p, x ->
                p.map { it + x } shouldBe PointI(p.x + x, p.y + x)
                p.map { it * x } shouldBe PointI(p.x * x, p.y * x)
        }
        checkAll(pointArb, Arb.int()) {
            p0, x ->
                var p = PointI(p0)
                p.iMap { it + x }
                p shouldBe PointI(p0.x + x, p0.y + x)
                p = PointI(p0)
                p.iMap { it * x }
                p shouldBe PointI(p0.x * x, p0.y * x)
        }
        checkAll(pointArb, pointArb) {
            p1, p2 ->
                p1 + p2 shouldBe PointI(p1.x + p2.x, p1.y + p2.y)
                p1 - p2 shouldBe PointI(p1.x - p2.x, p1.y - p2.y)
                p1 * p2 shouldBe PointI(p1.x * p2.x, p1.y * p2.y)
                if (p2.x == 0 || p2.y == 0) {
                    shouldThrow<ArithmeticException> {
                        p1 / p2
                    }
                } else {
                    p1 / p2 shouldBe PointI(p1.x / p2.x, p1.y / p2.y)
                }
                if (p2.x == 0 || p2.y == 0) {
                    shouldThrow<ArithmeticException> {
                        p1 % p2
                    }
                } else {
                    p1 % p2 shouldBe PointI(p1.x % p2.x, p1.y % p2.y)
                }
        }
        checkAll(pointArb, pointArb) {
            p1, p2 ->
                var p = PointI(p1)
                p.plusAssign(p2)
                p shouldBe PointI(p1.x + p2.x, p1.y + p2.y)
                p = PointI(p1)
                p.minusAssign(p2)
                p shouldBe PointI(p1.x - p2.x, p1.y - p2.y)
                p = PointI(p1)
                p.timesAssign(p2)
                p shouldBe PointI(p1.x * p2.x, p1.y * p2.y)
                p = PointI(p1)
                if (p2.x == 0 || p2.y == 0) {
                    shouldThrow<ArithmeticException> {
                        p.divAssign(p2)
                    }
                } else {
                    p.divAssign(p2)
                    p shouldBe PointI(p1.x / p2.x, p1.y / p2.y)
                }
                p = PointI(p1)
                if (p2.x == 0 || p2.y == 0) {
                    shouldThrow<ArithmeticException> {
                        p.remAssign(p2)
                    }
                } else {
                    p.remAssign(p2)
                    p shouldBe PointI(p1.x % p2.x, p1.y % p2.y)
                }
        }
        checkAll(pointArb, rectArb) {
            p, r ->
                if (p.isInside(r)) {
                    (r.left <= p.x && p.x <= r.right).shouldBeTrue()
                    (r.top <= p.y && p.y <= r.bottom).shouldBeTrue()
                } else {
                    (r.left > p.x || p.x > r.right || r.top > p.y || p.y > r.bottom).shouldBeTrue()
                }
        }
        checkAll(pointArb, pointArb) {
            p1, p2 ->
                p1 min p2 shouldBe PointI(min(p1.x, p2.x), min(p1.y, p2.y))
                p1 max p2 shouldBe PointI(max(p1.x, p2.x), max(p1.y, p2.y))
        }
    }
    "PointF" {
        val pointArb = arbitrary { rs ->
            val a = Arb.numericFloats(-1000f, 1000f).next(rs)
            var b = Arb.numericFloats(-1000f, 1000f).next(rs)
            PointF(a, b)
        }

        val rectArb = arbitrary { rs ->
            val a = Arb.numericFloats(-1000f, 1000f).next(rs)
            var b = Arb.numericFloats(-1000f, 1000f).next(rs)
            while(b == a) {
                b = Arb.numericFloats(-1000f, 1000f).next(rs)
            }
            val c = Arb.numericFloats(-1000f, 1000f).next(rs)
            var d = Arb.numericFloats(-1000f, 1000f).next(rs)
            while(c == d) {
                d = Arb.numericFloats(-1000f, 1000f).next(rs)
            }
            val (l, r) = listOf(a, b).sorted()
            val (t, bb) = listOf(c, d).sorted()
            RectF(l, t, r, bb)
        }

        checkAll(pointArb) {
            p -> p shouldBe p
        }
        checkAll(pointArb, pointArb, pointArb) {
            p1, p2, p3 ->
                if (p2.x <= p3.x && p2.y <= p3.y) {
                    val (first, second) = p1.clip(p2, p3)
                    p2.x shouldBeLessThanOrEqualTo first
                    first shouldBeLessThanOrEqualTo p3.x
                    p2.y shouldBeLessThanOrEqualTo second
                    second shouldBeLessThanOrEqualTo p3.y
                } else {
                    shouldThrow<IllegalArgumentException> {
                        p1.clip(p2, p3)
                    }
                }
        }
        checkAll(pointArb, Arb.numericFloats(-1000f, 1000f)) {
            p, x ->
                p.map { it + x } shouldBe PointF(p.x + x, p.y + x)
                p.map { it * x } shouldBe PointF(p.x * x, p.y * x)
        }
        checkAll(pointArb, Arb.numericFloats(-1000f, 1000f)) {
            p0, x ->
                var p = PointF(p0)
                p.iMap { it + x }
                p shouldBe PointF(p0.x + x, p0.y + x)
                p = PointF(p0)
                p.iMap { it * x }
                p shouldBe PointF(p0.x * x, p0.y * x)
        }
        checkAll(pointArb, pointArb) {
            p1, p2 ->
                p1 + p2 shouldBe PointF(p1.x + p2.x, p1.y + p2.y)
                p1 - p2 shouldBe PointF(p1.x - p2.x, p1.y - p2.y)
                p1 * p2 shouldBe PointF(p1.x * p2.x, p1.y * p2.y)
                p1 / p2 shouldBe PointF(p1.x / p2.x, p1.y / p2.y)
                p1 % p2 shouldBe PointF(p1.x % p2.x, p1.y % p2.y)
        }
        checkAll(pointArb, pointArb) {
            p1, p2 ->
                var p = PointF(p1)
                p.plusAssign(p2)
                p shouldBe PointF(p1.x + p2.x, p1.y + p2.y)
                p = PointF(p1)
                p.minusAssign(p2)
                p shouldBe PointF(p1.x - p2.x, p1.y - p2.y)
                p = PointF(p1)
                p.timesAssign(p2)
                p shouldBe PointF(p1.x * p2.x, p1.y * p2.y)
                p = PointF(p1)
                p.divAssign(p2)
                p shouldBe PointF(p1.x / p2.x, p1.y / p2.y)
                p = PointF(p1)
                p.remAssign(p2)
                p shouldBe PointF(p1.x % p2.x, p1.y % p2.y)
        }
        checkAll(pointArb, rectArb) {
            p, r ->
                if (p.isInside(r)) {
                    (r.left <= p.x && p.x <= r.right).shouldBeTrue()
                    (r.top <= p.y && p.y <= r.bottom).shouldBeTrue()
                } else {
                    (r.left > p.x || p.x > r.right || r.top > p.y || p.y > r.bottom).shouldBeTrue()
                }
        }
        checkAll(pointArb, pointArb) {
            p1, p2 ->
                p1 min p2 shouldBe PointF(min(p1.x, p2.x), min(p1.y, p2.y))
                p1 max p2 shouldBe PointF(max(p1.x, p2.x), max(p1.y, p2.y))
        }
        checkAll(pointArb) {
            p ->
                val (l1, r1) = p.round()
                l1 shouldBe (round(p.x) plusOrMinus abs(p.x)/1000)
                r1 shouldBe (round(p.y) plusOrMinus abs(p.y)/1000)
                val (l2, r2) = p.ceil()
                l2 shouldBe (ceil(p.x) plusOrMinus abs(p.x)/1000)
                r2 shouldBe (ceil(p.y) plusOrMinus abs(p.y)/1000)
                val (l3, r3) = p.floor()
                l3 shouldBe (floor(p.x) plusOrMinus abs(p.x)/1000)
                r3 shouldBe (floor(p.y) plusOrMinus abs(p.y)/1000)
        }
        checkAll(pointArb) {
            p ->
                var p1 = PointF(p)
                p1.iRound()
                p1.x shouldBe (round(p.x) plusOrMinus abs(p.x)/1000)
                p1.y shouldBe (round(p.y) plusOrMinus abs(p.y)/1000)
                p1 = PointF(p)
                p1.iCeil()
                p1.x shouldBe (ceil(p.x) plusOrMinus abs(p.x)/1000)
                p1.y shouldBe (ceil(p.y) plusOrMinus abs(p.y)/1000)
                p1 = PointF(p)
                p1.iFloor()
                p1.x shouldBe (floor(p.x) plusOrMinus abs(p.x)/1000)
                p1.y shouldBe (floor(p.y) plusOrMinus abs(p.y)/1000)
        }
    }
    "RectI" {
        val pointArb = arbitrary { rs ->
            val a = Arb.int(-1000, 1000).next(rs)
            var b = Arb.int(-1000, 1000).next(rs)
            PointI(a, b)
        }

        val rectArb = arbitrary { rs ->
            val a = Arb.int(-1000, 1000).next(rs)
            var b = Arb.int(-1000, 1000).next(rs)
            while(b == a) {
                b = Arb.int(-1000, 1000).next(rs)
            }
            val c = Arb.int(-1000, 1000).next(rs)
            var d = Arb.int(-1000, 1000).next(rs)
            while(c == d) {
                d = Arb.int(-1000, 1000).next(rs)
            }
            val (l, r) = listOf(a, b).sorted()
            val (t, bb) = listOf(c, d).sorted()
            RectI(l, t, r, bb)
        }

        checkAll(rectArb) {
            r ->
                r shouldBe r
                r.topLeft() shouldBe PointI(r.left, r.top)
                r.topRight() shouldBe PointI(r.right, r.top)
                r.bottomLeft() shouldBe PointI(r.left, r.bottom)
                r.bottomRight() shouldBe PointI(r.right, r.bottom)
        }
        checkAll(rectArb, Arb.int()) {
            r, x ->
                r + x shouldBe RectI(r.left + x, r.top + x, r.right + x, r.bottom + x)
                r - x shouldBe RectI(r.left - x, r.top - x, r.right - x, r.bottom - x)
                r * x shouldBe RectI(r.left * x, r.top * x, r.right * x, r.bottom * x)
                if (x == 0) {
                    shouldThrow<ArithmeticException> {
                        r / x
                    }
                    shouldThrow<ArithmeticException> {
                        r % x
                    }
                } else {
                    r / x shouldBe RectI(r.left / x, r.top / x, r.right / x, r.bottom / x)
                    r % x shouldBe RectI(r.left % x, r.top % x, r.right % x, r.bottom % x)
                }
        }
        checkAll(rectArb, Arb.int()) {
            r, x ->
                var rr = RectI(r)
                rr.plusAssign(x)
                rr shouldBe RectI(r.left + x, r.top + x, r.right + x, r.bottom + x)
                rr = RectI(r)
                rr.minusAssign(x)
                rr shouldBe RectI(r.left - x, r.top - x, r.right - x, r.bottom - x)
                rr = RectI(r)
                rr.timesAssign(x)
                rr shouldBe RectI(r.left * x, r.top * x, r.right * x, r.bottom * x)
                rr = RectI(r)
                if (x == 0) {
                    shouldThrow<ArithmeticException> {
                        rr.divAssign(x)
                    }
                } else {
                    rr.divAssign(x)
                    rr shouldBe RectI(r.left / x, r.top / x, r.right / x, r.bottom / x)
                }
                rr = RectI(r)
                if (x == 0) {
                    shouldThrow<ArithmeticException> {
                        rr.remAssign(x)
                    }
                } else {
                    rr.remAssign(x)
                    rr shouldBe RectI(r.left % x, r.top % x, r.right % x, r.bottom % x)
                }
        }
        checkAll(rectArb, pointArb) {
            r, p -> r + p shouldBe RectI(r.left + p.x, r.top + p.y, r.right + p.x, r.bottom + p.y)
        }
        checkAll(rectArb, rectArb) {
            r1, r2 -> r1.merge(r2) shouldBe RectI(min(r1.left, r2.left), min(r1.top, r2.top),
                                                  max(r1.right, r2.right), max(r1.bottom, r2.bottom))
        }
        checkAll(Arb.int(), Arb.int(), Arb.int(), Arb.int()) {
            a, b, c, d ->
                val (l, r) = listOf(a, b).sorted()
                val (t, bb) = listOf(c, d).sorted()
                RectI(a, c, b, d).sort() shouldBe RectI(l, t, r, bb)
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                try {
                    val (l, t, r, b) = r1.moveInto(r2)
                    l.shouldBeBetween(r2.left, r2.right)
                    t.shouldBeBetween(r2.top, r2.bottom)
                    r.shouldBeBetween(r2.left, r2.right)
                    b.shouldBeBetween(r2.top, r2.bottom)
                } catch(e: IllegalArgumentException) {
                    (r1.width() > r2.width() || r1.height() > r2.height()).shouldBeTrue()
                }
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                if (r1.overlap(r2)) {
                    val (l, t, r, b) = r1.intersect(r2)
                    l shouldBe max(r1.left, r2.left)
                    t shouldBe max(r1.top, r2.top)
                    r shouldBe min(r1.right, r2.right)
                    b shouldBe min(r1.bottom, r2.bottom)
		}
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                if (r1.isInside(r2)) {
                    r1.overlap(r2).shouldBeTrue()
                }
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                if (!r1.overlap(r2)) {
                    (r1.left > r2.right || r1.top > r2.bottom ||
                     r1.right < r2.left || r1.bottom < r2.top).shouldBeTrue()
                }
        }
    }
    "RectF" {
        val pointArb = arbitrary { rs ->
            val a = Arb.numericFloats(-1000f, 1000f).next(rs)
            var b = Arb.numericFloats(-1000f, 1000f).next(rs)
            PointF(a, b)
        }

        val rectArb = arbitrary { rs ->
            val a = Arb.numericFloats(-1000f, 1000f).next(rs)
            var b = Arb.numericFloats(-1000f, 1000f).next(rs)
            while(b == a) {
                b = Arb.numericFloats(-1000f, 1000f).next(rs)
            }
            val c = Arb.numericFloats(-1000f, 1000f).next(rs)
            var d = Arb.numericFloats(-1000f, 1000f).next(rs)
            while(c == d) {
                d = Arb.numericFloats(-1000f, 1000f).next(rs)
            }
            val (l, r) = listOf(a, b).sorted()
            val (t, bb) = listOf(c, d).sorted()
            RectF(l, t, r, bb)
        }

        checkAll(rectArb) {
            r ->
                r shouldBe r
                r.topLeft() shouldBe PointF(r.left, r.top)
                r.topRight() shouldBe PointF(r.right, r.top)
                r.bottomLeft() shouldBe PointF(r.left, r.bottom)
                r.bottomRight() shouldBe PointF(r.right, r.bottom)
        }
        checkAll(rectArb, Arb.numericFloats(-1000f, 1000f)) {
            r, x ->
                r + x shouldBe RectF(r.left + x, r.top + x, r.right + x, r.bottom + x)
                r - x shouldBe RectF(r.left - x, r.top - x, r.right - x, r.bottom - x)
                r * x shouldBe RectF(r.left * x, r.top * x, r.right * x, r.bottom * x)
                r / x shouldBe RectF(r.left / x, r.top / x, r.right / x, r.bottom / x)
                if (x != 0f) {                                        // FIXME: properly compare NaNs
                    r % x shouldBe RectF(r.left % x, r.top % x, r.right % x, r.bottom % x)
                }
        }
        checkAll(rectArb, Arb.numericFloats(-1000f, 1000f)) {
            r, x ->
                var rr = RectF(r)
                rr.plusAssign(x)
                rr shouldBe RectF(r.left + x, r.top + x, r.right + x, r.bottom + x)
                rr = RectF(r)
                rr.minusAssign(x)
                rr shouldBe RectF(r.left - x, r.top - x, r.right - x, r.bottom - x)
                rr = RectF(r)
                rr.timesAssign(x)
                rr shouldBe RectF(r.left * x, r.top * x, r.right * x, r.bottom * x)
                rr = RectF(r)
                rr.divAssign(x)
                rr shouldBe RectF(r.left / x, r.top / x, r.right / x, r.bottom / x)
                rr = RectF(r)
                if (x != 0f) {                                        // FIXME: properly compare NaNs
                    rr.remAssign(x)
                    rr shouldBe RectF(r.left % x, r.top % x, r.right % x, r.bottom % x)
                }
        }
        checkAll(rectArb, pointArb) {
            r, p -> r + p shouldBe RectF(r.left + p.x, r.top + p.y, r.right + p.x, r.bottom + p.y)
        }
        checkAll(rectArb, rectArb) {
            r1, r2 -> r1.merge(r2) shouldBe RectF(min(r1.left, r2.left), min(r1.top, r2.top),
                                                  max(r1.right, r2.right), max(r1.bottom, r2.bottom))
        }
        checkAll(Arb.numericFloats(-1000f, 1000f), Arb.numericFloats(-1000f, 1000f),
                 Arb.numericFloats(-1000f, 1000f), Arb.numericFloats(-1000f, 1000f)) {
            a, b, c, d ->
                val (l, r) = listOf(a, b).sorted()
                val (t, bb) = listOf(c, d).sorted()
                RectF(a, c, b, d).sort() shouldBe RectF(l, t, r, bb)
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                try {
                    val (l, t, r, b) = r1.moveInto(r2)
                    r2.left shouldBeLessThanOrEqual l
                    l shouldBeLessThanOrEqual r2.right
                    r2.top shouldBeLessThanOrEqual t
                    t shouldBeLessThanOrEqual r2.bottom
                    r2.left shouldBeLessThanOrEqual r
                    r shouldBeLessThanOrEqual r2.right
                    r2.top shouldBeLessThanOrEqual b
                    b shouldBeLessThanOrEqual r2.bottom
                } catch(e: IllegalArgumentException) {
                    (r1.width() > r2.width() || r1.height() > r2.height()).shouldBeTrue()
                }
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                if (r1.overlap(r2)) {
                    val (l, t, r, b) = r1.intersect(r2)
                    l shouldBe max(r1.left, r2.left)
                    t shouldBe max(r1.top, r2.top)
                    r shouldBe min(r1.right, r2.right)
                    b shouldBe min(r1.bottom, r2.bottom)
		}
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                if (r1.isInside(r2)) {
                    r1.overlap(r2).shouldBeTrue()
                }
        }
        checkAll(rectArb, rectArb) {
            r1, r2 ->
                if (!r1.overlap(r2)) {
                    (r1.left > r2.right || r1.top > r2.bottom ||
                     r1.right < r2.left || r1.bottom < r2.top).shouldBeTrue()
                }
        }
    }
})
