package org.matsievskiysv.pagerenderview

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.*
import io.kotest.matchers.booleans.*
import io.kotest.matchers.ints.*
import io.kotest.matchers.floats.*
import io.kotest.matchers.comparables.*
import io.kotest.property.checkAll
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import io.kotest.assertions.throwables.*

import org.matsievskiysv.pagerenderview.helpers.Grid

fun pseudo_random(i: Int, j: Int): Int? {
    if ((i or j) == (i xor j)) {
        return null
    } else {
        return (i * j) and ((0b11111111 and i) xor (0b11111111 and j))
    }
}


// TODO: replace with property tests where possible
class GridTest : StringSpec({
    "numeric" {
        val gridArb = arbitrary { rs ->
            val a = Arb.int(1, 1000).next(rs)
            val b = Arb.int(1, 1000).next(rs)
            val g = Grid<Int?>(a, b)
            for (i in 0 until a) {
                for (j in 0 until b) {
                    g[i, j] = pseudo_random(i, j)
                }
            }
            g
        }

        checkAll(gridArb) {
            grid ->
                val (x, y) = grid.dims
                grid.length shouldBe x * y
        }
    }
})
