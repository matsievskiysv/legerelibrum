package org.matsievskiysv.pagerenderview

import kotlin.math.*

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.*
import io.kotest.matchers.booleans.*
import io.kotest.matchers.ints.*
import io.kotest.matchers.floats.*
import io.kotest.matchers.comparables.*
import io.kotest.property.checkAll
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import io.kotest.assertions.throwables.*

import org.matsievskiysv.pagerenderview.helpers.CoordConvert
import org.matsievskiysv.pagerenderview.geometry.RectI


class CoordConvertTest : StringSpec({

    "Initialize" {
        val rectArb = arbitrary { rs ->
            val a = Arb.int(-1000, 1000).next(rs)
            var b = Arb.int(-1000, 1000).next(rs)
            while(b == a) {
                b = Arb.int(-1000, 1000).next(rs)
            }
            val c = Arb.int(-1000, 1000).next(rs)
            var d = Arb.int(-1000, 1000).next(rs)
            while(c == d) {
                d = Arb.int(-1000, 1000).next(rs)
            }
            val (l, r) = listOf(a, b).sorted()
            val (t, bb) = listOf(c, d).sorted()
            RectI(l, t, r, bb)
        }

        checkAll(rectArb, Arb.numericFloats(-1f, 1000f)) {
            rect, scale ->
                val cc = CoordConvert()
                if (scale <= 0) {
                    shouldThrow<IllegalArgumentException> {
                        cc.setOriginal(rect, scale)
                    }
                } else {
                    cc.setOriginal(rect, scale)
                    cc.originalRect.left shouldBe rect.left
                    cc.originalRect.top shouldBe rect.top
                    cc.originalRect.right shouldBe rect.right
                    cc.originalRect.bottom shouldBe rect.bottom
                    cc.scaledRect.left shouldBe round(rect.left * scale).toInt()
                    cc.scaledRect.top shouldBe round(rect.top * scale).toInt()
                    cc.scaledRect.right shouldBe round(rect.right * scale).toInt()
                    cc.scaledRect.bottom shouldBe round(rect.bottom * scale).toInt()
                }
        }

        checkAll(rectArb, Arb.numericFloats(-1f, 1000f)) {
            rect, scale ->
                val cc = CoordConvert()
                if (scale <= 0) {
                    shouldThrow<IllegalArgumentException> {
                        cc.setScaled(rect, scale)
                    }
                } else {
                    cc.setScaled(rect, scale)
                    cc.scaledRect.left shouldBe rect.left
                    cc.scaledRect.top shouldBe rect.top
                    cc.scaledRect.right shouldBe rect.right
                    cc.scaledRect.bottom shouldBe rect.bottom
                    cc.originalRect.left shouldBe round(rect.left / scale).toInt()
                    cc.originalRect.top shouldBe round(rect.top / scale).toInt()
                    cc.originalRect.right shouldBe round(rect.right / scale).toInt()
                    cc.originalRect.bottom shouldBe round(rect.bottom / scale).toInt()
                }
        }
    }
})
