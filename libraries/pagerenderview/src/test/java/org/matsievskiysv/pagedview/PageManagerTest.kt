// package org.matsievskiysv.pagerenderview

// import io.kotest.core.spec.style.FunSpec
// import io.kotest.matchers.shouldBe
// import org.matsievskiysv.pagerenderview.geometry.*

// class PageManagerTest : FunSpec({
//     // TODO: replace with property tests where possible
//     context("same size without tiles without crop") {
//         var m = PageManager(8) { PointI(100, 100) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         var tiles = mutableListOf<PageManager.PageTile>()

//         test("without padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 100, 100)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 120, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 99, 101)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 3)
//             m.visibleRect shouldBe RectI(0, 0, 100, 200)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 101, 101)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(0, 0, 200, 200)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 250, 250)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 8
//             val list = tiles.sortedBy { it.page }
//             list.map { it.page } shouldBe (0..7).toList()
//             list[0].position shouldBe RectI(0, 0, 100, 100)
//             list[1].position shouldBe RectI(100, 0, 200, 100)
//             list[2].position shouldBe RectI(200, 0, 300, 100)
//             list[3].position shouldBe RectI(0, 100, 100, 200)
//             list[4].position shouldBe RectI(100, 100, 200, 200)
//             list[5].position shouldBe RectI(200, 100, 300, 200)
//             list[6].position shouldBe RectI(0, 200, 100, 300)
//             list[7].position shouldBe RectI(100, 200, 200, 300)
//             m.visibleRect shouldBe RectI(0, 0, 300, 300)
//             tiles.clear()

//             m.viewPort = RectI(250, 250, 300, 300)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }

//         m = PageManager(8) { PointI(100, 100) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         m.padding = RectI(5, 10, 15, 20)
//         test("with padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(5, 10, 105, 110)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(5, 10, 105, 110)
//             m.visibleRect shouldBe RectI(0, 0, 120, 130)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 100, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(5, 10, 105, 110)
//             m.visibleRect shouldBe RectI(0, 0, 120, 130)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 120, 130)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(5, 10, 105, 110)
//             m.visibleRect shouldBe RectI(0, 0, 120, 130)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 130, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1)
//             m.visibleRect shouldBe RectI(0, 0, 240, 130)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 119, 131)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 3)
//             m.visibleRect shouldBe RectI(0, 0, 120, 260)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 121, 131)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(0, 0, 240, 260)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 300, 300)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 8
//             val list = tiles.sortedBy { it.page }
//             list[0].position shouldBe RectI(5, 10, 105, 110)
//             list[1].position shouldBe RectI(125, 10, 225, 110)
//             list[2].position shouldBe RectI(245, 10, 345, 110)
//             list[3].position shouldBe RectI(5, 140, 105, 240)
//             list[4].position shouldBe RectI(125, 140, 225, 240)
//             list[5].position shouldBe RectI(245, 140, 345, 240)
//             list[6].position shouldBe RectI(5, 270, 105, 370)
//             list[7].position shouldBe RectI(125, 270, 225, 370)
//             list.map { it.page } shouldBe (0..7).toList()
//             m.visibleRect shouldBe RectI(0, 0, 360, 390)
//             tiles.clear()

//             m.viewPort = RectI(300, 300, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }
//     }

//     context("different size without tiles without crop") {
//         var m = PageManager(8) { PointI(100 + it * 10, 100 + it * 10) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         var tiles = mutableListOf<PageManager.PageTile>()

//         test("without padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(30, 10, 130, 110)
//             m.visibleRect shouldBe RectI(0, 0, 160, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(30, 10, 130, 110)
//             m.visibleRect shouldBe RectI(0, 0, 160, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 160, 120)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(30, 10, 130, 110)
//             m.visibleRect shouldBe RectI(0, 0, 160, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 170, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1)
//             m.visibleRect shouldBe RectI(0, 0, 330, 120)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 159, 121)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 3)
//             m.visibleRect shouldBe RectI(0, 0, 160, 270)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 161, 121)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(0, 0, 330, 270)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 450, 420)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 8
//             val list = tiles.sortedBy { it.page }
//             list.map { it.page } shouldBe (0..7).toList()
//             list[0].position shouldBe RectI(30, 10, 130, 110)
//             list[1].position shouldBe RectI(190, 5, 300, 115)
//             list[2].position shouldBe RectI(345, 0, 465, 120)
//             list[3].position shouldBe RectI(15, 130, 145, 260)
//             list[4].position shouldBe RectI(175, 125, 315, 265)
//             list[5].position shouldBe RectI(330, 120, 480, 270)
//             list[6].position shouldBe RectI(0, 275, 160, 435)
//             list[7].position shouldBe RectI(160, 270, 330, 440)
//             m.visibleRect shouldBe RectI(0, 0, 480, 440)
//             tiles.clear()

//             m.viewPort = RectI(350, 350, 400, 400)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }

//         m = PageManager(8) { PointI(100 + it * 10, 100 + it * 10) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         m.padding = RectI(10, 20, 30, 40)

//         test("with padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(40, 30, 140, 130)
//             m.visibleRect shouldBe RectI(0, 0, 200, 180)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(40, 30, 140, 130)
//             m.visibleRect shouldBe RectI(0, 0, 200, 180)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 200, 180)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(40, 30, 140, 130)
//             m.visibleRect shouldBe RectI(0, 0, 200, 180)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 210, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1)
//             m.visibleRect shouldBe RectI(0, 0, 410, 180)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 159, 181)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 3)
//             m.visibleRect shouldBe RectI(0, 0, 200, 390)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 201, 181)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(0, 0, 410, 390)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 450, 420)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 8
//             var list = tiles.sortedBy { it.page }
//             list.map { it.page } shouldBe (0..7).toList()
//             list[0].position shouldBe RectI(40, 30, 140, 130)
//             list[1].position shouldBe RectI(240, 25, 350, 135)
//             list[2].position shouldBe RectI(435, 20, 555, 140)
//             list[3].position shouldBe RectI(25, 210, 155, 340)
//             list[4].position shouldBe RectI(225, 205, 365, 345)
//             list[5].position shouldBe RectI(420, 200, 570, 350)
//             list[6].position shouldBe RectI(10, 415, 170, 575)
//             list[7].position shouldBe RectI(210, 410, 380, 580)
//             m.visibleRect shouldBe RectI(0, 0, 600, 620)
//             tiles.clear()

//             m.viewPort = RectI(420, 420, 500, 500)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }
//     }

//     context("same size with tiles without crop") {
//         var m = PageManager(8) { PointI(300, 300) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.tiles = PointI(3, 3)
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         var tiles = mutableListOf<PageManager.PageTile>()

//         test("without padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 99, 99)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 100, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 110, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 99, 100)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 100, 200)
//             tiles.clear()

//             m.viewPort = RectI(250, 250, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(200, 200, 400, 400)
//             tiles.clear()

//             m.viewPort = RectI(150, 250, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 6
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0, 1, 3, 3, 4)
//             m.visibleRect shouldBe RectI(100, 200, 400, 400)
//             tiles.clear()

//             m.viewPort = RectI(450, 450, 750, 750)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 12
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(
//                 4, 4, 4, 4,
//                 5, 5, 5, 5,
//                 7, 7, 7, 7
//             )
//             m.visibleRect shouldBe RectI(400, 400, 800, 800)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 850, 850)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 72
//             val list = tiles.sortedWith {
//                 x, y ->
//                 if (x.page != y.page) {
//                     x.page - y.page
//                 } else {
//                     if (x.tile.y == x.tile.y) {
//                         x.tile.y - x.tile.y
//                     } else {
//                         if (x.tile.x == x.tile.x) {
//                             x.tile.x - x.tile.x
//                         } else {
//                             0
//                         }
//                     }
//                 }
//             }
//             for (r in 0..2) {
//                 for (c in 0..2) {
//                     if (r != 2 && c != 2) {
//                         for (t in 0..8) {
//                             val i = r * 3 * 9 + c * 9 + t
//                             val tile = PointI(t % 3, t / 3)
//                             list[i].page shouldBe r * 3 + c
//                             list[i].tile shouldBe tile
//                             list[i].position shouldBe RectI(
//                                 c * 300 + tile.x * 100,
//                                 r * 300 + tile.y * 100,
//                                 c * 300 + tile.x * 100 + 100,
//                                 r * 300 + tile.y * 100 + 100
//                             )
//                         }
//                     }
//                 }
//             }
//             m.visibleRect shouldBe RectI(0, 0, 900, 900)
//             tiles.clear()

//             m.viewPort = RectI(800, 800, 900, 900)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }

//         m = PageManager(8) { PointI(260, 240) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.tiles = PointI(3, 3)
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         m.padding = RectI(10, 20, 30, 40)
//         test("with padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(10, 20, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(10, 20, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 99, 99)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(10, 20, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 100, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 110, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 99, 100)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 100, 200)
//             tiles.clear()

//             m.viewPort = RectI(250, 250, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(200, 200, 400, 400)
//             tiles.clear()

//             m.viewPort = RectI(150, 250, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 6
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0, 1, 3, 3, 4)
//             m.visibleRect shouldBe RectI(100, 200, 400, 400)
//             tiles.clear()

//             m.viewPort = RectI(450, 450, 750, 750)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 12
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(
//                 4, 4, 4, 4,
//                 5, 5, 5, 5,
//                 7, 7, 7, 7
//             )
//             m.visibleRect shouldBe RectI(400, 400, 800, 800)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 850, 850)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 72
//             val list = tiles.sortedWith {
//                 x, y ->
//                 if (x.page != y.page) {
//                     x.page - y.page
//                 } else {
//                     if (x.tile.y == x.tile.y) {
//                         x.tile.y - x.tile.y
//                     } else {
//                         if (x.tile.x == x.tile.x) {
//                             x.tile.x - x.tile.x
//                         } else {
//                             0
//                         }
//                     }
//                 }
//             }
//             for (r in 0..2) {
//                 for (c in 0..2) {
//                     if (!(r == 2 && c == 2)) {
//                         for (t in 0..8) {
//                             val i = r * 3 * 9 + c * 9 + t
//                             val tile = PointI(t % 3, t / 3)
//                             list[i].page shouldBe r * 3 + c
//                             list[i].tile shouldBe tile
//                             val left = if (tile.x == 0) 10 else 0
//                             val top = if (tile.y == 0) 20 else 0
//                             val right = if (tile.x == 2) 30 else 0
//                             val bottom = if (tile.y == 2) 40 else 0
//                             list[i].position shouldBe RectI(
//                                 c * 300 + tile.x * 100 + left,
//                                 r * 300 + tile.y * 100 + top,
//                                 c * 300 + tile.x * 100 + 100 - right,
//                                 r * 300 + tile.y * 100 + 100 - bottom
//                             )
//                         }
//                     }
//                 }
//             }
//             m.visibleRect shouldBe RectI(0, 0, 900, 900)
//             tiles.clear()

//             m.viewPort = RectI(800, 800, 900, 900)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }
//     }

//     context("different size with tiles without crop") {
//         var m = PageManager(8) { PointI(300 - it * 40, 300 - it * 40) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.tiles = PointI(3, 3)
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         var tiles = mutableListOf<PageManager.PageTile>()

//         test("without padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 99, 99)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(0, 0, 100, 100)
//             m.visibleRect shouldBe RectI(0, 0, 100, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 100, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 110, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 200, 100)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 99, 100)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 100, 200)
//             tiles.clear()

//             m.viewPort = RectI(250, 250, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1)
//             m.visibleRect shouldBe RectI(200, 200, 386, 300)
//             tiles.clear()

//             m.viewPort = RectI(150, 250, 350, 350)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 5
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0, 1, 3, 3)
//             m.visibleRect shouldBe RectI(100, 200, 386, 360)
//             tiles.clear()

//             m.viewPort = RectI(450, 450, 750, 750)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 5
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(4, 4, 7, 7, 7)
//             m.visibleRect shouldBe RectI(386, 420, 560, 540)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 850, 850)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 72
//             val list = tiles.sortedWith {
//                 x, y ->
//                 if (x.page != y.page) {
//                     x.page - y.page
//                 } else {
//                     if (x.tile.y == x.tile.y) {
//                         x.tile.y - x.tile.y
//                     } else {
//                         if (x.tile.x == x.tile.x) {
//                             x.tile.x - x.tile.x
//                         } else {
//                             0
//                         }
//                     }
//                 }
//             }
//             for (r in 0..2) {
//                 for (c in 0..2) {
//                     if (!(r == 2 && c == 2)) {
//                         for (t in 0..8) {
//                             val i = r * 3 * 9 + c * 9 + t
//                             if (c == 0 && r == 0 && t == 0) {
//                                 list[i].position shouldBe RectI(0, 0, 100, 100)
//                             }
//                             if (c == 0 && r == 0 && t == 6) {
//                                 list[i].position shouldBe RectI(0, 200, 100, 300)
//                             }
//                             if (c == 1 && r == 1 && t == 5) {
//                                 list[i].position shouldBe RectI(473, 360, 500, 420)
//                             }
//                             if (c == 2 && r == 1 && t == 4) {
//                                 list[i].position shouldBe RectI(633, 360, 706, 420)
//                             }
//                             if (c == 0 && r == 2 && t == 7) {
//                                 list[i].position shouldBe RectI(120, 520, 180, 540)
//                             }
//                         }
//                     }
//                 }
//             }
//             m.visibleRect shouldBe RectI(0, 0, 780, 540)
//             tiles.clear()

//             m.viewPort = RectI(800, 800, 900, 900)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }

//         m = PageManager(8) { PointI(300 - it * 40, 300 - it * 40) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.tiles = PointI(3, 3)
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         m.padding = RectI(10, 20, 30, 40)
//         test("with padding") {
//             m.viewPort = RectI(0, 0, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(10, 20, 113, 120)
//             m.visibleRect shouldBe RectI(0, 0, 113, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 50, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(10, 20, 113, 120)
//             m.visibleRect shouldBe RectI(0, 0, 113, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 99, 99)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 1
//             tiles[0].page shouldBe 0
//             tiles[0].position shouldBe RectI(10, 20, 113, 120)
//             m.visibleRect shouldBe RectI(0, 0, 113, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 130, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 226, 120)
//             tiles.clear()

//             m.viewPort = RectI(-50, -50, 150, 50)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 226, 120)
//             tiles.clear()

//             m.viewPort = RectI(0, 0, 99, 120)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 2
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0)
//             m.visibleRect shouldBe RectI(0, 0, 113, 240)
//             tiles.clear()

//             m.viewPort = RectI(250, 250, 410, 420)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 4
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 1, 3, 4)
//             m.visibleRect shouldBe RectI(226, 240, 440, 440)
//             tiles.clear()

//             m.viewPort = RectI(150, 250, 420, 410)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 6
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(0, 0, 1, 3, 3, 4)
//             m.visibleRect shouldBe RectI(113, 240, 440, 440)
//             tiles.clear()

//             m.viewPort = RectI(450, 450, 750, 750)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 17
//             tiles.map { it.page }.sortedBy { it } shouldBe listOf(
//                 4, 4, 4, 4,
//                 5, 5, 5, 5,
//                 7, 7, 7, 7,
//                 7, 7, 7, 7,
//                 7
//             )
//             m.visibleRect shouldBe RectI(340, 440, 813, 720)
//             tiles.clear()

//             m.viewPort = RectI(50, 50, 850, 850)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 72
//             val list = tiles.sortedWith {
//                 x, y ->
//                 if (x.page != y.page) {
//                     x.page - y.page
//                 } else {
//                     if (x.tile.y == x.tile.y) {
//                         x.tile.y - x.tile.y
//                     } else {
//                         if (x.tile.x == x.tile.x) {
//                             x.tile.x - x.tile.x
//                         } else {
//                             0
//                         }
//                     }
//                 }
//             }
//             for (r in 0..2) {
//                 for (c in 0..2) {
//                     if (!(r == 2 && c == 2)) {
//                         for (t in 0..8) {
//                             val i = r * 3 * 9 + c * 9 + t
//                             if (c == 0 && r == 0 && t == 0) {
//                                 list[i].position shouldBe RectI(10, 20, 113, 120)
//                             }
//                             if (c == 0 && r == 0 && t == 6) {
//                                 list[i].position shouldBe RectI(10, 240, 113, 320)
//                             }
//                             if (c == 1 && r == 1 && t == 5) {
//                                 list[i].position shouldBe RectI(540, 440, 550, 520)
//                             }
//                             if (c == 2 && r == 1 && t == 4) {
//                                 list[i].position shouldBe RectI(726, 440, 810, 520)
//                             }
//                             if (c == 0 && r == 2 && t == 7) {
//                                 list[i].position shouldBe RectI(130, 680, 190, 680)
//                             }
//                         }
//                     }
//                 }
//             }
//             m.visibleRect shouldBe RectI(0, 0, 900, 720)
//             tiles.clear()

//             m.viewPort = RectI(800, 800, 900, 900)
//             m.calculate()
//             m.pageTiles.forEach { tiles.add(it) }
//             tiles.count() shouldBe 0
//             m.visibleRect shouldBe RectI(0, 0, 0, 0)
//             tiles.clear()
//         }
//     }

//     context("move to page") {
//         var m = PageManager(8) { PointI(100, 100) }
//         m.columns = 3
//         m.rows = 3
//         m.leftToRight = true
//         m.topToBottom = true
//         m.pageFlow = Direction.HORIZONTAL
//         m.gridDirection = Direction.VERTICAL
//         var tiles = mutableListOf<PageManager.PageTile>()

//         m.viewPort = RectI(0, 0, 100, 100)
//         m.calculate()

//         m.moveToPage(0)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(0)
//         m.visibleRect shouldBe RectI(0, 0, 100, 100)
//         tiles.clear()

//         m.moveToPage(1)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(1)
//         m.visibleRect shouldBe RectI(100, 0, 200, 100)
//         tiles.clear()

//         m.moveToPage(1, PointF(0.5f, 0.5f))
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 4
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(1, 2, 4, 5)
//         m.visibleRect shouldBe RectI(100, 0, 300, 200)
//         tiles.clear()

//         m.moveToPage(5)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(5)
//         m.visibleRect shouldBe RectI(200, 100, 300, 200)
//         tiles.clear()

//         m.moveToPage(6)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(6)
//         m.visibleRect shouldBe RectI(0, 200, 100, 300)
//         tiles.clear()

//         m.leftToRight = true
//         m.topToBottom = false

//         m.moveToPage(0)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(0)
//         m.visibleRect shouldBe RectI(0, 200, 100, 300)
//         tiles.clear()

//         m.moveToPage(1)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(1)
//         m.visibleRect shouldBe RectI(100, 200, 200, 300)
//         tiles.clear()

//         m.moveToPage(4, PointF(0.5f, 0.5f))
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 4
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(1, 2, 4, 5)
//         m.visibleRect shouldBe RectI(100, 100, 300, 300)
//         tiles.clear()

//         m.moveToPage(5)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(5)
//         m.visibleRect shouldBe RectI(200, 100, 300, 200)
//         tiles.clear()

//         m.moveToPage(6)
//         m.calculate()
//         m.pageTiles.forEach { tiles.add(it) }
//         tiles.count() shouldBe 1
//         tiles.map { it.page }.sortedBy { it } shouldBe listOf(6)
//         m.visibleRect shouldBe RectI(0, 0, 100, 100)
//         tiles.clear()
//     }
// })
