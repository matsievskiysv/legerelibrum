package org.matsievskiysv.pagerenderview.geometry

import kotlin.math.*

/**
 * Integer rectangle
 *
 * Inplace method versions are prepended with letter «i»
 *
 * @constructor Create new rectangle
 * @property left Left boundary coordinate
 * @property top Top boundary coordinate
 * @property right Right boundary coordinate
 * @property bottom Bottom boundary coordinate
 * @param left Left boundary coordinate
 * @param top Top boundary coordinate
 * @param right Right boundary coordinate
 * @param bottom Bottom boundary coordinate
 */
public class RectI(var left: Int, var top: Int, var right: Int, var bottom: Int) {
    /**
     * Create rectangle with all zeros
     */
    constructor() : this(0, 0, 0, 0)
    /**
     * Copy rectangle
     * @param other Other rectangle
     */
    constructor(other: RectI) : this(other.left, other.top, other.right, other.bottom)
    /**
     * Copy rectangle
     * @param other Other rectangle
     */
    constructor(other: RectF) : this(
        other.left.toInt(),
        other.top.toInt(),
        other.right.toInt(),
        other.bottom.toInt()
    )
    /**
     * Get string representation
     * @return String representation
     */
    override fun toString(): String {
        return "RectI[$left;$top;$right;$bottom]"
    }
    /**
     * Compare rectangles
     * @param other Other rectangle
     * @return Comparison result
     */
    override operator fun equals(other: Any?): Boolean {
        return when (other) {
            is RectI -> left == other.left && top == other.top && right == other.right && bottom == other.bottom
            else -> super.equals(other)
        }
    }
    /**
     * Return left coordinate
     * @return Left coordinate
     */
    operator fun component1(): Int = left
    /**
     * Return top coordinate
     * @return Top coordinate
     */
    operator fun component2(): Int = top
    /**
     * Return right coordinate
     * @return Right coordinate
     */
    operator fun component3(): Int = right
    /**
     * Return bottom coordinate
     * @return Bottom coordinate
     */
    operator fun component4(): Int = bottom
    /**
     * Set coordinates inplace
     * @param first Left coordinate
     * @param top Top coordinate
     * @param right Right coordinate
     * @param bottom Bottom coordinate
     * @return This rectangle
     */
    fun iSet(first: Int, second: Int, third: Int, forth: Int): RectI =
        this.apply {
            left = first
            top = second
            right = third
            bottom = forth
        }
    /**
     * Set coordinates inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iSet(other: RectI): RectI =
        this.apply {
            left = other.left
            top = other.top
            right = other.right
            bottom = other.bottom
        }
    /**
     * Apply function to coordinates
     * @param func Function to apply
     * @return New rectangle
     */
    fun map(func: (Int) -> Int): RectI = RectI(
        func(left),
        func(top),
        func(right),
        func(bottom)
    )
    /**
     * Apply function to coordinates inplace
     * @param func Function to apply
     * @return This rectangle
     */
    fun iMap(func: (Int) -> Int): RectI = iSet(
        func(left),
        func(top),
        func(right),
        func(bottom)
    )

    // {{{ plus
    /**
     * Add rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun plus(other: Int): RectI =
        RectI(
            left + other,
            top + other,
            right + other,
            bottom + other
        )
    /**
     * Add rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun plus(other: Float): RectF =
        RectF(
            left + other,
            top + other,
            right + other,
            bottom + other
        )
    /**
     * Add rectangles inplace
     * @param other Scalar value
     */
    operator fun plusAssign(other: Int) {
        left += other
        top += other
        right += other
        bottom += other
    }
    /**
     * Add rectangles inplace
     * @param other Scalar value
     */
    operator fun plus(other: RectI): RectI =
        RectI(
            left + other.left,
            top + other.top,
            right + other.right,
            bottom + other.bottom
        )
    /**
     * Add rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun plus(other: RectF): RectF =
        RectF(
            left + other.left,
            top + other.top,
            right + other.right,
            bottom + other.bottom
        )
    /**
     * Add rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun plus(other: PointI): RectI =
        RectI(
            left + other.x,
            top + other.y,
            right + other.x,
            bottom + other.y
        )
    /**
     * Add rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun plus(other: PointF): RectF =
        RectF(
            left + other.x,
            top + other.y,
            right + other.x,
            bottom + other.y
        )
    /**
     * Add rectangles inplace
     * @param other Other rectangle
     */
    operator fun plusAssign(other: PointI) {
        left += other.x
        top += other.y
        right += other.x
        bottom += other.y
    }
    // }}}

    // {{{ minus
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Int): RectI =
        RectI(
            left - other,
            top - other,
            right - other,
            bottom - other
        )
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Float): RectF =
        RectF(
            left - other,
            top - other,
            right - other,
            bottom - other
        )
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: Int) {
        left -= other
        top -= other
        right -= other
        bottom -= other
    }
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: RectI): RectI =
        RectI(
            left - other.left,
            top - other.top,
            right - other.right,
            bottom - other.bottom
        )
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: RectF): RectF =
        RectF(
            left - other.left,
            top - other.top,
            right - other.right,
            bottom - other.bottom
        )
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: RectI) {
        left -= other.left
        top -= other.top
        right -= other.right
        bottom -= other.bottom
    }
    /**
     * Subtract rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun minus(other: PointI): RectI =
        RectI(
            left - other.x,
            top - other.y,
            right - other.x,
            bottom - other.y
        )
    /**
     * Subtract rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun minus(other: PointF): RectF =
        RectF(
            left - other.x,
            top - other.y,
            right - other.x,
            bottom - other.y
        )
    /**
     * Subtract rectangles inplace
     * @param other Other rectangle
     */
    operator fun minusAssign(other: PointI) {
        left -= other.x
        top -= other.y
        right -= other.x
        bottom -= other.y
    }
    // }}}

    // {{{ times number
    /**
     * Multiply rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun times(other: Int): RectI =
        RectI(
            left * other,
            top * other,
            right * other,
            bottom * other
        )
    /**
     * Multiply rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun times(other: Float): RectF =
        RectF(
            left * other,
            top * other,
            right * other,
            bottom * other
        )
    /**
     * Multiply rectangles inplace
     * @param other Scalar value
     */
    operator fun timesAssign(other: Int) {
        left *= other
        top *= other
        right *= other
        bottom *= other
    }
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun times(other: RectI): RectI =
        RectI(
            left * other.left,
            top * other.top,
            right * other.right,
            bottom * other.bottom
        )
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun times(other: RectF): RectF =
        RectF(
            left * other.left,
            top * other.top,
            right * other.right,
            bottom * other.bottom
        )
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun timesAssign(other: RectI) {
        left *= other.left
        top *= other.top
        right *= other.right
        bottom *= other.bottom
    }
    /**
     * Multiply rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun times(other: PointI): RectI =
        RectI(
            left * other.x,
            top * other.y,
            right * other.x,
            bottom * other.y
        )
    /**
     * Multiply rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun times(other: PointF): RectF =
        RectF(
            left * other.x,
            top * other.y,
            right * other.x,
            bottom * other.y
        )
    /**
     * Multiply points inplace
     * @param other Other point
     */
    operator fun timesAssign(other: PointI) {
        left *= other.x
        top *= other.y
        right *= other.x
        bottom *= other.y
    }
    // }}}

    // {{{ div
    /**
     * Divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun div(other: Int): RectI =
        RectI(
            left / other,
            top / other,
            right / other,
            bottom / other
        )
    /**
     * Divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun div(other: Float): RectF =
        RectF(
            left / other,
            top / other,
            right / other,
            bottom / other
        )
    /**
     * Divide rectangles inplace
     * @param other Scalar value
     */
    operator fun divAssign(other: Int) {
        left /= other
        top /= other
        right /= other
        bottom /= other
    }
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun div(other: RectI): RectI =
        RectI(
            left / other.left,
            top / other.top,
            right / other.right,
            bottom / other.bottom
        )
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun div(other: RectF): RectF =
        RectF(
            left / other.left,
            top / other.top,
            right / other.right,
            bottom / other.bottom
        )
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun divAssign(other: RectI) {
        left /= other.left
        top /= other.top
        right /= other.right
        bottom /= other.bottom
    }
    /**
     * Divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun div(other: PointI): RectI =
        RectI(
            left / other.x,
            top / other.y,
            right / other.x,
            bottom / other.y
        )
    /**
     * Divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun div(other: PointF): RectF =
        RectF(
            left / other.x,
            top / other.y,
            right / other.x,
            bottom / other.y
        )
    /**
     * Divide points inplace
     * @param other Other point
     */
    operator fun divAssign(other: PointI) {
        left /= other.x
        top /= other.y
        right /= other.x
        bottom /= other.y
    }
    // }}}

    // {{{ rem
    /**
     * Whole divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun rem(other: Int): RectI =
        RectI(
            left % other,
            top % other,
            right % other,
            bottom % other
        )
    /**
     * Whole divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun rem(other: Float): RectF =
        RectF(
            left % other,
            top % other,
            right % other,
            bottom % other
        )
    /**
     * Whole divide rectangles inplace
     * @param other Scalar value
     */
    operator fun remAssign(other: Int) {
        left %= other
        top %= other
        right %= other
        bottom %= other
    }
    /**
     * Whole divide rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
     operator fun rem(other: RectI): RectI =
        RectI(
            left % other.left,
            top % other.top,
            right % other.right,
            bottom % other.bottom
        )
    /**
     * Whole divide rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
     operator fun rem(other: RectF): RectF =
        RectF(
            left % other.left,
            top % other.top,
            right % other.right,
            bottom % other.bottom
        )
    /**
     * Whole divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun remAssign(other: RectI) {
        left %= other.left
        top %= other.top
        right %= other.right
        bottom %= other.bottom
    }
    /**
     * Whole divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun rem(other: PointI): RectI =
        RectI(
            left % other.x,
            top % other.y,
            right % other.x,
            bottom % other.y
        )
    /**
     * Whole divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun rem(other: PointF): RectF =
        RectF(
            left % other.x,
            top % other.y,
            right % other.x,
            bottom % other.y
        )
    /**
     * Whole divide rectangles inplace
     * @param other Other point
     */
    operator fun remAssign(other: PointI) {
        left %= other.x
        top %= other.y
        right %= other.x
        bottom %= other.y
    }
    // }}}

    // {{{ min
    /**
     * Get minimum coordinates of new rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    fun min(other: RectI): RectI =
        RectI(
            min(left, other.left),
            min(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    /**
     * Get minimum coordinates of two rectangles inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iMin(other: RectI): RectI =
        iSet(
            min(left, other.left),
            min(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    // }}}

    // {{{ max
    /**
     * Get maximum coordinates of two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    fun max(other: RectI): RectI =
        RectI(
            max(left, other.left),
            max(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    /**
     * Get maximum coordinates of two rectangles inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iMax(other: RectI): RectI =
        iSet(
            max(left, other.left),
            max(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    // }}}

    // {{{ merge
    /**
     * Merge two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    infix fun merge(other: RectI): RectI =
        RectI(
            min(left, other.left),
            min(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    /**
     * Merge two rectangles inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iMerge(other: RectI): RectI =
        iSet(
            min(left, other.left),
            min(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    /**
     * Merge two other rectangles in this
     * @param first First rectangle
     * @param second Second rectangle
     * @return This rectangle
     */
    fun mergeInto(first: RectI, second: RectI): RectI =
        iSet(
            min(first.left, second.left),
            min(first.top, second.top),
            max(first.right, second.right),
            max(first.bottom, second.bottom)
        )
    // }}}

    // {{{ intersect
    /**
     * Intersect two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    infix fun intersect(other: RectI): RectI =
        RectI(
            max(left, other.left),
            max(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    /**
     * Intersect two rectangles inplace
     * @param other Other rectangle
     * @return New rectangle
     */
    fun iIntersect(other: RectI): RectI =
        iSet(
            max(left, other.left),
            max(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    /**
     * Intersect two other rectangles in this
     * @param first First rectangle
     * @param second Second rectangle
     * @return This rectangle
     */
    fun intersectInto(first: RectI, second: RectI): RectI =
        iSet(
            max(first.left, second.left),
            max(first.top, second.top),
            min(first.right, second.right),
            min(first.bottom, second.bottom)
        )
    // }}}

    // {{{ sort
    /**
     * Sort rectangle coordinates
     * @return New rectangle
     */
    fun sort(): RectI =
        RectI(
            min(left, right),
            min(top, bottom),
            max(left, right),
            max(top, bottom)
        )
    /**
     * Sort rectangle coordinates inplace
     * @return This rectangle
     */
    fun iSort(): RectI =
        iSet(
            min(left, right),
            min(top, bottom),
            max(left, right),
            max(top, bottom)
        )
    // }}}

    // {{{ width/height
    /**
     * Rectangle width
     * @return width
     */
    fun width(): Int = (right - left)
    /**
     * Rectangle height
     * @return height
     */
    fun height(): Int = (bottom - top)
    // }}}

    // {{{ fit
    /**
     * Move rectangle into other horizontally
     * @param other Other rectangle
     * @return New rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun moveIntoHorizontal(other: RectI): RectI {
        val width = width()
        if (width > other.width()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var left = left
        var right = right
        if (left < other.left) {
            left = other.left
            right = left + width
        } else if (right > other.right) {
            right = other.right
            left = right - width
        }
        return RectI(left, top, right, bottom)
    }
    /**
     * Move rectangle into other horizontally inplace
     * @param other Other rectangle
     * @return This rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun iMoveIntoHorizontal(other: RectI): RectI {
        val width = width()
        if (width > other.width()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var left = left
        var right = right
        if (left < other.left) {
            left = other.left
            right = left + width
        } else if (right > other.right) {
            right = other.right
            left = right - width
        }
        return iSet(left, top, right, bottom)
    }
    /**
     * Move rectangle into other vertically
     * @param other Other rectangle
     * @return New rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun moveIntoVertical(other: RectI): RectI {
        val height = height()
        if (height > other.height()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var top = top
        var bottom = bottom
        if (top < other.top) {
            top = other.top
            bottom = top + height
        } else if (bottom > other.bottom) {
            bottom = other.bottom
            top = bottom - height
        }
        return RectI(left, top, right, bottom)
    }
    /**
     * Move rectangle into other vertically inplace
     * @param other Other rectangle
     * @return This rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun iMoveIntoVertical(other: RectI): RectI {
        val height = height()
        if (height > other.height()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var top = top
        var bottom = bottom
        if (top < other.top) {
            top = other.top
            bottom = top + height
        } else if (bottom > other.bottom) {
            bottom = other.bottom
            top = bottom - height
        }
        return iSet(left, top, right, bottom)
    }
    /**
     * Move rectangle into other
     * @param other Other rectangle
     * @return New rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    fun moveInto(other: RectI): RectI = moveIntoHorizontal(other).iMoveIntoVertical(other)
    /**
     * Move rectangle into other inplace
     * @param other Other rectangle
     * @return This rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    fun iMoveInto(other: RectI): RectI = iMoveIntoHorizontal(other).iMoveIntoVertical(other)
    // }}}

    // {{{ isInside
    /**
     * Test if rectangle inside other
     * @param other Other rectangle
     * @return Result
     */
    infix fun isInside(other: RectI): Boolean =
        left >= other.left && top >= other.top && right <= other.right && bottom <= other.bottom
    /**
     * Test if rectangle inside other
     * @param other Other rectangle
     * @return Result
     */
    infix fun isInside(other: RectF): Boolean =
        left >= other.left && top >= other.top && right <= other.right && bottom <= other.bottom
    // }}}

    // {{{ points
    /**
     * Get rectangle point
     * @return Point
     */
    fun topLeft(): PointI = PointI(left, top)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun topLeftInto(other: PointI): PointI = other.iSet(left, top)
    /**
     * Get rectangle point
     * @return Point
     */
    fun top(): PointI = PointI((right + left)/2, top)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun topInto(other: PointI): PointI = other.iSet((right + left)/2, top)
    /**
     * Get rectangle point
     * @return Point
     */
    fun topRight(): PointI = PointI(right, top)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun topRightInto(other: PointI): PointI = other.iSet(right, top)
    /**
     * Get rectangle point
     * @return Point
     */
    fun right(): PointI = PointI(right, (bottom + top)/2)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun rightInto(other: PointI): PointI = other.iSet(right, (bottom + top)/2)
    /**
     * Get rectangle point
     * @return Point
     */
    fun bottomLeft(): PointI = PointI(left, bottom)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun bottomLeftInto(other: PointI): PointI = other.iSet(left, bottom)
    /**
     * Get rectangle point
     * @return Point
     */
    fun bottom(): PointI = PointI((right + left)/2, bottom)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun bottomInto(other: PointI): PointI = other.iSet((right + left)/2, bottom)
    /**
     * Get rectangle point
     * @return Point
     */
    fun bottomRight(): PointI = PointI(right, bottom)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun bottomRightInto(other: PointI): PointI = other.iSet(right, bottom)
    /**
     * Get rectangle point
     * @return Point
     */
    fun left(): PointI = PointI(left, (bottom + top)/2)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun leftInto(other: PointI): PointI = other.iSet(left, (bottom + top)/2)
    /**
     * Get rectangle point
     * @return Point
     */
    fun center(): PointI = PointI((left + right)/2, (bottom + top)/2)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun centerInto(other: PointI): PointI = other.iSet((left + right)/2, (bottom + top)/2)
    /**
     * Get rectangle point
     * @param selector Corner selector
     * @return Point
     */
    fun point(selector: RectSide): PointI = when (selector) {
        RectSide.TOPLEFT -> topLeft()
        RectSide.TOP -> top()
        RectSide.TOPRIGHT -> topRight()
        RectSide.RIGHT -> right()
        RectSide.BOTTOMRIGHT -> bottomRight()
        RectSide.BOTTOM -> bottom()
        RectSide.BOTTOMLEFT -> bottomLeft()
        RectSide.LEFT -> left()
        RectSide.CENTER -> center()
    }
    /**
     * Get rectangle point in other
     * @param selector Corner selector
     * @param other Other point
     * @return Other point
     */
    fun pointInto(selector: RectSide, other: PointI): PointI = when (selector) {
        RectSide.TOPLEFT -> topLeftInto(other)
        RectSide.TOP -> topInto(other)
        RectSide.TOPRIGHT -> topRightInto(other)
        RectSide.RIGHT -> rightInto(other)
        RectSide.BOTTOMRIGHT -> bottomRightInto(other)
        RectSide.BOTTOM -> bottomInto(other)
        RectSide.BOTTOMLEFT -> bottomLeftInto(other)
        RectSide.LEFT -> leftInto(other)
        RectSide.CENTER -> centerInto(other)
    }
    // }}}

    // {{{ overlap
    /**
     * Check if rectangles overlap
     * @param other Other rectangle
     * @return Result
     */
    infix fun overlap(other: RectI): Boolean =
        !(
            (left < other.left && right < other.left) ||
                (top < other.top && bottom < other.top) ||
                (left > other.right && right > other.right) ||
                (top > other.bottom && bottom > other.bottom)
            )
    /**
     * Check if rectangles overlap
     * @param other Other rectangle
     * @return Result
     */
    infix fun overlap(other: RectF): Boolean =
        !(
            (left < other.left && right < other.left) ||
                (top < other.top && bottom < other.top) ||
                (left > other.right && right > other.right) ||
                (top > other.bottom && bottom > other.bottom)
            )
    // }}}
}
