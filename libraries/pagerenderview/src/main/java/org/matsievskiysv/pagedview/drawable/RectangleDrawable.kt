package org.matsievskiysv.pagerenderview.drawable

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint

import android.util.Log

import org.matsievskiysv.pagerenderview.geometry.*
import org.matsievskiysv.pagerenderview.interfaces.OverlayDrawable
import org.matsievskiysv.pagerenderview.interfaces.OverlayItem

/**
 * Abstract class for overlay draw object
 * @property pageSize Size of page
 * @property area Drawable area
 * @property canvas Draw canvas
 * @param canvas Draw canvas
 * @property paint Draw paint
 * @param paint Draw paint
 */
class RectangleDrawable(pageSize: PointI, area: RectI,
                        public val canvas: Canvas, public val paint: Paint): OverlayDrawable(pageSize, area) {
    /**
     * Draw rectangle on page
     * @param canvas Drawing canvas
     * @param paint Drawing paint
     * @param bitmap Drawing bitmap
     * @param cropLeft Tile left boundary
     * @param cropTop Tile top boundary
     * @param cropRight Tile right boundary
     * @param cropBottom Tile bottom boundary
     */
    override fun draw(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float) {
        val scale = bitmap.width.toFloat() / pageSize.x / (cropRight - cropLeft)
        canvas.setBitmap(bitmap)
        canvas.drawRect((area.left - pageSize.x * cropLeft) * scale,
                         (area.top - pageSize.y * cropTop) * scale,
                         (area.right - pageSize.x * cropLeft) * scale,
                         (area.bottom - pageSize.y * cropTop) * scale,
                         paint)
    }
}
