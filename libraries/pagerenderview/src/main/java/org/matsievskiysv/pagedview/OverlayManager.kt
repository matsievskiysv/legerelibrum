package org.matsievskiysv.pagerenderview

import android.graphics.Bitmap

import android.util.Log

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.Lifecycle

import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Job
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive

import org.matsievskiysv.pagerenderview.geometry.*
import org.matsievskiysv.pagerenderview.parcel.OverlayManagerParcel
import org.matsievskiysv.pagerenderview.interfaces.OverlayItem
import org.matsievskiysv.pagerenderview.interfaces.Overlay

/**
 * Overlay manager.
 */
class OverlayManager() : LifecycleObserver {
    private val logName = this::class.simpleName
    /**
     * Overlay layers.
     */
    private val layers = mutableListOf<Pair<Int, Overlay>>()
    /**
     * Function used to get page coordinates. Function argument is a page number, return value is coordinate rectangle.
     */
    lateinit public var getPageCoords: (Int) -> RectI
    /**
     * Function to call when layers were updated.
     * Argument of a function is a page number of updated page or null if all pages were updated.
     */
    lateinit public var onUpdate: (Int?) -> Unit
    /**
     * Life cycle object
     */
    public var lifeCycle: Lifecycle? = null
        set(value) {
            field = value
            value!!.addObserver(this)
        }
    /**
     * Coroutine scope
     */
    public var coroutineScope : CoroutineScope? = null
    /**
     * Execute function on start.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onStart() {
    }
    /**
     * Execute function on stop.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onStop() {
        layers.clear()
    }
    /**
     * Add overlay.
     * @param priority Overlay priority
     * @param overlay Overlay to add
     */
    public fun addOverlay(priority: Int, overlay: Overlay) {
        layers.add(Pair(priority, overlay))
        layers.sortBy { it.first }
        if (!overlay.empty) {
            onUpdate(null)
        }
    }
    /**
     * Draw page overlay images on bitmap.
     * @param bitmap Bitmap to put image into
     * @param pageNum Page number
     * @param tileLeft Cropping boundary
     * @param tileTop Cropping boundary
     * @param tileRight Cropping boundary
     * @param tileBottom Cropping boundary
     */
    public fun draw(bitmap: Bitmap,
                    pageNum: Int,
                    tileLeft: Float,
                    tileTop: Float,
                    tileRight: Float,
                    tileBottom: Float) {
        for (layer in layers.map { it.second }) {
            Log.d(logName, "drawing overlay ${layer.tag} on page ${pageNum}")
            layer.drawPage(bitmap, pageNum, tileLeft, tileTop, tileRight, tileBottom)
        }
    }
    /**
     * Call action at point
     * @param page Search for [OverlayItem] at this page
     * @param point Search for [OverlayItem] at this position
     * @param type Action type
     * @return Action was called
     */
    fun callAtPoint(page: Int, point: PointI, type: OverlayAction): Boolean =
        layers.map { it.second } .firstOrNull {it.runCallbacks && it.callAtPoint(page, point, type) } ?.let {
            true
        } ?: false
    /**
     * Preprocess layers.
     */
    public fun preprocessLayers() {
        // for (layer in layers) {
        //     if (layer.job == null) {
        //         layer.job = coroutineScope?.launch(Dispatchers.Main) {
        //             layer.items.values.flatten().map {

        //             }
                    // val items: Map<Int, List<OverlayItem>>,
                    // var actionMap: MutableList<Pair<RectI, OverlayItem>>?

                    // layer.items[pageNum]?.forEach {
                    //     it.withBlock {
                    //         if (it.page == pageNum) {
                    //             it.drawOnTile(bitmap, tileLeft, tileTop, tileRight, tileBottom)
                    //         }
                    //     }
        //             // }
        //         }
        //     }
        // }
    }
    /**
     * Write state to parcel.
     * @param parcel State parcel
     */
    public fun writeToParcel(parcel: OverlayManagerParcel) : OverlayManagerParcel {
        return parcel
    }
    /**
     * Write state to parcel.
     */
    public fun writeToParcel() : OverlayManagerParcel = writeToParcel(OverlayManagerParcel())
    /**
     * Read state from parcel.
     * @param parcel State parcel
     */
    public fun readFromParcel(parcel: OverlayManagerParcel) {
    }
}
/**
 * Overlay action type.
 */
public enum class OverlayAction {
    /**
     * Single tap
     */
    SINGLETAP,
    /**
     * Double tap
     */
    DOUBLETAP,
    /**
     * Long press
     */
    LONGPRESS,
}
