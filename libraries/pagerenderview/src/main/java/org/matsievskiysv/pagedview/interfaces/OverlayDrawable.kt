package org.matsievskiysv.pagerenderview.interfaces

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * Abstract class for overlay draw object.
 * Drawable object encapsulates geometric feature or image drawing implementation, e.g. drawing a colored rectangle.
 * @param pageSize Size of page
 * @property pageSize Size of page
 * @param pageSize Size of page
 * @property area Drawable area
 * @param area Drawable area
 */
abstract class OverlayDrawable(public val pageSize: PointI, public val area: RectI) {
    /**
     * Draw on page
     * @param canvas Drawing canvas
     * @param paint Drawing paint
     * @param bitmap Drawing bitmap
     * @param cropLeft Tile left boundary
     * @param cropTop Tile top boundary
     * @param cropRight Tile right boundary
     * @param cropBottom Tile bottom boundary
     */
    abstract fun draw(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float)
}
