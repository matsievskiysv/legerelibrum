package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import kotlin.math.*

import org.matsievskiysv.pagerenderview.ViewRectMapper
import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [ViewRectMapper] state serialization.
 */
public class ViewRectMapperParcel() : Parcelable {
    /**
     * Scale down flag
     */
    public var scaleDown = false
    /**
     * Scale down factor
     */
    public var scaleDownFactor = 1f
    /**
     * Zoom value
     */
    public var zoom = 1f
    /**
     * Zoom max value
     */
    public var zoomMax = 1f
    // /**
    //  * Crop flag
    //  */
    // public var crop = false

    constructor(parcel: Parcel) : this() {
        scaleDown = parcel.readBoolean()
        scaleDownFactor = parcel.readFloat()
        zoom = parcel.readFloat()
        zoomMax = parcel.readFloat()
        // crop = parcel.readBoolean()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeBoolean(scaleDown)
        parcel.writeFloat(scaleDownFactor)
        parcel.writeFloat(zoom)
        parcel.writeFloat(zoomMax)
        // parcel.writeBoolean(crop)
    }

    companion object {
        val CREATOR: Parcelable.Creator<ViewRectMapperParcel?>
            = object : Parcelable.Creator<ViewRectMapperParcel?> {
                override fun createFromParcel(parcel: Parcel): ViewRectMapperParcel? {
                    return ViewRectMapperParcel(parcel)
                }
                override fun newArray(size: Int): Array<ViewRectMapperParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
