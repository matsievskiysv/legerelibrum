package org.matsievskiysv.pagerenderview.geometry

import kotlin.math.*

/**
 * Integer point geometry point
 *
 * Inplace method versions are prepended with letter «i»
 *
 * @constructor Create new point
 * @property x X coordinate
 * @property y Y coordinate
 * @param x X coordinate
 * @param y Y coordinate
 */
public class PointI(var x: Int, var y: Int) {
    /**
     * Create point at (0,0)
     */
    constructor() : this(0, 0)
    /**
     * Copy point
     * @param other Other point
     */
    constructor(other: PointI) : this(other.x, other.y)
    /**
     * Copy point
     * @param other Other point
     */
    constructor(other: PointF) : this(other.x.toInt(), other.y.toInt())
    /**
     * Copy point
     * @param other Other point
     */
    constructor(other: Pair<Int, Int>) : this(other.first, other.second)
    /**
     * Get string representation
     * @return String representation
     */
    override fun toString(): String {
        return "PointI[$x;$y]"
    }
    /**
     * Compare points
     * @param other Other point
     * @return Comparison result
     */
    override operator fun equals(other: Any?): Boolean {
        return when (other) {
            is PointI -> x == other.x && y == other.y
            else -> super.equals(other)
        }
    }
    /**
     * Return X coordinate
     * @return X coordinate
     */
    operator fun component1(): Int = x
    /**
     * Return Y coordinate
     * @return Y coordinate
     */
    operator fun component2(): Int = y
    /**
     * Set coordinates inplace
     * @param first X coordinate
     * @param second Y coordinate
     * @return This point
     */
    fun iSet(first: Int, second: Int): PointI =
        this.apply {
            x = first
            y = second
        }
    /**
     * Set coordinates inplace
     * @param other Other point
     * @return This point
     */
    fun iSet(other: PointI): PointI =
        this.apply {
            x = other.x
            y = other.y
        }
    /**
     * Clip point coordinates between two coordinates
     * @param start Start coordinate
     * @param end End coordinate
     * @return New point
     */
    fun clip(start: Int, end: Int): PointI =
        PointI(
            clipnum(x, start, end),
            clipnum(y, start, end)
        )
    /**
     * Clip point coordinates between two points inplace
     * @param start Start point
     * @param end End point
     * @return This point
     */
    fun iClip(start: Int, end: Int): PointI =
        iSet(clipnum(x, start, end), clipnum(y, start, end))
    /**
     * Clip point coordinates between two coordinates
     * @param start Start point
     * @param end End point
     * @return New point
     */
    fun clip(start: PointI, end: PointI): PointI =
        PointI(
            clipnum(x, start.x, end.x),
            clipnum(y, start.y, end.y)
        )
    /**
     * Clip point coordinates between two points inplace
     * @param start Start point
     * @param end End point
     * @return This point
     */
    fun iClip(start: PointI, end: PointI): PointI = iSet(clipnum(x, start.x, end.x), clipnum(y, start.y, end.y))
    /**
     * Apply function to coordinates
     * @param func Function to apply
     * @return New point
     */
    fun map(func: (Int) -> Int): PointI = PointI(func(x), func(y))
    /**
     * Apply function to coordinates inplace
     * @param func Function to apply
     * @return This point
     */
    fun iMap(func: (Int) -> Int): PointI = iSet(func(x), func(y))

    // {{{ plus
    /**
     * Add points
     * @param other Scalar value
     * @return New point
     */
    operator fun plus(other: Int): PointI = PointI(x + other, y + other)
    /**
     * Add points
     * @param other Scalar value
     * @return New point
     */
    operator fun plus(other: Float): PointF = PointF(x + other, y + other)
    /**
     * Add points inplace
     * @param other Scalar value
     */
    operator fun plusAssign(other: Int) {
        this.x += other
        this.y += other
    }
    /**
     * Add points
     * @param other Other point
     * @return New point
     */
    operator fun plus(other: PointI): PointI = PointI(x + other.x, y + other.y)
    /**
     * Add points
     * @param other Other point
     * @return New point
     */
    operator fun plus(other: PointF): PointF = PointF(x + other.x, y + other.y)
    /**
     * Add points inplace
     * @param other Other point
     */
    operator fun plusAssign(other: PointI) {
        x += other.x
        y += other.y
    }
    // }}}

    // {{{ minus number
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Int): PointI = PointI(x - other, y - other)
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Float): PointF = PointF(x - other, y - other)
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: Int) {
        x -= other
        y -= other
    }
    /**
     * Subtract points
     * @param other Other point
     * @return New point
     */
    operator fun minus(other: PointI): PointI = PointI(x - other.x, y - other.y)
    /**
     * Subtract points
     * @param other Other point
     * @return New point
     */
    operator fun minus(other: PointF): PointF = PointF(x - other.x, y - other.y)
    /**
     * Subtract points inplace
     * @param other Other point
     */
    operator fun minusAssign(other: PointI) {
        x -= other.x
        y -= other.y
    }
    // }}}

    // {{{ times number
    /**
     * Multiply points
     * @param other Scalar value
     * @return New point
     */
    operator fun times(other: Int): PointI = PointI(x * other, y * other)
    /**
     * Multiply points
     * @param other Scalar value
     * @return New point
     */
    operator fun times(other: Float): PointF = PointF(x * other, y * other)
    /**
     * Multiply points
     * @param other Other point
     * @return New point
     */
    operator fun times(other: PointF): PointF = PointF(x * other.x, y * other.y)
    /**
     * Multiply points inplace
     * @param other Scalar value
     */
    operator fun timesAssign(other: Int) {
        x *= other
        y *= other
    }
    /**
     * Multiply points
     * @param other Other point
     * @return New point
     */
    operator fun times(other: PointI): PointI = PointI(x * other.x, y * other.y)
    /**
     * Multiply points inplace
     * @param other Other point
     */
    operator fun timesAssign(other: PointI) {
        x *= other.x
        y *= other.y
    }
    // }}}

    // {{{ div number
    /**
     * Divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun div(other: Int): PointI = PointI(x / other, y / other)
    /**
     * Divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun div(other: Float): PointF = PointF(x / other, y / other)
    /**
     * Divide points inplace
     * @param other Scalar value
     */
    operator fun divAssign(other: Int) {
        x /= other
        y /= other
    }
    /**
     * Divide points
     * @param other Other point
     * @return New point
     */
    operator fun div(other: PointI): PointI = PointI(x / other.x, y / other.y)
    /**
     * Divide points
     * @param other Other point
     * @return New point
     */
    operator fun div(other: PointF): PointF = PointF(x / other.x, y / other.y)
    /**
     * Divide points inplace
     * @param other Other point
     */
    operator fun divAssign(other: PointI) {
        x /= other.x
        y /= other.y
    }
    // }}}

    // {{{ rem number
    /**
     * Whole divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun rem(other: Int): PointI = PointI(x % other, y % other)
    /**
     * Whole divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun rem(other: Float): PointF = PointF(x % other, y % other)
    /**
     * Whole divide points inplace
     * @param other Scalar value
     */
    operator fun remAssign(other: Int) {
        x %= other
        y %= other
    }
    /**
     * Whole divide points
     * @param other Other point
     * @return New point
     */
    operator fun rem(other: PointI): PointI = PointI(x % other.x, y % other.y)
    /**
     * Whole divide points
     * @param other Other point
     * @return New point
     */
    operator fun rem(other: PointF): PointF = PointF(x % other.x, y % other.y)
    /**
     * Whole divide points inplace
     * @param other Other point
     */
    operator fun remAssign(other: PointI) {
        x %= other.x
        y %= other.y
    }
    // }}}

    // {{{ min
    /**
     * Get minimum coordinates of two points
     * @param other Other point
     * @return New point
     */
    infix fun min(other: PointI): PointI = PointI(min(x, other.x), min(y, other.y))
    /**
     * Get minimum coordinates of two points inplace
     * @param other Other point
     * @return This point
     */
    infix fun iMin(other: PointI): PointI = iSet(min(x, other.x), min(y, other.y))
    // }}}

    // {{{ max
    /**
     * Get maximum coordinates of two points
     * @param other Other point
     * @return New point
     */
    infix fun max(other: PointI): PointI = PointI(max(x, other.x), max(y, other.y))
    /**
     * Get maximum coordinates of two points inplace
     * @param other Other point
     * @return This point
     */
    infix fun iMax(other: PointI): PointI = iSet(max(x, other.x), max(y, other.y))
    // }}}

    // {{{ isInside
    /**
     * Check if point is inside rectangle
     * @param other Rectangle
     * @return Result
     */
    infix fun isInside(other: RectI): Boolean =
        x >= other.left && y >= other.top && x <= other.right && y <= other.bottom
    /**
     * Check if point is inside rectangle
     * @param other Rectangle
     * @return Result
     */
    infix fun isInside(other: RectF): Boolean =
        x >= other.left && y >= other.top && x <= other.right && y <= other.bottom
    // }}}
}
