package org.matsievskiysv.pagerenderview.geometry

/**
 * Rectangle sides
 */
public enum class RectSide {
    /**
     * Top left point
     */
    TOPLEFT,
    /**
     * Top point
     */
    TOP,
    /**
     * Top right point
     */
    TOPRIGHT,
    /**
     * Right point
     */
    RIGHT,
    /**
     * Bottom right point
     */
    BOTTOMRIGHT,
    /**
     * Bottom point
     */
    BOTTOM,
    /**
     * Bottom left point
     */
    BOTTOMLEFT,
    /**
     * Left point
     */
    LEFT,
    /**
     * Center point
     */
    CENTER,
}
