package org.matsievskiysv.pagerenderview

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Bitmap
import android.graphics.Matrix

import android.content.Context
import android.util.AttributeSet
import android.view.View

import android.view.MotionEvent
import android.view.View.BaseSavedState
// import android.content.res.Configuration
import android.view.GestureDetector
import android.view.ScaleGestureDetector

import android.os.Parcel
import android.os.Parcelable

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent

import android.util.Log
import android.util.Size
import android.widget.Toast

import kotlin.math.*

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Job
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive

import androidx.dynamicanimation.animation.FloatValueHolder
import androidx.dynamicanimation.animation.FlingAnimation

import org.matsievskiysv.pagerenderview.geometry.*
import org.matsievskiysv.pagerenderview.parcel.PageRenderViewParcel
import org.matsievskiysv.pagerenderview.interfaces.Document
import org.matsievskiysv.pagerenderview.interfaces.Overlay
import org.matsievskiysv.pagerenderview.helpers.*
import org.matsievskiysv.pagerenderview.PageRenderView

/**
 * Tiled page display [View]
 * @param context Application context
 * @param attrs Activity AttributeSet
 */
class PageRenderView(context: Context, attrs: AttributeSet?) : View(context, attrs), LifecycleObserver {
    private val logName = this::class.simpleName
    init {
        isSaveEnabled = true // enable view state save/restore
        setLongClickable(true) // enable view state save/restore
    }
    /**
     * Text color
     */
    public var TEXTCOLOR: Int = ( // TODO: read from settings?
        (0xff shl 24) or // alpha
        (0x3a shl 16) or // red
        (0x3a shl 8) or // green
        (0x3a)) // blue
        set(value) {
            field = value
            paint.setColor(value)
        }
    /**
     * Empty page color
     */
    public var FGCOLOR: Int = ( // TODO: read from settings?
        (0xff shl 24) or // alpha
        (0xea shl 16) or // red
        (0xea shl 8) or // green
        (0xea)) // blue
        set(value) {
            field = value
            pageLoader.invalidate()
        }
    /**
     * Background color
     */
    public var BGCOLOR: Int = ( // TODO: read from settings?
        (0xff shl 24) or // alpha
        (0xdd shl 16) or // red
        (0xdd shl 8) or // green
        (0xdd)) // blue
        set(value) {
            field = value
            pageLoader.invalidate()
        }
    /**
     * Scaled visible rectangle.
     * Shared between [PageRenderView] and [ViewRectMapper].
     */
    private val viewPort = RectI()
    /**
     * Scaled document rectangle.
     * Shared between [PageRenderView] and [ViewRectMapper].
     */
    private val docRect = RectI()
    /*
     * View size.
     * Shared between [PageRenderView] and [ViewRectMapper].
     */
    private val viewSize = PointI()
    /**
     * View state
     */
    private var state = ViewState.UNINITIALIZED
    /**
     * Overlay manager
     */
    private val overlayManager: OverlayManager = OverlayManager().apply {
        onUpdate = {
            _ -> pageLoader::invalidate
            // FIXME: handle single page invalidation //
            postInvalidate()
        }
    }
    /**
     * Page loader
     */
    private val pageLoader: PageLoader = PageLoader().apply {
        renderRequest = ::postInvalidate
        overlayDraw = overlayManager::draw
        onInvalidate = ::postInvalidate // FIXME: maybe unneeded
    }
    /**
     * Layout manager. This object should normally be used via [viewRectMapper].
     */
    private val pageLayoutManager = PageLayoutManager()
    /**
     * Document to view coordinate convert
     */
    private val viewRectMapper = ViewRectMapper(pageLayoutManager, viewSize, viewPort, docRect).apply {
        onInvalidateTiles = pageLoader::invalidate
    }
    /**
     * Life cycle object
     */
    public var lifeCycle: Lifecycle? = null
        set(value) {
            field = value
            value!!.addObserver(this)
            pageLoader.lifeCycle = value
            overlayManager.lifeCycle = value
        }
    /**
     * Coroutine scope
     */
    public var coroutineScope : CoroutineScope? = null
        set(value) {
            field = value
            pageLoader.coroutineScope = value
            overlayManager.coroutineScope = value
        }
    /**
     * Document handle
     */
    public var bitmapCacheSize = 0L
        set(value) {
            field = value
            pageLoader.bitmapCacheSize = value
        }
        get() = pageLoader.bitmapCacheSize
    /**
     * Speed limit for drawing tiles
     */
    public var noDrawSpeed = 1000
    /**
     * Horizontal fling is in process
     */
    private var flingingX = false
    /**
     * Vertical fling is in process
     */
    private var flingingY = false
    /**
     * Defer bitmap render
     */
    private var deferLoading = false
        get() {
            return flingingX || flingingY
        }
    /**
     * Flag indicating that calculation of crop pages is done
     */
    private var cropDone = false
    /**
     * Enable/disable page crop
     */
    public var crop = false
        set(value) {
            if (!cropDone) {
                onCropNotAvailable()
            } else {
                if (state == ViewState.INITIALIZED) {
                    field = value
                    coroutineScope!!.launch(Dispatchers.Main) {
                        stopAnimation()
                    }
                    val page = getCurrentPage()
                    viewRectMapper.crop = value
                    viewRectMapper.invalidate()
                    viewRectMapper.updatePosition {} // FIXME: try removing updatePosition call
                    viewRectMapper.updateSize()
                    viewRectMapper.moveTo(page)
                    postInvalidate()
                }
            }
        }
    /**
     * Current page
     */
    public var page = 0
        get() = when (state) {
            ViewState.INITIALIZED -> {
                viewRectMapper.getCurrentPage()
            }
            else -> 0
        }
        set(value) {
            if (state == ViewState.INITIALIZED) {
                field = value
                viewRectMapper.moveTo(value)
                postInvalidate()
            }
        }
    /**
     * Page padding.
     */
    public var padding = RectI()
        set(value) {
            field = value
            viewRectMapper.padding = value
            postInvalidate()
        }
        get() = viewRectMapper.padding
    /**
     * Page flow direction.
     */
    public var pageFlow = 0
        set(value) {
            field = value
            viewRectMapper.pageFlow = Direction.values()[value]
            postInvalidate()
        }
        get() = viewRectMapper.pageFlow.ordinal
    /**
     * Left to right.
     */
    public var leftToRight = true
        set(value) {
            field = value
            viewRectMapper.leftToRight = value
            postInvalidate()
        }
        get() = viewRectMapper.leftToRight
    /**
     * Left to right.
     */
    public var topToBottom = true
        set(value) {
            field = value
            viewRectMapper.topToBottom = value
            postInvalidate()
        }
        get() = viewRectMapper.topToBottom
    /**
     * Number of columns.
     */
    public var columns = 1
        set(value) {
            field = value
            viewRectMapper.columns = value
            postInvalidate()
        }
        get() = viewRectMapper.columns
    /**
     * Number of rows.
     */
    public var rows = 1
        set(value) {
            field = value
            viewRectMapper.rows = value
            postInvalidate()
        }
        get() = viewRectMapper.rows
    /**
     * Callback to run when crop becomes available
     */
    public var onCropAvailable: () -> Unit = {}
    /**
     * Callback to run when crop is requested but not yet available
     */
    public var onCropNotAvailable: () -> Unit = {}
    // /**
    //  * Execute function on start
    //  */
    // @OnLifecycleEvent(Lifecycle.Event.ON_START)
    // private fun onStart() {
    //     Log.v(logName, "onStart hook ${state}")
    //     when (state) {
    //         ViewState.INITIALIZED -> {
    //             viewRectMapper.invalidate()
    //         }
    //         else -> {}
    //     }
    // }
    /**
     * Execute function on start
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onStart() {
        Log.v(logName, "onStart hook ${state}")
        when (state) {
            ViewState.INITIALIZED -> {
                viewRectMapper.invalidate()
                if (!cropDone) {
                    pageLoader.calculateCrop(pageLayoutManager.pageGeometries) {
                        cropDone = true
                        onCropAvailable()
                    }
                }
            }
            else -> {}
        }
    }
    /**
     * Execute function on stop
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onStop() {
        Log.v(logName, "onStop hook")
    }
    /**
     * Initialize view with document
     * @param doc Object implementing [Document] interface
     * @param viewState View state
     */
    public fun setDocument(doc: Document,
                           viewState: PageRenderViewParcel? = null) {
        Log.v(logName, "initialize data structures")
        state = ViewState.INITIALIZING
        pageLoader.initialize(doc)

        if (viewState == null) {
            lateinit var pageList: List<PageLayoutManager.PageGeometry>
            coroutineScope!!.launch(Dispatchers.Default) {
                doc.withBlock {
                    Log.v(logName, "start calculating page sizes")
                    val pageNum = it.pageNum()
                    pageList = List(pageNum, { _ -> PageLayoutManager.PageGeometry(PointI(0, 0),
                                                                                   RectF(0f, 0f, 0f, 0f)) })
                    for (i in 0 until pageNum) {
                        if (!isActive) {
                            // cooperative stop
                            break
                        }
                        it.page(i).withBlock {
                            Log.v(logName, "get page size ${i}")
                            it.close()
                            pageList[i].size.iSet(it.pageSize)
                        }
                        pageLoadPercent = 100 * i / pageNum
                        postInvalidate()
                    }
                }
            }.invokeOnCompletion {
                Log.v(logName, "calculate page sizes callback")
                pageLayoutManager.initialize(pageList)
                state = ViewState.INITIALIZED
                viewRectMapper.resetPosition()
                if (viewSize.x != 0 && viewSize.y != 0) {
                    viewRectMapper.updateSize()
                }
                postInvalidate()
                pageLoader.calculateCrop(pageList) {
                    cropDone = true
                    onCropAvailable()
                }
            }
        } else {
            Log.v(logName, "restoring saved state")
            readFromParcel(viewState)
            state = ViewState.INITIALIZED
            viewRectMapper.updateSize()
            postInvalidate()
            if (!cropDone) {
                pageLoader.calculateCrop(pageLayoutManager.pageGeometries) {
                    cropDone = true
                    onCropAvailable()
                }
            }
        }
    }
    // /**
    //  * Save state
    //  * @return Parcel
    //  */
    // override fun onSaveInstanceState(): Parcelable {
    //     val bundle = Bundle()
    //     val currentPage = if (state == ViewState.INITIALIZED) {
    //         viewRectMapper.getCurrentPage()
    //     } else {
    //         0
    //     }
    //     bundle.putInt("currentPage", currentPage)
    //     bundle.putParcelable("superState", super.onSaveInstanceState())
    //     Log.d("org.matsievskiysv.pagerenderviewtestapp", "save state")
    //     return bundle
    // }
    // /**
    //  * Restore state
    //  * @param st Parcel
    //  */
    // override fun onRestoreInstanceState(st: Parcelable) {
    //     if (st is Bundle) {
    //         if (state == ViewState.INITIALIZED) {
    //             var currentPage = st.getInt("currentPage", 0)
    //             viewRectMapper.moveToPage(currentPage)
    //         }
    //         Log.d("org.matsievskiysv.pagerenderviewtestapp", "restore state")
    //         super.onRestoreInstanceState(st.getParcelable("superState")!!)
    //     }
    // }
    /**
     * On size changed callback
     * @param w View width
     * @param h View height
     * @param oldw Old view width
     * @param oldh Old view height
     */
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        Log.d(logName, "size changed ${w} ${h}")
        viewSize.iSet(w, h)
        if (state == ViewState.INITIALIZED) {
            Log.v(logName, "initialized size changed ${viewSize}")
            viewRectMapper.updateSize()
        }
    }
    /**
     * Get current page based on occupied screen size
     * @return Current page
     */
    public fun getCurrentPage(): Int {
        if (state == ViewState.INITIALIZED) {
            return viewRectMapper.getCurrentPage()
        } else {
            return 0
        }
    }
    /**
     * Get view state
     * @return Current position coordinates
     */
    public fun writeToParcel(): PageRenderViewParcel? {
        if (state == ViewState.INITIALIZED) {
            val viewState = PageRenderViewParcel()
            viewState.textColor = TEXTCOLOR
            viewState.fgColor = FGCOLOR
            viewState.bgColor = BGCOLOR
            viewState.overlayManager = overlayManager.writeToParcel()
            viewState.viewRectMapper = viewRectMapper.writeToParcel()
            viewState.pageLayoutManager = pageLayoutManager.writeToParcel()
            viewState.viewSize.point.iSet(viewSize)
            viewState.noDrawSpeed = noDrawSpeed
            viewState.cropDone = cropDone
            viewState.crop = crop
            return viewState
        } else {
            return null
        }
    }
    /**
     * Apply view state
     * @param viewState View state
     */
    public fun readFromParcel(viewState: PageRenderViewParcel) {
        TEXTCOLOR = viewState.textColor
        FGCOLOR = viewState.fgColor
        BGCOLOR = viewState.bgColor
        pageLayoutManager.readFromParcel(viewState.pageLayoutManager)
        viewRectMapper.readFromParcel(viewState.viewRectMapper)
        overlayManager.readFromParcel(viewState.overlayManager)
        viewSize.iSet(viewState.viewSize.point)
        noDrawSpeed = viewState.noDrawSpeed
        cropDone = viewState.cropDone
        crop = viewState.crop
        postInvalidate()
    }

    public fun addOverlay(priority: Int, overlay: Overlay) {
        Log.i(logName, "add overlay ${overlay.tag} with priority ${priority}")
        overlayManager.addOverlay(priority, overlay)
    }
    // public fun moveTo(page: Int, horizontalPosition: Int, verticalPosition: Int) {
    //     viewRectMapper.moveTo(page, horizontalPosition, verticalPosition)
    //     postInvalidate()
    // }

    // public fun moveTo(page: Int, horizontalPosition: Float, verticalPosition: Float) {
    //     viewRectMapper.moveTo(page, horizontalPosition, verticalPosition)
    //     postInvalidate()
    // }

    // public fun moveTo(page: Int) = moveTo(page, 0, 0)

    // {{{ drawing
    private var pageLoadPercent = 0
    /**
     * On draw callback
     * @param canvas Draw canvas
     */
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        when (state) {
            ViewState.UNINITIALIZED -> drawWarningMessage(canvas, "View is not initialized")
            ViewState.INITIALIZING -> drawWarningMessage(canvas, "Retrieving page sizes: ${pageLoadPercent}%")
            ViewState.INITIALIZED -> {
                pageLoader.defer = deferLoading
                viewRectMapper.updatePosition {
                    loadBitmaps()
                }
                if (!deferLoading) {
                    startDeferredRender()
                }
                drawTiles(canvas)
            }
        }
    }
    /**
     * Paint to draw [View] info message
     */
    private val paint = Paint().apply {
        setColor(TEXTCOLOR)
        setTextAlign(Paint.Align.CENTER)
        setAntiAlias(true)
        setTextSize(50f)
        setStrokeWidth(5f)
    }
    /**
     * Draw warning message
     * @param canvas Drawing canvas
     * @param message Message
     */
    private fun drawWarningMessage(canvas: Canvas, message: String) {
        Log.v(logName, "draw warning message")
        canvas.apply {
            drawColor(BGCOLOR)
            drawText(message, viewSize.x.toFloat()/2, viewSize.y.toFloat()/2, paint)
        }
    }
    /**
     * Send bitmaps to load
     */
    private fun loadBitmaps() =
        viewRectMapper.withTiles {
            tile -> pageLoader.loadTileBitmap(tile, viewRectMapper.tileScale, viewRectMapper.scaleId)
        }
    /**
     * Continue stopped rendering
     */
    private fun startDeferredRender() =
        viewRectMapper.withTiles {
            tile -> pageLoader.startDeferredRender(tile, viewRectMapper.scaleId)
        }

    // private fun drawEmptyBitmap(
    //     bitmap: Bitmap,
    //     sizeX: Int,
    //     sizeY: Int,
    //     pageNum: Int,
    //     tileX: Int,
    //     tileY: Int
    // ) =
    //     canvas.apply {
    //         setBitmap(bitmap)
    //         drawColor(FGCOLOR)
    //         if (debugFlags.noRender) {
    //             val x = sizeX.toFloat()
    //             val y = sizeY.toFloat()
    //             drawLines(
    //                 floatArrayOf(
    //                     0f, 0f,
    //                     x, 0f,
    //                     x, 0f,
    //                     x, y,
    //                     x, y,
    //                     0f, y,
    //                     0f, y,
    //                     0f, 0f
    //                 ),
    //                 paint
    //             )
    //             drawText("$pageNum:${tileX}x$tileY", x / 2, y / 2, paint)
    //         }
    //     }

    private val ctm = Matrix()
    private var emptyBitmap: Bitmap? = null
    /**
     * Draw tiles. Main drawing function.
     * @param viewCanvas Drawing canvas
     */
    private fun drawTiles(viewCanvas: Canvas) {
        viewCanvas.apply {
            drawColor(BGCOLOR)
        }
        val tileScale = viewRectMapper.tileScale
        val relativeZoom = viewRectMapper.tileRelativeZoom
        viewRectMapper.withTiles {
            tile ->
                Log.d(logName, "draw ${tile}")
                ctm.reset()
                val translate_horizontal = viewRectMapper.scale * tile.position.left - viewPort.left
                val translate_vertical = viewRectMapper.scale * tile.position.top - viewPort.top
                ctm.apply {
                    postScale(relativeZoom, relativeZoom)
                    postTranslate(translate_horizontal, translate_vertical)
                }

                val bitmap = pageLoader.getTile(tile, viewRectMapper.scaleId)
                when (bitmap) {
                    null -> {
                        val tileWidth = ceil(tile.position.width() * tileScale).toInt()
                        val tileHeight = ceil(tile.position.height() * tileScale).toInt()
                        // try to reuse bitmap from previous runs
                        if (emptyBitmap == null ||
                            emptyBitmap!!.getHeight() != tileHeight ||
                            emptyBitmap!!.getWidth() != tileWidth) {
                            if (emptyBitmap != null) {
                                pageLoader.putBitmap(emptyBitmap!!)
                            }
                            emptyBitmap = pageLoader.getBitmap(tileWidth, tileHeight)
                            emptyBitmap?.eraseColor(FGCOLOR)
                        }
                        viewCanvas.drawBitmap(emptyBitmap!!, ctm, null)
                    }
                    else -> viewCanvas.drawBitmap(bitmap, ctm, null)
                }
        }
    }
    // }}}

    // {{{ animation
    /**
     * Fling animation friction
     */
    public var flingFriction = 1.2f // TODO: read from settings
        set(value) {
            animationX.friction = value
            animationY.friction = value
            field = value
        }
    /**
     * Animation in X direction
     */
    private var animationX = FlingAnimation(FloatValueHolder(0f))
        .apply {
            setMinValue(0f)
            addEndListener { _, _, _, _ -> flingingX = false; postInvalidate() }
            addUpdateListener {
                _, position, velocity ->
                if (state == ViewState.INITIALIZED) {
                    Log.v(logName, "animationX position ${position}")
                    viewRectMapper.moveToHorizontal(round(position).toInt())
                    if (abs(velocity) > noDrawSpeed) {
                        flingingX = true
                    } else {
                        flingingX = false
                    }
                    postInvalidate()
                }
            }
        }
    /**
     * Animation in Y direction
     */
    private var animationY = FlingAnimation(FloatValueHolder(0f))
        .apply {
            setMinValue(0f)
            addEndListener { _, _, _, _ -> flingingY = false; postInvalidate() }
            addUpdateListener {
                _, position, velocity ->
                viewRectMapper.moveToVertical(round(position).toInt())
                if (state == ViewState.INITIALIZED) {
                    if (abs(velocity) > noDrawSpeed) {
                        flingingY = true
                    } else {
                        flingingY = false
                    }
                    postInvalidate()
                }
            }
        }
    /**
     * Stop animation.
     * @return Animation was stopped.
     */
    private fun stopAnimation(): Boolean {
        val rv = animationX.isRunning() || animationY.isRunning()
        if (animationX.isRunning()) {
            animationX.cancel()
        }
        if (animationY.isRunning()) {
            animationY.cancel()
        }
        return rv
    }
    // }}}

    // {{{ gestures
    /**
     * Scale listener
     */
    private val scaleDetector: ScaleGestureDetector = ScaleGestureDetector(
        context,
        object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                if (state == ViewState.INITIALIZED) {
                    Log.v(logName, "onScale event")
                    viewRectMapper.scale(detector.getFocusX(), detector.getFocusY(), detector.getScaleFactor())
                    postInvalidate()
                    return true
                } else {
                    return false
                }
            }
        }
    )
    private val clickPoint = PointI()
    /**
     * Gesture detector
     */
    private val simpleDetector: GestureDetector = GestureDetector(
        context,
        object : GestureDetector.SimpleOnGestureListener() {
            override fun onDown(e: MotionEvent): Boolean {
                Log.v(logName, "onDown event")
                stopAnimation()
                return true
            }
            override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean =
                when (state) {
                    ViewState.INITIALIZED -> {
                        Log.v(logName, "onScroll event")
                        viewRectMapper.scroll(round(distanceX).toInt(), round(distanceY).toInt())
                        postInvalidate()
                        true
                    }
                    else -> false
                }

            override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean =
                when (state) {
                    ViewState.INITIALIZED -> {
                        Log.v(logName, "onFling event with speed ${velocityX} ${velocityY}")
                        if ((viewPort.left > 0) && (viewPort.right < docRect.right)) {
                            animationX.setMaxValue((docRect.right - viewPort.width()).toFloat())
                                .setFriction(flingFriction)
                                .setMinimumVisibleChange(viewPort.width() / 100f)
                                .setStartValue(viewPort.left.toFloat())
                                .setStartVelocity(-velocityX)
                                .start()
                        }
                        if ((viewPort.top > 0) && (viewPort.bottom < docRect.bottom)) {
                            animationY.setMaxValue((docRect.bottom - viewPort.height()).toFloat())
                                .setFriction(flingFriction)
                                .setMinimumVisibleChange(viewPort.height() / 100f)
                                .setStartValue(viewPort.top.toFloat())
                                .setStartVelocity(-velocityY)
                                .start()
                        }
                        true
                    }
                    else -> false
                }
            override fun onSingleTapConfirmed(e: MotionEvent): Boolean =
                when (state) {
                    ViewState.INITIALIZED -> {
                        Log.v(logName, "singleTapConfirmed event")
                        clickPoint.iSet(round(e.getX(0)).toInt(), round(e.getY(0)).toInt())
                        val page = viewRectMapper.getPositionOnPage(clickPoint)
                        if (page != null) {
                            Log.d(logName, "single tap page ${page} point ${clickPoint}")
                            overlayManager.callAtPoint(page, clickPoint, OverlayAction.SINGLETAP)
                        }
                        true
                    }
                    else -> false
                }
            override fun onDoubleTap(e: MotionEvent): Boolean =
                when (state) {
                    ViewState.INITIALIZED -> {
                        Log.v(logName, "onDoubleTap event")
                        clickPoint.iSet(round(e.getX(0)).toInt(), round(e.getY(0)).toInt())
                        val page = viewRectMapper.getPositionOnPage(clickPoint)
                        if (page != null) {
                            Log.d(logName, "double tap page ${page} point ${clickPoint}")
                            overlayManager.callAtPoint(page, clickPoint, OverlayAction.DOUBLETAP)
                        }
                        true
                    }
                    else -> false
                }
            override fun onLongPress(e: MotionEvent) {
                when (state) {
                    ViewState.INITIALIZED -> {
                        Log.v(logName, "longPress event")
                        clickPoint.iSet(round(e.getX(0)).toInt(), round(e.getY(0)).toInt())
                        val page = viewRectMapper.getPositionOnPage(clickPoint)
                        if (page != null) {
                            Log.d(logName, "long press page ${page} point ${clickPoint}")
                            overlayManager.callAtPoint(page, clickPoint, OverlayAction.LONGPRESS)
                        }
                    }
                    else -> {}
                }
            }
        }
    )
    /**
     * On touch callback
     * @param event Motion event
     * @return Event handled flag
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return scaleDetector.onTouchEvent(event).let {
            scaleDetector.isInProgress()
        } || simpleDetector.onTouchEvent(event).let {
            it
            // if (!result) {
            //     if (event.action == MotionEvent.ACTION_UP) {
            //         logger.v("gesture action up")
            //         // stopScrolling()
            //         true
            //     } else false
            // } else true
        }
    }
    // }}}
    /**
     * State of the view
     */
    enum class ViewState {
        /**
         * View not initialized
         */
        UNINITIALIZED,
        /**
         * View in process of initialization
         */
        INITIALIZING,
        /**
         * View initialized
         */
        INITIALIZED,
    }
}
