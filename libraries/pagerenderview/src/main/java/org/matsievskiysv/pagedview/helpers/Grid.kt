package org.matsievskiysv.pagerenderview.helpers

/**
 * Assign values to grid coordinates
 *
 * @param rows Initial number of grid rows
 * @param cols Initial number of grid cols
 * @property rows Number of grid rows
 * @property cols Number of grid cols
 * @property length Length of underlying buffer
 * @property dims Grid dimensions
 */
public class Grid<T>(private var rows: Int, private var cols: Int) {

    private val data = MutableList<T?>(length) { null }

    public val length: Int
        get() = this.cols * this.rows

    public val dims: Pair<Int, Int>
        get() = Pair(this.rows, this.cols)

    /**
     * Convert array index to grid position
     *
     * @param ind Array index
     * @return Grid position
     * @throws IndexOutOfBoundsException Index is out of array bounds
     */
    public fun ind2pos(ind: Int): Pair<Int, Int> {
        if (ind >= length) {
            throw IndexOutOfBoundsException()
        }
        val row = ind / cols
        val col = ind - row * cols
        return Pair(row, col)
    }

    /**
     * Convert grid position to array index
     *
     * @param row Row position
     * @param col Col position
     * @return Grid position
     */
    public fun pos2ind(row: Int, col: Int): Int {
        return cols * row + col
    }

    /**
     * Set grid position to value
     *
     * @param row Row position
     * @param col Col position
     * @param d Value to set
     * @throws IndexOutOfBoundsException Grid position is out of bounds
     */
    operator fun set(row: Int, col: Int, d: T?) {
        if (row < 0 || col < 0 || row > rows || col > cols) {
            throw IndexOutOfBoundsException()
        }
        data[pos2ind(row, col)] = d
    }

    /**
     * Get grid value at position
     *
     * @param row Row position
     * @param col Col position
     * @return Value at position
     * @throws IndexOutOfBoundsException Grid position is out of bounds
     */
    operator fun get(row: Int, col: Int): T? {
        if (row < 0 || col < 0 || row > rows || col > cols) {
            throw IndexOutOfBoundsException()
        }
        return data.elementAtOrNull(pos2ind(row, col))
    }

    /**
     * Change grid shape
     *
     * @param row Row position
     * @param col Col position
     */
    public fun reshape(row: Int, col: Int) {
        if (row * col > length) {
            data.addAll(List<T?>(row * col - length) { null })
        }
        rows = row
        cols = col
    }

    /**
     * Find first occurrence of value on grid
     *
     * @param v Value
     * @throws IndexOutOfBoundsException Value not found
     */
    public fun findFirst(v: T): Pair<Int, Int> {
        val i = data.indexOfFirst { it == v }
        if (i == -1) {
            throw IndexOutOfBoundsException()
        }
        return ind2pos(i)
    }

    /**
     * Set all values to null
     */
    public fun clear() {
        for (i in 0 until data.size) {
            data[i] = null
        }
    }

    /**
     * String representation of object
     *
     * @return String representation of object
     */
    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("Grid[\n")
        for (r in 0..rows - 1) {
            for (c in 0..cols - 1) {
                sb.append('\t')
                sb.append(data[pos2ind(r, c)])
            }
            sb.append('\n')
        }
        sb.append(']')
        return sb.toString()
    }
}
