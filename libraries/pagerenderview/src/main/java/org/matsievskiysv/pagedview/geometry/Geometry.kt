package org.matsievskiysv.pagerenderview.geometry

import kotlin.math.*

/**
 * Clip number between others
 *
 * @param v Value
 * @param start Start value
 * @param end End value
 * @return Clipped number
 */
fun <T : Comparable<T>> clipnum(v: T, start: T, end: T): T {
    if (start > end) {
        throw IllegalArgumentException()
    }
    if (v < start) {
        return start
    } else {
        if (v > end) {
            return end
        } else {
            return v
        }
    }
}
