package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import kotlin.math.*

import org.matsievskiysv.pagerenderview.geometry.*

/**
 * State serialization.
 */
public class PageRenderViewParcel() : Parcelable {
    /**
     * Text color
     */
    public var textColor = 0
    /**
     * Foreground color
     */
    public var fgColor = 0
    /**
     * Background color
     */
    public var bgColor = 0
    /**
     * [OverlayManager] state
     */
    public var overlayManager = OverlayManagerParcel()
    /**
     * [ViewRectMapper] state
     */
    public var viewRectMapper = ViewRectMapperParcel()
    /**
     * [PageLayoutManager] state
     */
    public var pageLayoutManager = PageLayoutManagerParcel()
    /**
     * View size to use before [PageRenderView.onSizeChanged] call
     */
    public var viewSize = PointIParcel()
    /**
     * Speed limit for drawing tiles
     */
    public var noDrawSpeed = 500
    /**
     * Crop done flag
     */
    public var cropDone = false
    /**
     * Crop flag
     */
    public var crop = false

    constructor(parcel: Parcel) : this() {
        textColor = parcel.readInt()
        fgColor = parcel.readInt()
        bgColor = parcel.readInt()
        pageLayoutManager = parcel.readParcelable(null)!!
        viewRectMapper = parcel.readParcelable(null)!!
        overlayManager = parcel.readParcelable(null)!!
        viewSize = parcel.readParcelable(null)!!
        noDrawSpeed = parcel.readInt()
        cropDone = parcel.readBoolean()
        crop = parcel.readBoolean()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(textColor)
        parcel.writeInt(fgColor)
        parcel.writeInt(bgColor)
        parcel.writeParcelable(pageLayoutManager, 0)
        parcel.writeParcelable(viewRectMapper, 0)
        parcel.writeParcelable(overlayManager, 0)
        parcel.writeParcelable(viewSize, 0)
        parcel.writeInt(noDrawSpeed)
        parcel.writeBoolean(cropDone)
        parcel.writeBoolean(crop)
    }

    companion object {
        val CREATOR: Parcelable.Creator<PageRenderViewParcel?>
            = object : Parcelable.Creator<PageRenderViewParcel?> {
                override fun createFromParcel(parcel: Parcel): PageRenderViewParcel? {
                    return PageRenderViewParcel(parcel)
                }
                override fun newArray(size: Int): Array<PageRenderViewParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
