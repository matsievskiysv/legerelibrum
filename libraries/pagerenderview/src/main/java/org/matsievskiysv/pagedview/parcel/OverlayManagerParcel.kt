package org.matsievskiysv.pagerenderview.parcel

import android.os.Parcel
import android.os.Parcelable

import kotlin.math.*

import org.matsievskiysv.pagerenderview.PageLayoutManager
import org.matsievskiysv.pagerenderview.geometry.*

/**
 * [OverlayManager] state serialization.
 */
public class OverlayManagerParcel() : Parcelable {

    constructor(parcel: Parcel) : this() {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }

    companion object {
        val CREATOR: Parcelable.Creator<OverlayManagerParcel?>
            = object : Parcelable.Creator<OverlayManagerParcel?> {
                override fun createFromParcel(parcel: Parcel): OverlayManagerParcel? {
                    return OverlayManagerParcel(parcel)
                }
                override fun newArray(size: Int): Array<OverlayManagerParcel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
