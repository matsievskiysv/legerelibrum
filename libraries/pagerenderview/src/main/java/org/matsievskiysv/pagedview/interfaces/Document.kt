package org.matsievskiysv.pagerenderview.interfaces

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

/**
 * Abstract class for opening document
 * @property mutex Document mutex
 */
abstract class Document {
    open protected val mutex = Mutex()
    /**
     * Execute non-blocking action with exclusive access to document
     * @param func Function to perform with document. Function argument is [Document]
     * @return Result of the function
     */
    open suspend fun <R> withLock(func: (Document) -> R): R {
        return mutex.withLock {
            this.let(func)
        }
    }
    /**
     * Execute blocking action with exclusive access to document
     * @param func Function to perform with document. Function argument is [Document]
     * @return Result of the function
     */
    fun <R> withBlock(func: (Document) -> R): R {
        return runBlocking {
            withLock(func)
        }
    }
    /**
     * Close document
     */
    open fun close() {}
    /**
     * Document is password protected
     * @return Document is encrypted
     */
    open fun needsPassword(): Boolean = false
    /**
     * Authenticate document
     * @param password Document password
     * @return Document unlocked
     */
    open fun authenticate(password: String): Boolean = true
    /**
     * Get number of pages
     * @return Number of pages
     */
    abstract fun pageNum(): Int
    /**
     * Get page
     * @param page Page number
     * @return Page
     */
    abstract fun page(page: Int): Page
}
