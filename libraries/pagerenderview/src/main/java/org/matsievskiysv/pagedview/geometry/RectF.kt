package org.matsievskiysv.pagerenderview.geometry

import kotlin.math.*

/**
 * Floating point rectangle
 *
 * Inplace method versions are prepended with letter «i»
 *
 * @constructor Create new rectangle
 * @property left Left boundary coordinate
 * @property top Top boundary coordinate
 * @property right Right boundary coordinate
 * @property bottom Bottom boundary coordinate
 * @param left Left boundary coordinate
 * @param top Top boundary coordinate
 * @param right Right boundary coordinate
 * @param bottom Bottom boundary coordinate
 */
public class RectF(var left: Float, var top: Float, var right: Float, var bottom: Float) {
    /**
     * Create rectangle with all zeros
     */
    constructor() : this(0f, 0f, 0f, 0f)
    /**
     * Copy rectangle
     * @param other Other rectangle
     */
    constructor(other: RectF) : this(other.left, other.top, other.right, other.bottom)
    /**
     * Copy rectangle
     * @param other Other rectangle
     */
    constructor(other: RectI) : this(
        other.left.toFloat(),
        other.top.toFloat(),
        other.right.toFloat(),
        other.bottom.toFloat()
    )
    /**
     * Get string representation
     * @return String representation
     */
    override fun toString(): String {
        return "RectF[$left;$top;$right;$bottom]"
    }
    /**
     * Compare rectangles
     * @param other Other rectangle
     * @return Comparison result
     */
    override operator fun equals(other: Any?): Boolean {
        return when (other) {
            is RectF -> left == other.left && top == other.top && right == other.right && bottom == other.bottom
            else -> super.equals(other)
        }
    }
    /**
     * Return left coordinate
     * @return Left coordinate
     */
    operator fun component1(): Float = left
    /**
     * Return top coordinate
     * @return Top coordinate
     */
    operator fun component2(): Float = top
    /**
     * Return right coordinate
     * @return Right coordinate
     */
    operator fun component3(): Float = right
    /**
     * Return bottom coordinate
     * @return Bottom coordinate
     */
    operator fun component4(): Float = bottom
    /**
     * Set coordinates inplace
     * @param first Left coordinate
     * @param top Top coordinate
     * @param right Right coordinate
     * @param bottom Bottom coordinate
     * @return This rectangle
     */
    fun iSet(first: Float, second: Float, third: Float, forth: Float): RectF =
        this.apply {
            left = first
            top = second
            right = third
            bottom = forth
        }
    /**
     * Set coordinates inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iSet(other: RectF): RectF =
        this.apply {
            left = other.left
            top = other.top
            right = other.right
            bottom = other.bottom
        }
    /**
     * Set coordinates inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iSet(other: RectI): RectF =
        this.apply {
            left = other.left.toFloat()
            top = other.top.toFloat()
            right = other.right.toFloat()
            bottom = other.bottom.toFloat()
        }
    /**
     * Apply function to coordinates
     * @param func Function to apply
     * @return New rectangle
     */
    fun map(func: (Float) -> Float): RectF = RectF(
        func(left),
        func(top),
        func(right),
        func(bottom)
    )
    /**
     * Apply function to coordinates inplace
     * @param func Function to apply
     * @return This rectangle
     */
    fun iMap(func: (Float) -> Float): RectF = iSet(
        func(left),
        func(top),
        func(right),
        func(bottom)
    )

    // {{{ plus
    /**
     * Add rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun plus(other: Int): RectF =
        RectF(
            left + other,
            top + other,
            right + other,
            bottom + other
        )
    /**
     * Add rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun plus(other: Float): RectF =
        RectF(
            left + other,
            top + other,
            right + other,
            bottom + other
        )
    /**
     * Add rectangles inplace
     * @param other Scalar value
     */
    operator fun plusAssign(other: Int) {
        left += other
        top += other
        right += other
        bottom += other
    }
    /**
     * Add rectangles inplace
     * @param other Scalar value
     */
    operator fun plusAssign(other: Float) {
        left += other
        top += other
        right += other
        bottom += other
    }
    /**
     * Add rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun plus(other: RectI): RectF =
        RectF(
            left + other.left,
            top + other.top,
            right + other.right,
            bottom + other.bottom
        )
    /**
     * Add rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun plus(other: RectF): RectF =
        RectF(
            left + other.left,
            top + other.top,
            right + other.right,
            bottom + other.bottom
        )
    /**
     * Add rectangles inplace
     * @param other Other rectangle
     */
    operator fun plusAssign(other: RectI) {
        left += other.left
        top += other.top
        right += other.right
        bottom += other.bottom
    }
    /**
     * Add rectangles inplace
     * @param other Other rectangle
     */
    operator fun plusAssign(other: RectF) {
        left += other.left
        top += other.top
        right += other.right
        bottom += other.bottom
    }
    /**
     * Add rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun plus(other: PointI): RectF =
        RectF(
            left + other.x,
            top + other.y,
            right + other.x,
            bottom + other.y
        )
    /**
     * Add rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun plus(other: PointF): RectF =
        RectF(
            left + other.x,
            top + other.y,
            right + other.x,
            bottom + other.y
        )
    /**
     * Add rectangles inplace
     * @param other Other point
     */
    operator fun plusAssign(other: PointI) {
        left += other.x
        top += other.y
        right += other.x
        bottom += other.y
    }
    /**
     * Add rectangles inplace
     * @param other Other point
     */
    operator fun plusAssign(other: PointF) {
        left += other.x
        top += other.y
        right += other.x
        bottom += other.y
    }
    // }}}

    // {{{ minus
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Int): RectF =
        RectF(
            left - other,
            top - other,
            right - other,
            bottom - other
        )
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Float): RectF =
        RectF(
            left - other,
            top - other,
            right - other,
            bottom - other
        )
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: Int) {
        left -= other
        top -= other
        right -= other
        bottom -= other
    }
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: Float) {
        left -= other
        top -= other
        right -= other
        bottom -= other
    }
    /**
     * Subtract rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun minus(other: RectI): RectF =
        RectF(
            left - other.left,
            top - other.top,
            right - other.right,
            bottom - other.bottom
        )
    /**
     * Subtract rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun minus(other: RectF): RectF =
        RectF(
            left - other.left,
            top - other.top,
            right - other.right,
            bottom - other.bottom
        )
    /**
     * Subtract rectangles inplace
     * @param other Other rectangle
     */
    operator fun minusAssign(other: RectI) {
        left -= other.left
        top -= other.top
        right -= other.right
        bottom -= other.bottom
    }
    /**
     * Subtract rectangles inplace
     * @param other Other rectangle
     */
    operator fun minusAssign(other: RectF) {
        left -= other.left
        top -= other.top
        right -= other.right
        bottom -= other.bottom
    }
    /**
     * Subtract rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun minus(other: PointI): RectF =
        RectF(
            left - other.x,
            top - other.y,
            right - other.x,
            bottom - other.y
        )
    /**
     * Subtract rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun minus(other: PointF): RectF =
        RectF(
            left - other.x,
            top - other.y,
            right - other.x,
            bottom - other.y
        )
    /**
     * Subtract rectangles inplace
     * @param other Other point
     */
    operator fun minusAssign(other: PointI) {
        left -= other.x
        top -= other.y
        right -= other.x
        bottom -= other.y
    }
    /**
     * Subtract rectangles inplace
     * @param other Other point
     */
    operator fun minusAssign(other: PointF) {
        left -= other.x
        top -= other.y
        right -= other.x
        bottom -= other.y
    }
    // }}}

    // {{{ times number
    /**
     * Multiply rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun times(other: Int): RectF =
        RectF(
            left * other,
            top * other,
            right * other,
            bottom * other
        )
    /**
     * Multiply rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun times(other: Float): RectF =
        RectF(
            left * other,
            top * other,
            right * other,
            bottom * other
        )
    /**
     * Multiply rectangles inplace
     * @param other Scalar value
     */
    operator fun timesAssign(other: Int) {
        left *= other
        top *= other
        right *= other
        bottom *= other
    }
    /**
     * Multiply rectangles inplace
     * @param other Scalar value
     */
    operator fun timesAssign(other: Float) {
        left *= other
        top *= other
        right *= other
        bottom *= other
    }
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun times(other: RectI): RectF =
        RectF(
            left * other.left,
            top * other.top,
            right * other.right,
            bottom * other.bottom
        )
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun times(other: RectF): RectF =
        RectF(
            left * other.left,
            top * other.top,
            right * other.right,
            bottom * other.bottom
        )
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun timesAssign(other: RectI) {
        left *= other.left
        top *= other.top
        right *= other.right
        bottom *= other.bottom
    }
    /**
     * Multiply rectangles inplace
     * @param other Other rectangle
     */
    operator fun timesAssign(other: RectF) {
        left *= other.left
        top *= other.top
        right *= other.right
        bottom *= other.bottom
    }
    /**
     * Multiply rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun times(other: PointI): RectF =
        RectF(
            left * other.x,
            top * other.y,
            right * other.x,
            bottom * other.y
        )
    /**
     * Multiply rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun times(other: PointF): RectF =
        RectF(
            left * other.x,
            top * other.y,
            right * other.x,
            bottom * other.y
        )
    /**
     * Multiply points inplace
     * @param other Other point
     */
    operator fun timesAssign(other: PointI) {
        left *= other.x
        top *= other.y
        right *= other.x
        bottom *= other.y
    }
    /**
     * Multiply points inplace
     * @param other Other point
     */
    operator fun timesAssign(other: PointF) {
        left *= other.x
        top *= other.y
        right *= other.x
        bottom *= other.y
    }
    // }}}

    // {{{ div
    /**
     * Divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun div(other: Int): RectF =
        RectF(
            left / other,
            top / other,
            right / other,
            bottom / other
        )
    /**
     * Divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun div(other: Float): RectF =
        RectF(
            left / other,
            top / other,
            right / other,
            bottom / other
        )
    /**
     * Divide rectangles inplace
     * @param other Scalar value
     */
    operator fun divAssign(other: Int) {
        left /= other
        top /= other
        right /= other
        bottom /= other
    }
    /**
     * Divide rectangles inplace
     * @param other Scalar value
     */
    operator fun divAssign(other: Float) {
        left /= other
        top /= other
        right /= other
        bottom /= other
    }
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun div(other: RectI): RectF =
        RectF(
            left / other.left,
            top / other.top,
            right / other.right,
            bottom / other.bottom
        )
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun div(other: RectF): RectF =
        RectF(
            left / other.left,
            top / other.top,
            right / other.right,
            bottom / other.bottom
        )
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun divAssign(other: RectI) {
        left /= other.left
        top /= other.top
        right /= other.right
        bottom /= other.bottom
    }
    /**
     * Divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun divAssign(other: RectF) {
        left /= other.left
        top /= other.top
        right /= other.right
        bottom /= other.bottom
    }
    /**
     * Divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun div(other: PointI): RectF =
        RectF(
            left / other.x,
            top / other.y,
            right / other.x,
            bottom / other.y
        )
    /**
     * Divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun div(other: PointF): RectF =
        RectF(
            left / other.x,
            top / other.y,
            right / other.x,
            bottom / other.y
        )
    /**
     * Divide points inplace
     * @param other Other point
     */
    operator fun divAssign(other: PointI) {
        left /= other.x
        top /= other.y
        right /= other.x
        bottom /= other.y
    }
    /**
     * Divide points inplace
     * @param other Other point
     */
    operator fun divAssign(other: PointF) {
        left /= other.x
        top /= other.y
        right /= other.x
        bottom /= other.y
    }
    // }}}

    // {{{ rem
    /**
     * Whole divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun rem(other: Int): RectF =
        RectF(
            left % other,
            top % other,
            right % other,
            bottom % other
        )
    /**
     * Whole divide rectangles
     * @param other Scalar value
     * @return New rectangle
     */
    operator fun rem(other: Float): RectF =
        RectF(
            left % other,
            top % other,
            right % other,
            bottom % other
        )
    /**
     * Whole divide rectangles inplace
     * @param other Scalar value
     */
    operator fun remAssign(other: Int) {
        left %= other
        top %= other
        right %= other
        bottom %= other
    }
    /**
     * Whole divide rectangles inplace
     * @param other Scalar value
     */
    operator fun remAssign(other: Float) {
        left %= other
        top %= other
        right %= other
        bottom %= other
    }
    /**
     * Whole divide rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun rem(other: RectI): RectF =
        RectF(
            left % other.left,
            top % other.top,
            right % other.right,
            bottom % other.bottom
        )
    /**
     * Whole divide rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    operator fun rem(other: RectF): RectF =
        RectF(
            left % other.left,
            top % other.top,
            right % other.right,
            bottom % other.bottom
        )
    /**
     * Whole divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun remAssign(other: RectI) {
        left %= other.left
        top %= other.top
        right %= other.right
        bottom %= other.bottom
    }
    /**
     * Whole divide rectangles inplace
     * @param other Other rectangle
     */
    operator fun remAssign(other: RectF) {
        left %= other.left
        top %= other.top
        right %= other.right
        bottom %= other.bottom
    }
    /**
     * Whole divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun rem(other: PointI): RectF =
        RectF(
            left % other.x,
            top % other.y,
            right % other.x,
            bottom % other.y
        )
    /**
     * Whole divide rectangles
     * @param other Other point
     * @return New rectangle
     */
    operator fun rem(other: PointF): RectF =
        RectF(
            left % other.x,
            top % other.y,
            right % other.x,
            bottom % other.y
        )
    /**
     * Whole divide rectangles inplace
     * @param other Other point
     */
    operator fun remAssign(other: PointI) {
        left %= other.x
        top %= other.y
        right %= other.x
        bottom %= other.y
    }
    /**
     * Whole divide rectangles inplace
     * @param other Other point
     */
    operator fun remAssign(other: PointF) {
        left %= other.x
        top %= other.y
        right %= other.x
        bottom %= other.y
    }
    // }}}

    // {{{ min
    /**
     * Get minimum coordinates of new rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    fun min(other: RectF): RectF =
        RectF(
            min(left, other.left),
            min(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    /**
     * Get minimum coordinates of two rectangles inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iMin(other: RectF): RectF =
        iSet(
            min(left, other.left),
            min(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    // }}}

    // {{{ max
    /**
     * Get maximum coordinates of two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    fun max(other: RectF): RectF =
        RectF(
            max(left, other.left),
            max(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    /**
     * Get maximum coordinates of two rectangles inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iMax(other: RectF): RectF =
        iSet(
            max(left, other.left),
            max(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    // }}}

    // {{{ round
    /**
     * Round rectangle coordinates
     * @return New rectangle
     */
    fun round(): RectF = map(::round)
    /**
     * Round rectangle coordinates
     * @return This rectangle
     */
    fun iRound(): RectF = iMap(::round)
    /**
     * Ceil rectangle coordinates
     * @return New rectangle
     */
    fun ceil(): RectF = map(::ceil)
    /**
     * Ceil rectangle coordinates
     * @return This rectangle
     */
    fun iCeil(): RectF = iMap(::ceil)
    /**
     * Floor rectangle coordinates
     * @return New rectangle
     */
    fun floor(): RectF = map(::floor)
    /**
     * Floot rectangle coordinates
     * @return This rectangle
     */
    fun iFloor(): RectF = iMap(::floor)
    // }}}

    // {{{ merge
    /**
     * Merge two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    infix fun merge(other: RectF): RectF =
        RectF(
            min(left, other.left),
            min(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    /**
     * Merge two rectangles inplace
     * @param other Other rectangle
     * @return This rectangle
     */
    fun iMerge(other: RectF): RectF =
        iSet(
            min(left, other.left),
            min(top, other.top),
            max(right, other.right),
            max(bottom, other.bottom)
        )
    /**
     * Merge two other rectangles in this
     * @param first First rectangle
     * @param second Second rectangle
     * @return This rectangle
     */
    fun mergeInto(first: RectF, second: RectF): RectF =
        iSet(
            min(first.left, second.left),
            min(first.top, second.top),
            max(first.right, second.right),
            max(first.bottom, second.bottom)
        )
    // }}}

    // {{{ intersect
    /**
     * Intersect two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    infix fun intersect(other: RectF): RectF =
        RectF(
            max(left, other.left),
            max(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    /**
     * Intersect two rectangles
     * @param other Other rectangle
     * @return New rectangle
     */
    infix fun intersect(other: RectI): RectF =
        RectF(
            max(left, other.left.toFloat()),
            max(top, other.top.toFloat()),
            min(right, other.right.toFloat()),
            min(bottom, other.bottom.toFloat())
        )
    /**
     * Intersect two rectangles inplace
     * @param other Other rectangle
     * @return New rectangle
     */
    fun iIntersect(other: RectF): RectF =
        iSet(
            max(left, other.left),
            max(top, other.top),
            min(right, other.right),
            min(bottom, other.bottom)
        )
    /**
     * Intersect two rectangles inplace
     * @param other Other rectangle
     * @return New rectangle
     */
    fun iIntersect(other: RectI): RectF =
        iSet(
            max(left, other.left.toFloat()),
            max(top, other.top.toFloat()),
            min(right, other.right.toFloat()),
            min(bottom, other.bottom.toFloat())
        )
    /**
     * Intersect two other rectangles in this
     * @param first First rectangle
     * @param second Second rectangle
     * @return This rectangle
     */
    fun intersectInto(first: RectF, second: RectF): RectF =
        iSet(
            max(first.left, second.left),
            max(first.top, second.top),
            min(first.right, second.right),
            min(first.bottom, second.bottom)
        )
    // }}}

    // {{{ sort
    /**
     * Sort rectangle coordinates
     * @return New rectangle
     */
    fun sort(): RectF =
        RectF(
            min(left, right),
            min(top, bottom),
            max(left, right),
            max(top, bottom)
        )
    /**
     * Sort rectangle coordinates inplace
     * @return This rectangle
     */
    fun iSort(): RectF =
        iSet(
            min(left, right),
            min(top, bottom),
            max(left, right),
            max(top, bottom)
        )
    // }}}

    // {{{ width/height
    /**
     * Rectangle width
     * @return width
     */
    fun width(): Float = (right - left)
    /**
     * Rectangle height
     * @return height
     */
    fun height(): Float = (bottom - top)
    // }}}

    // {{{ fit
    /**
     * Move rectangle into other horizontally
     * @param other Other rectangle
     * @return New rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun moveIntoHorizontal(other: RectF): RectF {
        val width = width()
        if (width > other.width()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var left = left
        var right = right
        if (left < other.left) {
            left = other.left
            right = left + width
        } else if (right > other.right) {
            right = other.right
            left = right - width
        }
        return RectF(left, top, right, bottom)
    }
    /**
     * Move rectangle into other horizontally inplace
     * @param other Other rectangle
     * @return This rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun iMoveIntoHorizontal(other: RectF): RectF {
        val width = width()
        if (width > other.width()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var left = left
        var right = right
        if (left < other.left) {
            left = other.left
            right = left + width
        } else if (right > other.right) {
            right = other.right
            left = right - width
        }
        return iSet(left, top, right, bottom)
    }
    /**
     * Move rectangle into other vertically
     * @param other Other rectangle
     * @return New rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun moveIntoVertical(other: RectF): RectF {
        val height = height()
        if (height > other.height()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var top = top
        var bottom = bottom
        if (top < other.top) {
            top = other.top
            bottom = top + height
        } else if (bottom > other.bottom) {
            bottom = other.bottom
            top = bottom - height
        }
        return RectF(left, top, right, bottom)
    }
    /**
     * Move rectangle into other vertically inplace
     * @param other Other rectangle
     * @return This rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    infix fun iMoveIntoVertical(other: RectF): RectF {
        val height = height()
        if (height > other.height()) {
            throw IllegalArgumentException("Cannot fit rectangle in smaller rectangle")
        }
        var top = top
        var bottom = bottom
        if (top < other.top) {
            top = other.top
            bottom = top + height
        } else if (bottom > other.bottom) {
            bottom = other.bottom
            top = bottom - height
        }
        return iSet(left, top, right, bottom)
    }
    /**
     * Move rectangle into other
     * @param other Other rectangle
     * @return New rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    fun moveInto(other: RectF): RectF = moveIntoHorizontal(other).iMoveIntoVertical(other)
    /**
     * Move rectangle into other inplace
     * @param other Other rectangle
     * @return This rectangle
     * @throws IllegalArgumentException This rectangle is to big to fit into other
     */
    fun iMoveInto(other: RectF): RectF = iMoveIntoHorizontal(other).iMoveIntoVertical(other)
    // }}}

    // {{{ isInside
    /**
     * Test if rectangle inside other
     * @param other Other rectangle
     * @return Result
     */
    infix fun isInside(other: RectI): Boolean =
        left >= other.left && top >= other.top && right <= other.right && bottom <= other.bottom
    /**
     * Test if rectangle inside other
     * @param other Other rectangle
     * @return Result
     */
    infix fun isInside(other: RectF): Boolean =
        left >= other.left && top >= other.top && right <= other.right && bottom <= other.bottom
    // }}}

    // {{{ points
    /**
     * Get rectangle point
     * @return Point
     */
    fun topLeft(): PointF = PointF(left, top)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun topLeftInto(other: PointF): PointF = other.iSet(left, top)
    /**
     * Get rectangle point
     * @return Point
     */
    fun top(): PointF = PointF((right + left)/2, top)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun topInto(other: PointF): PointF = other.iSet((right + left)/2, top)
    /**
     * Get rectangle point
     * @return Point
     */
    fun topRight(): PointF = PointF(right, top)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun topRightInto(other: PointF): PointF = other.iSet(right, top)
    /**
     * Get rectangle point
     * @return Point
     */
    fun right(): PointF = PointF(right, (bottom + top)/2)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun rightInto(other: PointF): PointF = other.iSet(right, (bottom + top)/2)
    /**
     * Get rectangle point
     * @return Point
     */
    fun bottomLeft(): PointF = PointF(left, bottom)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun bottomLeftInto(other: PointF): PointF = other.iSet(left, bottom)
    /**
     * Get rectangle point
     * @return Point
     */
    fun bottom(): PointF = PointF((right + left)/2, bottom)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun bottomInto(other: PointF): PointF = other.iSet((right + left)/2, bottom)
    /**
     * Get rectangle point
     * @return Point
     */
    fun bottomRight(): PointF = PointF(right, bottom)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun bottomRightInto(other: PointF): PointF = other.iSet(right, bottom)
    /**
     * Get rectangle point
     * @return Point
     */
    fun left(): PointF = PointF(left, (bottom + top)/2)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun leftInto(other: PointF): PointF = other.iSet(left, (bottom + top)/2)
    /**
     * Get rectangle point
     * @return Point
     */
    fun center(): PointF = PointF((left + right)/2, (bottom + top)/2)
    /**
     * Get rectangle point in other
     * @param other Other point
     * @return Other point
     */
    fun centerInto(other: PointF): PointF = other.iSet((left + right)/2, (bottom + top)/2)
    /**
     * Get rectangle point
     * @param selector Corner selector
     * @return Point
     */
    fun point(selector: RectSide): PointF = when (selector) {
        RectSide.TOPLEFT -> topLeft()
        RectSide.TOP -> top()
        RectSide.TOPRIGHT -> topRight()
        RectSide.RIGHT -> right()
        RectSide.BOTTOMRIGHT -> bottomRight()
        RectSide.BOTTOM -> bottom()
        RectSide.BOTTOMLEFT -> bottomLeft()
        RectSide.LEFT -> left()
        RectSide.CENTER -> center()
    }
    /**
     * Get rectangle point in other
     * @param selector Corner selector
     * @param other Other point
     * @return Other point
     */
    fun pointInto(selector: RectSide, other: PointF): PointF = when (selector) {
        RectSide.TOPLEFT -> topLeftInto(other)
        RectSide.TOP -> topInto(other)
        RectSide.TOPRIGHT -> topRightInto(other)
        RectSide.RIGHT -> rightInto(other)
        RectSide.BOTTOMRIGHT -> bottomRightInto(other)
        RectSide.BOTTOM -> bottomInto(other)
        RectSide.BOTTOMLEFT -> bottomLeftInto(other)
        RectSide.LEFT -> leftInto(other)
        RectSide.CENTER -> centerInto(other)
    }
    // }}}

    // {{{ overlap
    /**
     * Check if rectangles overlap
     * @param other Other rectangle
     * @return Result
     */
    infix fun overlap(other: RectI): Boolean =
        !(
            (left < other.left && right < other.left) ||
            (top < other.top && bottom < other.top) ||
            (left > other.right && right > other.right) ||
            (top > other.bottom && bottom > other.bottom)
        )
    /**
     * Check if rectangles overlap
     * @param other Other rectangle
     * @return Result
     */
    infix fun overlap(other: RectF): Boolean =
        !(
            (left < other.left && right < other.left) ||
            (top < other.top && bottom < other.top) ||
            (left > other.right && right > other.right) ||
            (top > other.bottom && bottom > other.bottom)
        )
    // }}}
}
