package org.matsievskiysv.pagerenderview.helpers

import kotlin.collections.ArrayDeque

/**
 * Least Recently Used Cache
 *
 * LRU cache with initialized and destructor hooks
 *
 * @param maxSize Maximum size of cache contents in arbitrary units
 * @param capacity Initial capacity of cache array
 * @param initializer Function to call on cell initialization. Function arguments are label and data object
 * @param destructor Function to call on cell destruction. Function arguments are label and data object
 */
class LRUCache<N, T>(
    private var maxSize: Long,
    private var capacity: Int = 20,
    private val initializer: ((N, T) -> Unit) = { _, _ -> },
    private val destructor: ((N, T) -> Unit) = { _, _ -> }
) {

    /**
     * Cache size counter
     */
    private var totalSize = 0
    /**
     * LRUCache item
     * @property label Unique item label
     * @property size Size of the this item
     * @property obj Pointer to the item data
     */
    internal data class QueueItem<N, T>(val label: N, val size: Int, val obj: T)

    private val queue = ArrayDeque<QueueItem<N, T>>(capacity) //! queue sorted from most to least recently used objects
    /**
     * Push item to cache
     * @param label Label of the item
     * @param obj Data object
     * @param size Size of an object. It is used to determine [maxSize] overflow
     * @param callDestruct Call destruct function on object being removed
     * @throws IllegalArgumentException Cache is too small for object
     */
    public fun push(label: N, obj: T, size: Int = 1, callInit: Boolean = true, callDestruct: Boolean = true) {
        if (size >= maxSize) {
            throw(IllegalArgumentException("Object cannot fit into cache"))
        }
        val index = queue.indexOfFirst { it.label == label }
        totalSize += size
        if (index == -1) {
            if (callInit) {
                initializer.invoke(label, obj)
            }
        } else {
            // label exists. Push to front
            val item = queue.removeAt(index)
            totalSize -= item.size
        }
        queue.addFirst(QueueItem(label, size, obj))
        while (maxSize < totalSize) {
            // pop least recently used items until the new object could fit
            val item = queue[queue.size - 1]
            if (callDestruct) {
                destructor.invoke(item.label, item.obj)
            }
            totalSize -= item.size
            queue.removeLast()
        }
    }
    /**
     * Get from cache. Object is pushed to the beginning of the queue
     * @param label Item label
     * @return Item with label
     */
    public fun get(label: N): T? {
        val index = queue.indexOfFirst { it.label == label }
        if (index == -1) {
            return null
        }
        val item = queue.removeAt(index)
        queue.addFirst(item)
        return item.obj
    }
    /**
     * Pop from cache
     * @param label Item label
     * @param callDestruct Call destructor on popped item
     * @return Item with label
     */
    fun pop(label: N, callDestruct: Boolean = true): T? {
        val index = queue.indexOfFirst { it.label == label }
        if (index == -1) {
            return null
        }
        val item = queue.removeAt(index)
        if (callDestruct) {
            destructor.invoke(item.label, item.obj)
        }
        totalSize -= item.size
        return item.obj
    }
    /**
     * Check object exists
     * @param label Item label
     * @return Object exists
     */
    fun exists(label: N): Boolean = queue.indexOfFirst { it.label == label } != -1
    /**
     * Clear cache
     * @param callDestruct Call destructor on popped item
     */
    fun clear(callDestruct: Boolean = true) {
        if (callDestruct) {
            queue.forEach {
                destructor.invoke(it.label, it.obj)
            }
        }
        totalSize = 0
        queue.clear()
    }
    /**
     * Set cache capacity
     * @param newc New capacity value
     * @param callDestruct Call destructor on popped item
     */
    fun setCapacity(newc: Long, callDestruct: Boolean = true) {
        if (capacity < 1) {
            throw(IllegalArgumentException("Capacity must be positive"))
        }
        maxSize = newc
        while (maxSize < totalSize) {
            val item = queue[queue.size - 1]
            if (callDestruct) {
                destructor.invoke(item.label, item.obj)
            }
            totalSize -= item.size
            queue.removeLast()
        }
    }
    /**
     * Get cache capacity
     * @return Cache capacity
     */
    fun getCapacity(): Int = capacity
    /**
     * Get cache objects
     * @return Cache objects
     */
    fun getObjects(): List<T> = queue.map { it.obj }
    /**
     * Get cache labels
     * @return Cache labels
     */
    fun getLabels(): List<N> = queue.map { it.label }
}
