package org.matsievskiysv.pagerenderview

import android.util.Log

import kotlin.math.*

import org.matsievskiysv.pagerenderview.geometry.*
import org.matsievskiysv.pagerenderview.parcel.ViewRectMapperParcel
import org.matsievskiysv.pagerenderview.helpers.CoordConvert
import org.matsievskiysv.pagerenderview.helpers.Pool
import androidx.core.graphics.scaleMatrix


/**
 * Convert between screen and page coordinate systems.
 * This class holds information about view position in two coordinate systems:
 * screen coordinate system and page coordinate system.
 * It uses [PageLayoutManager] class to calculate page coordinate system positions.
 * @property pageLayoutManager Page coordinate system converter class
 * @property viewSize Size of view
 * @property viewPort Scaled [viewPort]
 * @property docRect Scaled [docRect]
 */
class ViewRectMapper(private val pageLayoutManager: PageLayoutManager,
                     private val viewSize: PointI,
                     viewPort: RectI,
                     docRect: RectI) {
    private val logName = this::class.simpleName

    init {
        pageLayoutManager.onArrange = ::invalidate
    }
    /**
     * [viewPort] coordinate mapping
     */
    private val viewPort = CoordConvert(originalRect=pageLayoutManager.viewPort,
                                        scaledRect=viewPort)
    /**
     * [docRect] document mapping
     */
    private val docRect = CoordConvert(originalRect=pageLayoutManager.docRect,
                                       scaledRect=docRect)
    /**
     * Base scale
     */
    private var base = 1f
    /**
     * Scale down tiles flag
     */
    public var scaleDown = false
    /**
     * Scale down tiles by factor
     */
    public var scaleDownFactor = 1f
        get() = if (scaleDown) {
            field
        } else {
            1f
        }
    /**
     * Zoom scale. Its value is >= 1
     */
    private var zoom = 1f
        set(value) {
            if (value <= 1) {
                field = 1f
            } else if (!zoomMax.isNaN() && value > zoomMax ) {
                field = zoomMax
            } else {
                field = value
            }
            zoomQuantized = floor(field / zoomStep) * zoomStep
        }
    /**
     * Zoom scale max
     */
    public var zoomMax = 5f
    /**
     * Quantized zoom step. When changing this value one needs to flush cached bitmaps
     * because existing tile IDs will become invalid
     */
    public var zoomStep = 1f
    /**
     * Quantized zoom. Bitmaps are drawn with this scale applied
     */
    private var zoomQuantized = 1f
    /**
     * Crop pages
     */
    public var crop = false
        set(value) {
            field = value
            pageLayoutManager.crop = value
            invalidate()
        }
    /**
     * Page padding.
     */
    public var padding = RectI()
        set(value) {
            field = value
            pageLayoutManager.padding = value
            invalidate()
        }
        get() = pageLayoutManager.padding
    /**
     * Page flow direction.
     */
    public var pageFlow = Direction.VERTICAL
        set(value) {
            field = value
            pageLayoutManager.pageFlow = value
            invalidate()
        }
        get() = pageLayoutManager.pageFlow
    /**
     * Left to right.
     */
    public var leftToRight = true
        set(value) {
            field = value
            pageLayoutManager.leftToRight = value
            invalidate()
        }
        get() = pageLayoutManager.leftToRight
    /**
     * Left to right.
     */
    public var topToBottom = true
        set(value) {
            field = value
            pageLayoutManager.topToBottom = value
            invalidate()
        }
        get() = pageLayoutManager.topToBottom
    /**
     * Number of columns.
     */
    public var columns = 1
        set(value) {
            field = value
            pageLayoutManager.columns = value
            pageLayoutManager.gridDirection = Direction.VERTICAL
            invalidate()
        }
        get() = pageLayoutManager.columns
    /**
     * Number of rows.
     */
    public var rows = 1
        set(value) {
            field = value
            pageLayoutManager.rows = value
            pageLayoutManager.gridDirection = Direction.HORIZONTAL
            invalidate()
        }
        get() = pageLayoutManager.rows
    /**
     * On invalidate change hook
     */
    public var onInvalidateTiles: () -> Unit = {}
    /**
     * Force visible tiles recalculation
     */
    public fun invalidate() {
        Log.d(logName, "invalidate")
        updateSize()
        scaleIdSaved = -1 // in order to trigger updatePosition block
    }
    /**
     * Update size
     */
    public fun updateSize() {
        Log.d(logName, "update size ${viewSize}")
        pageLayoutManager.arrange()
        val newBase = pageLayoutManager.getScale(viewSize)
        if (newBase != base) {
            onInvalidateTiles()
            base = newBase
        }
        Log.d(logName, "base scale ${base}")
        rescale()
    }
    /**
     * Rescale coordinate systems
     */
    private fun rescale() {
        viewPort.rescaleFromOriginal(base * zoom)
        viewPort.cropScaled(viewSize)
        docRect.rescaleFromOriginal(base * zoom)
        docRect.updateScaled()
        Log.v(logName, "coordinate systems ${viewPort} ${docRect}")
        moveIntoDoc()
        splitTiles()
    }
    /**
     * Move rectangles into document boundaries
     */
    private fun moveIntoDoc() {
        pageLayoutManager.moveIntoDoc()
        viewPort.updateScaled()
    }
    /**
     * Reset position
     */
    public fun resetPosition() {
        pageLayoutManager.viewPort.iSet(0, 0, pageLayoutManager.viewPort.width(), pageLayoutManager.viewPort.height())
        viewPort.updateScaled()
    }
    /**
     * Convert point from [viewRect] coordinate system to page coordinate system
     * i.e. translate point to position on page.
     * @param point Coordinate of point. Page coordinate will be set in this object in place if there's page at point.
     * Otherwise it will be set to zeros.
     * @return Page at point or null.
     */
    public fun getPositionOnPage(point: PointI): Int? {

        point.iSet(round((point.x.toFloat() + viewPort.scaledRect.left) / viewPort.scale).toInt(),
                   round((point.y + viewPort.scaledRect.top) / viewPort.scale).toInt())
        return pageLayoutManager.getPositionOnPage(point)
    }

    // {{{ Movement
    /**
     * Move to page and position
     * @param page Page number
     * @param horizontalPosition Horizontal position
     * @param verticalPosition Vertical position
     */
    public fun moveTo(page: Int, horizontalPosition: Int, verticalPosition: Int) {
        pageLayoutManager.moveToPage(page, horizontalPosition, verticalPosition)
        moveIntoDoc()
    }
    /**
     * Move to page and position
     * @param page Page number
     */
    public fun moveTo(page: Int) {
        pageLayoutManager.moveToPage(page)
        moveIntoDoc()
    }
    /**
     * Move to original system coordinate
     * @param posX Horizontal position
     * @param posY Vertical position
     */
    public fun moveOriginalTo(posX: Int, posY: Int) {
        viewPort.moveOriginalTo(posX, posY)
        moveIntoDoc()
    }
    /**
     * Move to horizontal position
     * @param pos Document position
     */
    public fun moveToHorizontal(pos: Int) {
        viewPort.moveScaledTo(pos, viewPort.scaledRect.top)
        moveIntoDoc()
    }
    /**
     * Move to vertical position
     * @param pos Document position
     */
    public fun moveToVertical(pos: Int) {
        viewPort.moveScaledTo(viewPort.scaledRect.left, pos)
        moveIntoDoc()
    }
    /**
     * Scroll page
     * @param deltaX X offset
     * @param deltaY Y offset
     */
    public fun scroll(deltaX: Int, deltaY: Int) {
        viewPort.moveScaled(deltaX, deltaY)
        moveIntoDoc()
    }
    // }}}

    /**
     * Scale page
     * @param focusX Position of x focus
     * @param focusY Position of y focus
     * @param immediateZoom Immediate zoom value. This value is a ratio between current and previous zoom level
     */
    public fun scale(focusX: Float, focusY: Float, immediateZoom: Float) {
        zoom *= immediateZoom
        Log.v(logName, "new zoom value ${zoom} with focus ${focusX}:${focusY}")
        val translateX = round(focusX * (immediateZoom - 1)).toInt()
        val translateY = round(focusY * (immediateZoom - 1)).toInt()
        viewPort.moveScaled(translateX, translateY)
        rescale()
    }
    /**
     * Reset page scale to value
     * @param newZoom New zoom value.
     */
    public fun resetScale(newZoom: Float) {
        zoom = newZoom
        Log.v(logName, "new zoom value ${zoom}")
        rescale()
    }
    /**
     * Reset page scale
     */
    public fun resetScale() = resetScale(1f)
    /**
     * Recalculate number of page tiles
     */
    public fun splitTiles() {
        pageLayoutManager.tiles.iSet(
            floor(max(1f, (pageLayoutManager.maxCol * scale).toFloat() / viewSize.x)).toInt(),
            floor(max(1f, (pageLayoutManager.maxRow * scale).toFloat() / viewSize.y)).toInt()
        )
        Log.d(logName, "split tiles ${pageLayoutManager.tiles}")
    }

    private var scaleIdSaved = -1 // saved value of quantized zoom
    private var cropSaved = pageLayoutManager.crop // saved value of crop
    /**
     * Update page position in internal classes lazily
     * @param onRecalculate Run function if position is changed
     */
    public fun updatePosition(onRecalculate: () -> Unit) {
        Log.v(logName, "update position request")
        if (!viewPort.originalRect.isInside(pageLayoutManager.visibleRect) ||
	        scaleId != scaleIdSaved || pageLayoutManager.crop != cropSaved) {
            Log.v(logName, "update position")
            scaleIdSaved = scaleId
            cropSaved = pageLayoutManager.crop
            moveIntoDoc()
            pageLayoutManager.calculate()
            onRecalculate()
        }
    }

    // {{{ Scale
    /**
     * Get tile scale
     * Tile scale describes bitmap scaling
     * @return Tile scale
     */
    public var tileScale: Float = 1f
        get() = base * zoomQuantized / scaleDownFactor
    /**
     * Get tile relative zoom
     * This value is relative to the tile scale and describes
     * how much scaled bitmap needs to be scaled additionally to match zoom level
     * @return Tile relative zoom
     */
    public var tileRelativeZoom: Float = 1f
        get() = zoom * scaleDownFactor / zoomQuantized
    /**
     * Get scale integer representation
     * @return Tile ID
     */
    public var scaleId: Int = 1
        get() = floor(if (scaleDown) { -1 } else { 1 } * (zoomQuantized - 1) / zoomStep).toInt()
    /**
     * Get scale
     * @return Coordinate scale
     */
    public var scale: Float = 1f
        get() = viewPort.scale
    // }}}
    /**
     * Get current page
     * @return Current page
     */
    public fun getCurrentPage(): Int = pageLayoutManager.getCurrentPage()
    /**
     * Get parcel.
     * Needed for save/restore functions.
     * @return Current coordinates
     */
    // public fun getParcel(): ViewRectMapperParcel {
    //     val sp = pageLayoutManager.writeToParcel()
    //     sp.zoom = zoom
    //     sp.zoomMax = zoomMax
    //     return sp
    // }
    /**
     * Apply parcel.
     * Needed for save/restore functions.
     * @return Current coordinates
     */
    // public fun applyParcel(stateParcel: StateParcel) {
    //     pageLayoutManager.readFromParcel(stateParcel)
    //     if (stateParcel.zoomMax != null) {
    //         zoomMax = stateParcel.zoomMax!!
    //     }
    //     invalidate()
    // }
    /**
     * Execute action with page tiles
     * @param action Execute function for each visible tile. Function argument is [PageLayoutManager.PageTile]
     */
    public fun withTiles(action: (PageLayoutManager.PageTile) -> Unit) {
        pageLayoutManager.pageTiles.forEach(action)
    }
    /**
     * Write state to parcel.
     * @param parcel State parcel
     */
    public fun writeToParcel(parcel: ViewRectMapperParcel) : ViewRectMapperParcel {
        parcel.scaleDown = scaleDown
        parcel.scaleDownFactor = scaleDownFactor
        parcel.zoom = zoom
        parcel.zoomMax = zoomMax
        // parcel.crop = crop
        return parcel
    }
    /**
     * Write state to parcel.
     */
    public fun writeToParcel() : ViewRectMapperParcel = writeToParcel(ViewRectMapperParcel())
    /**
     * Read state from parcel.
     * @param parcel State parcel
     */
    public fun readFromParcel(parcel: ViewRectMapperParcel) {
        scaleDown = parcel.scaleDown
        scaleDownFactor = parcel.scaleDownFactor
        zoom = parcel.zoom
        zoomMax = parcel.zoomMax
        // crop = parcel.crop
    }
}
