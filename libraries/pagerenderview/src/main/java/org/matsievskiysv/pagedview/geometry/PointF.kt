package org.matsievskiysv.pagerenderview.geometry

import kotlin.math.*

/**
 * Floating point geometry point
 *
 * Inplace method versions are prepended with letter «i»
 *
 * @constructor Create new point
 * @property x X coordinate
 * @property y Y coordinate
 * @param x X coordinate
 * @param y Y coordinate
 */
public class PointF(var x: Float, var y: Float) {
    /**
     * Create point at (0,0)
     */
    constructor() : this(0f, 0f)
    /**
     * Copy point
     * @param other Other point
     */
    constructor(other: PointF) : this(other.x, other.y)
    /**
     * Copy point
     * @param other Other point
     */
    constructor(other: PointI) : this(other.x.toFloat(), other.y.toFloat())
    /**
     * Copy point
     * @param other Other point
     */
    constructor(other: Pair<Float, Float>) : this(other.first, other.second)
    /**
     * Get string representation
     * @return String representation
     */
    override fun toString(): String {
        return "PointF[$x;$y]"
    }
    /**
     * Compare points
     * @param other Other point
     * @return Comparison result
     */
    override operator fun equals(other: Any?): Boolean {
        return when (other) {
            is PointF -> x == other.x && y == other.y
            else -> super.equals(other)
        }
    }
    /**
     * Return X coordinate
     * @return X coordinate
     */
    operator fun component1(): Float = x
    /**
     * Return Y coordinate
     * @return Y coordinate
     */
    operator fun component2(): Float = y
    /**
     * Set coordinates inplace
     * @param first X coordinate
     * @param second Y coordinate
     * @return This point
     */
    fun iSet(first: Float, second: Float): PointF =
        this.apply {
            x = first
            y = second
        }
    /**
     * Set coordinates inplace
     * @param other Other point
     * @return This point
     */
    fun iSet(other: PointF): PointF =
        this.apply {
            x = other.x
            y = other.y
        }
    /**
     * Clip point coordinates between two coordinates
     * @param start Start coordinate
     * @param end End coordinate
     * @return New point
     */
    fun clip(start: Float, end: Float): PointF =
        PointF(
            clipnum(x, start, end),
            clipnum(y, start, end)
        )
    /**
     * Clip point coordinates between two points inplace
     * @param start Start point
     * @param end End point
     * @return This point
     */
    fun iClip(start: Float, end: Float): PointF =
        iSet(clipnum(x, start, end), clipnum(y, start, end))
    /**
     * Clip point coordinates between two coordinates
     * @param start Start point
     * @param end End point
     * @return New point
     */
    fun clip(start: PointF, end: PointF): PointF =
        PointF(
            clipnum(x, start.x, end.x),
            clipnum(y, start.y, end.y)
        )
    /**
     * Clip point coordinates between two points inplace
     * @param start Start point
     * @param end End point
     * @return This point
     */
    fun iClip(start: PointF, end: PointF): PointF =
        iSet(clipnum(x, start.x, end.x), clipnum(y, start.y, end.y))
    /**
     * Apply function to coordinates
     * @param func Function to apply
     * @return New point
     */
    fun map(func: (Float) -> Float): PointF = PointF(func(x), func(y))
    /**
     * Apply function to coordinates inplace
     * @param func Function to apply
     * @return This point
     */
    fun iMap(func: (Float) -> Float): PointF =
        iSet(func(this.x), func(this.y))

    // {{{ plus
    /**
     * Add points
     * @param other Scalar value
     * @return New point
     */
    operator fun plus(other: Float): PointF = PointF(x + other, y + other)
    /**
     * Add points
     * @param other Scalar value
     * @return New point
     */
    operator fun plus(other: Int): PointF = PointF(x + other, y + other)
    /**
     * Add points inplace
     * @param other Scalar value
     */
    operator fun plusAssign(other: Float) {
        x += other
        y += other
    }
    /**
     * Add points inplace
     * @param other Scalar value
     */
    operator fun plusAssign(other: Int) {
        x += other
        y += other
    }
    /**
     * Add points
     * @param other Other point
     * @return New point
     */
    operator fun plus(other: PointF): PointF = PointF(x + other.x, y + other.y)
    /**
     * Add points
     * @param other Other point
     * @return New point
     */
    operator fun plus(other: PointI): PointF = PointF(x + other.x, y + other.y)
    /**
     * Add points inplace
     * @param other Other point
     */
    operator fun plusAssign(other: PointF) {
        x += other.x
        y += other.y
    }
    /**
     * Add points inplace
     * @param other Other point
     */
    operator fun plusAssign(other: PointI) {
        x += other.x
        y += other.y
    }
    // }}}

    // {{{ minus number
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Float): PointF = PointF(x - other, y - other)
    /**
     * Subtract points
     * @param other Scalar value
     * @return New point
     */
    operator fun minus(other: Int): PointF = PointF(x - other, y - other)
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: Float) {
        x -= other
        y -= other
    }
    /**
     * Subtract points inplace
     * @param other Scalar value
     */
    operator fun minusAssign(other: Int) {
        x -= other
        y -= other
    }
    /**
     * Subtract points
     * @param other Other point
     * @return New point
     */
    operator fun minus(other: PointF): PointF = PointF(x - other.x, y - other.y)
    /**
     * Subtract points
     * @param other Other point
     * @return New point
     */
    operator fun minus(other: PointI): PointF = PointF(x - other.x, y - other.y)
    /**
     * Subtract points inplace
     * @param other Other point
     */
    operator fun minusAssign(other: PointF) {
        x -= other.x
        y -= other.y
    }
    /**
     * Subtract points inplace
     * @param other Other point
     */
    operator fun minusAssign(other: PointI) {
        x -= other.x
        y -= other.y
    }
    // }}}

    // {{{ times number
    /**
     * Multiply points
     * @param other Scalar value
     * @return New point
     */
    operator fun times(other: Float): PointF = PointF(x * other, y * other)
    /**
     * Multiply points
     * @param other Scalar value
     * @return New point
     */
    operator fun times(other: Int): PointF = PointF(x * other, y * other)
    /**
     * Multiply points inplace
     * @param other Scalar value
     */
    operator fun timesAssign(other: Float) {
        x *= other
        y *= other
    }
    /**
     * Multiply points inplace
     * @param other Scalar value
     */
    operator fun timesAssign(other: Int) {
        x *= other
        y *= other
    }
    /**
     * Multiply points
     * @param other Other point
     * @return New point
     */
    operator fun times(other: PointF): PointF = PointF(x * other.x, y * other.y)
    /**
     * Multiply points
     * @param other Other point
     * @return New point
     */
    operator fun times(other: PointI): PointF = PointF(x * other.x, y * other.y)
    /**
     * Multiply points inplace
     * @param other Other point
     */
    operator fun timesAssign(other: PointF) {
        x *= other.x
        y *= other.y
    }
    /**
     * Multiply points inplace
     * @param other Other point
     */
    operator fun timesAssign(other: PointI) {
        x *= other.x
        y *= other.y
    }
    // }}}

    // {{{ div number
    /**
     * Divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun div(other: Float): PointF = PointF(x / other, y / other)
    /**
     * Divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun div(other: Int): PointF = PointF(x / other, y / other)
    /**
     * Divide points inplace
     * @param other Scalar value
     */
    operator fun divAssign(other: Float) {
        x /= other
        y /= other
    }
    /**
     * Divide points inplace
     * @param other Scalar value
     */
    operator fun divAssign(other: Int) {
        x /= other
        y /= other
    }
    /**
     * Divide points
     * @param other Other point
     * @return New point
     */
    operator fun div(other: PointF): PointF = PointF(x / other.x, y / other.y)
    /**
     * Divide points
     * @param other Other point
     * @return New point
     */
    operator fun div(other: PointI): PointF = PointF(x / other.x, y / other.y)
    /**
     * Divide points inplace
     * @param other Other point
     */
    operator fun divAssign(other: PointF) {
        x /= other.x
        y /= other.y
    }
    /**
     * Divide points inplace
     * @param other Other point
     */
    operator fun divAssign(other: PointI) {
        x /= other.x
        y /= other.y
    }
    // }}}

    // {{{ rem number
    /**
     * Whole divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun rem(other: Float): PointF = PointF(x % other, y % other)
    /**
     * Whole divide points
     * @param other Scalar value
     * @return New point
     */
    operator fun rem(other: Int): PointF = PointF(x % other, y % other)
    /**
     * Whole divide points inplace
     * @param other Scalar value
     */
    operator fun remAssign(other: Float) {
        x %= other
        y %= other
    }
    /**
     * Whole divide points inplace
     * @param other Scalar value
     */
    operator fun remAssign(other: Int) {
        x %= other
        y %= other
    }
    /**
     * Whole divide points
     * @param other Other point
     * @return New point
     */
    operator fun rem(other: PointF): PointF = PointF(x % other.x, y % other.y)
    /**
     * Whole divide points
     * @param other Other point
     * @return New point
     */
    operator fun rem(other: PointI): PointF = PointF(x % other.x, y % other.y)
    /**
     * Whole divide points inplace
     * @param other Other point
     */
    operator fun remAssign(other: PointF) {
        x %= other.x
        y %= other.y
    }
    /**
     * Whole divide points inplace
     * @param other Other point
     */
    operator fun remAssign(other: PointI) {
        x %= other.x
        y %= other.y
    }
    // }}}

    // {{{ min
    /**
     * Get minimum coordinates of two points
     * @param other Other point
     * @return New point
     */
    infix fun min(other: PointF): PointF = PointF(min(x, other.x), min(y, other.y))
    /**
     * Get minimum coordinates of two points inplace
     * @param other Other point
     * @return This point
     */
    infix fun iMin(other: PointF): PointF = iSet(min(x, other.x), min(y, other.y))
    // }}}

    // {{{ max
    /**
     * Get maximum coordinates of two points
     * @param other Other point
     * @return New point
     */
    infix fun max(other: PointF): PointF = PointF(max(x, other.x), max(y, other.y))
    /**
     * Get maximum coordinates of two points inplace
     * @param other Other point
     * @return This point
     */
    infix fun iMax(other: PointF): PointF = iSet(max(x, other.x), max(y, other.y))
    // }}}

    // {{{ round
    /**
     * Round point coordinates
     * @return New point
     */
    fun round(): PointF = map(::round)
    /**
     * Round point coordinates
     * @return This point
     */
    fun iRound(): PointF = iMap(::round)
    /**
     * Ceil point coordinates
     * @return New point
     */
    fun ceil(): PointF = map(::ceil)
    /**
     * Ceil point coordinates
     * @return This point
     */
    fun iCeil(): PointF = iMap(::ceil)
    /**
     * Floor point coordinates
     * @return New point
     */
    fun floor(): PointF = map(::floor)
    /**
     * Floot point coordinates
     * @return This point
     */
    fun iFloor(): PointF = iMap(::floor)
    // }}}

    // {{{ isInside
    /**
     * Check if point is inside rectangle
     * @param other Rectangle
     * @return Result
     */
    infix fun isInside(other: RectI): Boolean =
        x >= other.left && y >= other.top && x <= other.right && y <= other.bottom
    /**
     * Check if point is inside rectangle
     * @param other Rectangle
     * @return Result
     */
    infix fun isInside(other: RectF): Boolean =
        x >= other.left && y >= other.top && x <= other.right && y <= other.bottom
    // }}}
}
