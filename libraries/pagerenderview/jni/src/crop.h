#pragma once

#include <jni.h>

JNIEXPORT void JNICALL cropConfigure(JNIEnv *env, jclass this, jint bg_color, jint bg_color_tolerance, jobject margins);

JNIEXPORT void JNICALL getCropBounds(JNIEnv *env, jclass this, jobject targetRect, jobject bufferSize, jobject pixels);
