#pragma once

#include <jni.h>

typedef struct PointI {
	int x;
	int y;
} PointI;

void   PointIRef_init(JNIEnv *env);
void   PointIRef_free(JNIEnv *env);
void   PointI_set(JNIEnv *env, jobject point, PointI val);
PointI PointI_get(JNIEnv *env, jobject point);

typedef struct RectI {
	int left;
	int top;
	int right;
	int bottom;
} RectI;

void  RectIRef_init(JNIEnv *env);
void  RectIRef_free(JNIEnv *env);
void  RectI_set(JNIEnv *env, jobject rect, RectI val);
RectI RectI_get(JNIEnv *env, jobject rect);
