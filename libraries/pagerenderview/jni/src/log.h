#include <android/log.h>

#define LCTX "PageLoader"

#define DEBUG(args...) __android_log_print(ANDROID_LOG_DEBUG, LCTX, args)

#define ERROR(args...) __android_log_print(ANDROID_LOG_ERROR, LCTX, args)

#define INFO(args...) __android_log_print(ANDROID_LOG_INFO, LCTX, args)
