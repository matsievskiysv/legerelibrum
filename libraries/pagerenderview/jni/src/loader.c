#include "crop.h"
#include "geometry.h"
#include "log.h"

#include <jni.h>

JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM *vm, __attribute__((unused)) void *reserved)
{
	JNIEnv *env;
	if ((*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_4) != JNI_OK) {
		return JNI_ERR;
	}

	DEBUG("Initializing PageLoader JNI library");

	PointIRef_init(env);
	RectIRef_init(env);

	jclass PageLoader = (*env)->FindClass(env, "org/matsievskiysv/pagerenderview/PageLoader");
	if (!PageLoader) {
		return JNI_ERR;
	}

	static const JNINativeMethod PageLoader_methods[] = {
	    {"getCropBounds",
	     "("
	     "Lorg/matsievskiysv/pagerenderview/geometry/RectI;"
	     "Lorg/matsievskiysv/pagerenderview/geometry/PointI;"
	     "Ljava/nio/ByteBuffer;"
	     ")V",
	     (void *) (getCropBounds)},
	    {"cropConfigure",
	     "("
	     "I"
	     "I"
	     "Lorg/matsievskiysv/pagerenderview/geometry/RectI;"
	     ")V",
	     (void *) (cropConfigure)},
	};

	if ((*env)->RegisterNatives(env, PageLoader, PageLoader_methods,
				    sizeof(PageLoader_methods) / sizeof(JNINativeMethod)) != JNI_OK) {
		return JNI_ERR;
	}

	DEBUG("Initialized PageLoader JNI library");
	return JNI_VERSION_1_4;
}

JNIEXPORT void JNICALL
JNI_OnUnload(JavaVM *vm, __attribute__((unused)) void *reserved)
{
	JNIEnv *env;
	(*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_4);

	PointIRef_free(env);
	RectIRef_free(env);

	DEBUG("Unloading PageLoader JNI library");
}
