#include "crop.h"

#include "geometry.h"
#include "log.h"

#include <stdint.h>

#define MAXTOLERANCE 80

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

struct CropConfig {
	/* alpha_min is always = 0x00 */
	uint8_t alpha_max;
	uint8_t red_min;
	uint8_t red_max;
	uint8_t green_min;
	uint8_t green_max;
	uint8_t blue_min;
	uint8_t blue_max;
	RectI	margins;
};

struct CropConfig cropConfig = {
    /* white with 5% tolerance */
    .alpha_max = 0x01, .red_min = 0xf9,	 .red_max = 0xff,  .green_min = 0xf9,
    .green_max = 0xff, .blue_min = 0xf9, .blue_max = 0xff, .margins = {5, 5, 5, 5}};

JNIEXPORT void JNICALL
cropConfigure(JNIEnv *env, __attribute__((unused)) jclass this, jint bg_color, jint bg_color_tolerance, jobject margins)
{
	uint8_t red	     = bg_color & (0xff << 16);
	uint8_t green	     = bg_color & (0xff << 8);
	uint8_t blue	     = bg_color & 0xff;
	uint8_t tol	     = 255 * MIN(bg_color_tolerance, MAXTOLERANCE) / 200;
	cropConfig.alpha_max = 2 * tol;
	cropConfig.red_min   = MAX(0, red + tol);
	cropConfig.red_max   = MIN(0xff, red - tol);
	cropConfig.green_min = MAX(0, green + tol);
	cropConfig.green_max = MIN(0xff, green - tol);
	cropConfig.blue_min  = MAX(0, blue + tol);
	cropConfig.blue_max  = MIN(0xff, blue - tol);

	RectI mar		  = RectI_get(env, margins);
	cropConfig.margins.left	  = mar.left;
	cropConfig.margins.top	  = mar.top;
	cropConfig.margins.right  = mar.right;
	cropConfig.margins.bottom = mar.bottom;
}

JNIEXPORT void JNICALL
getCropBounds(JNIEnv *env, __attribute__((unused)) jclass this, jobject targetRect, jobject bufferSize, jobject pixels)
{
	uint8_t *pixBuf = (uint8_t *) ((*env)->GetDirectBufferAddress(env, pixels));
	if (!pixBuf) {
		ERROR("Can not get direct buffer");
		return;
	}
	PointI size = PointI_get(env, bufferSize);

	int left   = size.x;
	int top	   = size.y;
	int right  = 0;
	int bottom = 0;
	for (int row = 0; row < size.x; row++) {
		int found_left = 0;
		int found_top  = 0;
		for (int col = 0; col < size.x; col++) {
			/* Use nested loops to utilize cache locality */
			uint8_t alpha = pixBuf[4 * (col + size.x * row)];
			uint8_t red   = pixBuf[4 * (col + size.x * row) + 1];
			uint8_t green = pixBuf[4 * (col + size.x * row) + 2];
			uint8_t blue  = pixBuf[4 * (col + size.x * row) + 3];
			if ((alpha > cropConfig.alpha_max) &&
			    ((cropConfig.red_min > red || red > cropConfig.red_max) ||
			     (cropConfig.green_min > green || green > cropConfig.green_max) ||
			     (cropConfig.blue_min > blue || blue > cropConfig.blue_max))) {
				if (found_left) {
					right = MAX(right, col);
				} else {
					left	   = MIN(left, col);
					found_left = 1;
				}
				if (found_top) {
					bottom = MAX(bottom, row);
				} else {
					top	  = MIN(top, row);
					found_top = 1;
				}
			}
		}
	}

	RectI r = {MAX(0, left - cropConfig.margins.left), MAX(0, top - cropConfig.margins.top),
		   MAX(0, size.x - 1 - right - cropConfig.margins.right),
		   MAX(0, size.y - 1 - bottom - cropConfig.margins.bottom)};
	RectI_set(env, targetRect, r);
}
