LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

SRC_DIR := $(LOCAL_PATH)/src

LOCAL_MODULE := pagerenderview
LOCAL_CPP_EXTENSION := .cpp

LOCAL_WARNINGS := -Wall -Wextra -pedantic -Wno-variadic-macros
LOCAL_SRC_FILES := $(wildcard $(SRC_DIR)/*.c)
LOCAL_C_INCLUDES := $(SRC_DIR)
LOCAL_CFLAGS := $(LOCAL_WARNINGS)
LOCAL_CPPFLAGS := $(LOCAL_WARNINGS)

LOCAL_LDLIBS := -llog -lz

include $(BUILD_SHARED_LIBRARY)
