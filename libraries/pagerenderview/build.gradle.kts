import org.gradle.api.JavaVersion

plugins {
    id("com.android.library")
    kotlin("android")
    id("org.jetbrains.dokka") version Versions.dokka
}

android {
    namespace = "org.matsievskiysv.pagerenderview"
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)
    ndkVersion = AppConfig.ndkVersion

    kotlinOptions {
        jvmTarget = Versions.jvm
    }

    compileOptions {
        sourceCompatibility = JavaVersion.values().filter { it.toString() == Versions.jvm } .first()
        targetCompatibility = JavaVersion.values().filter { it.toString() == Versions.jvm } .first()
    }

    defaultConfig {
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)

        testInstrumentationRunner = AppConfig.androidTestInstrumentation
        consumerProguardFiles("consumer-rules.pro")

        externalNativeBuild {
            ndkBuild {
                arguments("-j${gradle.startParameter.getMaxWorkerCount()}")
            }
        }
    }

    externalNativeBuild {
        ndkBuild {
            path = File("$projectDir/jni/Android.mk")
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("debug") {
            isMinifyEnabled = false
        }
    }

    packagingOptions {
        resources.excludes.add("/META-INF/*")
    }

    splits {
        abi {
            isEnable = true
            reset()
            include("armeabi-v7a", "arm64-v8a", "x86", "x86_64")
            isUniversalApk = false
        }
    }

    lintOptions {
        isAbortOnError = AppConfig.lintAbortOnError
        isWarningsAsErrors = AppConfig.lintWarningsAsErrors
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs",
                                  "include" to listOf("*.jar"))))
    // app libs
    implementation(AppDependencies.sharedLibraries)
    implementation(AppDependencies.libLibraries)
    //test libs
    testImplementation(AppDependencies.testLibraries)
    androidTestImplementation(AppDependencies.androidTestLibraries)
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("dokka"))
    suppressObviousFunctions.set(true)
    suppressInheritedMembers.set(true)
    dokkaSourceSets {
        configureEach {
            includeNonPublic.set(false)
            reportUndocumented.set(true)
        }
    }
}
