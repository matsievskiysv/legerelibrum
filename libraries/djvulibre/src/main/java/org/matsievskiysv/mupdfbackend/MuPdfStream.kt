package org.matsievskiysv.mupdfbackend

import java.nio.channels.FileChannel

import java.nio.ByteBuffer
import java.io.IOException

import com.artifex.mupdf.fitz.SeekableInputStream
import com.artifex.mupdf.fitz.SeekableOutputStream

/*
 *
 */
class MuPdfReadOnlyStream(private val file: FileChannel): SeekableInputStream {

    override fun seek(offset: Long, whence: Int): Long {
        when (whence) {
            0 -> {
                // SEEK_SET set file position to offset
                file.position(offset)
                return offset
            }
            1 -> {
                // SEEK_CUR set file position to current location plus offset
                val position = file.position() + offset
                file.position(position)
                return position
            }
            2 -> {
                // SEEK_END set file position to EOF plus offset
                val position = file.size() + offset
                file.position(position)
                return position
            }
            else -> throw IOException("Unknown seek mode")
        }
    }

    override fun position(): Long {
        return file.position()
    }

    override fun read(b: ByteArray): Int {
        val buffer = ByteBuffer.wrap(b)
        return file.read(buffer)
    }
}
