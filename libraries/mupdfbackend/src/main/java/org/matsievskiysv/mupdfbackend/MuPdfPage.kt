package org.matsievskiysv.mupdfbackend

import android.graphics.Bitmap
import android.graphics.Paint

import com.artifex.mupdf.fitz.Page as FPage
import com.artifex.mupdf.fitz.PDFPage as PDFPage
import com.artifex.mupdf.fitz.Matrix
import com.artifex.mupdf.fitz.android.AndroidDrawDevice

import android.util.Log

import org.matsievskiysv.pagerenderview.interfaces.Page
import org.matsievskiysv.pagerenderview.interfaces.Overlay
import org.matsievskiysv.pagerenderview.geometry.*

class MuPdfPage(pageNum: Int, page: FPage): Page(pageNum) {

    private val page = page as PDFPage

    override val pageSize: PointI

    init {
        val bounds = RectI(RectF(page.getBounds()).round())
        pageSize = PointI(bounds.width(), bounds.height())
    }

    override fun close() = page.destroy()

    override fun putCrossRefs(overlay: Overlay) {
        val links = page.links?.filter {
            !it.isExternal()
        } ?.map {
            val (page, x, y) = it.uri.trim('#').split(',', limit=3)
            CrossReference(pageNum, pageSize, RectI(RectF(it.bounds).round()),
                     page.toInt(), PointI(x.toInt(), y.toInt()), overlay as ReferenceOverlay)
        }
        if (links != null) {
            overlay.items[pageNum] = links
        }
    }

    override fun putExternalLinks(overlay: Overlay) {
        val links = page.links?.filter {
            it.isExternal()
        } ?.map {
            ExternalLink(pageNum, pageSize, RectI(RectF(it.bounds).round()), it.uri, overlay as ReferenceOverlay)
        }
        if (links != null) {
            overlay.items[pageNum] = links
        }
    }

    override fun draw(bitmap: Bitmap, cropLeft: Float, cropTop: Float, cropRight: Float, cropBottom: Float) {
        val ctm = Matrix()
        val sizeX = bitmap.getWidth()
        val sizeY = bitmap.getHeight()
        val scaleX = sizeX.toFloat() / (cropRight - cropLeft) / pageSize.x
        val scaleY = sizeY.toFloat() / (cropBottom - cropTop) / pageSize.y
        ctm.scale(scaleX, scaleY)                                   // scale whole page to fit
        ctm.e = -cropLeft * pageSize.x * scaleX                     // move slice to the left
        ctm.f = -cropTop * pageSize.y * scaleY                      // move slice to the up
        val dev = AndroidDrawDevice(bitmap, 0, 0, 0, 0, sizeX, sizeY)
        page.run(dev, ctm, null);
        dev.close();
        dev.destroy();
    }
}
