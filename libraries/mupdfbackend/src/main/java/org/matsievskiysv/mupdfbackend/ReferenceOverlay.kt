package org.matsievskiysv.mupdfbackend

import android.graphics.Canvas
import android.graphics.Paint

import org.matsievskiysv.pagerenderview.interfaces.Overlay

/**
 * Reference overlay. This overlay represents [CrossReference] and [ExternalLink] groups
 * and holds shared [Paint] and [Canvas] instances for them.
 * @param tag Name resource tag
 * @property canvas Canvas object
 * @property paint Group paint
 */
class ReferenceOverlay(tag: Int): Overlay(tag) {
    public var canvas = Canvas()
    public var paint = Paint().apply {
        setColor(( (0x8f shl 24) or // alpha
                   (0xdd shl 16) or // red
                   (0x88 shl 8) or // green
                   (0x44)))
    }
}
