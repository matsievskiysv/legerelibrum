// https://medium.com/android-dev-hacks/kotlin-dsl-gradle-scripts-in-android-made-easy-b8e2991e2ba

import org.gradle.api.artifacts.dsl.DependencyHandler

object AppDependencies {
    // std lib
    private val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"

    // kotlin libs
    private val kotlinCoroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlinCoroutines}"
    private val kotlinReflections = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlinReflections}"
    private val serializationCbor = "org.jetbrains.kotlinx:kotlinx-serialization-cbor:${Versions.serializationCbor}"
    private val dokka = "org.jetbrains.dokka:dokka-gradle-plugin:${Versions.dokka}"

    // android
    private val activity = "androidx.activity:activity:${Versions.activity}"
    private val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    private val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    private val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    private val drawer = "androidx.drawerlayout:drawerlayout:${Versions.drawer}"
    private val dynamicAnimation = "androidx.dynamicanimation:dynamicanimation-ktx:${Versions.dynamicAnimation}"
    private val fragmentKtx = "androidx.fragment:fragment-ktx:${Versions.fragmentKtx}"
    private val lifecycle = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}"
    private val material = "com.google.android.material:material:${Versions.material}"
    private val preference = "androidx.preference:preference-ktx:${Versions.preference}"
    private val room = "androidx.room:room-runtime:${Versions.room}"
    private val roomKapt = "androidx.room:room-compiler:${Versions.room}"
    private val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    private val viewPager2 = "androidx.viewpager2:viewpager2:${Versions.viewpager2}"

    // third party
    private val bitmapPool = "com.amitshekhar.android:glide-bitmap-pool:${Versions.bitmapPool}"

    // test libs
    private val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    private val extJUnit = "androidx.test.ext:junit:${Versions.extJunit}"
    private val junit = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.junit}"
    private val kotestAssertions = "io.kotest:kotest-assertions-core-jvm:${Versions.kotest}"
    private val kotestProperty = "io.kotest:kotest-property:${Versions.kotest}"
    private val kotestRunner = "io.kotest:kotest-runner-junit5-jvm:${Versions.kotest}"

    val sharedLibraries = arrayListOf<String>().apply {
        add(kotlinStdLib)
	    add(kotlinCoroutines)
        add(coreKtx)
        add(lifecycle)
        add(appcompat)
    }

    val appLibraries = arrayListOf<String>().apply {
	    add(kotlinReflections)
	    add(dokka)
        add(activity)
        add(material)
        add(constraintLayout)
        add(viewPager2)
        add(room)
        add(roomKtx)
        add(drawer)
        add(preference)
        add(serializationCbor)
    }

    val appKapt = arrayListOf<String>().apply {
        add(roomKapt)
    }

    val libLibraries = arrayListOf<String>().apply {
        add(dynamicAnimation)
        add(bitmapPool)
    }

    val androidTestLibraries = arrayListOf<String>().apply {
        add(extJUnit)
        add(espressoCore)
    }

    val testLibraries = arrayListOf<String>().apply {
        // add(junit)
        add(kotestRunner)
        add(kotestAssertions)
        add(kotestProperty)
    }
}

// util functions for adding the different type dependencies from build.gradle file
fun DependencyHandler.kapt(list: List<String>) {
    list.forEach { dependency ->
        add("kapt", dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add("implementation", dependency)
    }
}

fun DependencyHandler.androidTestImplementation(list: List<String>) {
    list.forEach { dependency ->
        add("androidTestImplementation", dependency)
    }
}

fun DependencyHandler.testImplementation(list: List<String>) {
    list.forEach { dependency ->
        add("testImplementation", dependency)
    }
}
