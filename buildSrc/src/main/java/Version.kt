// https://medium.com/android-dev-hacks/kotlin-dsl-gradle-scripts-in-android-made-easy-b8e2991e2ba

//version constants for the Kotlin DSL dependencies
object Versions {
    // JVM
    const val jvm = "1.8"

    // app level
    const val gradle = "7.3.0"
    const val kotlin = "1.7.20"

    // kotlin libs
    const val kotlinCoroutines = "1.6.4"
    const val kotlinReflections = "1.7.20"
    const val serializationCbor = "1.4.0"
    const val dokka = "1.7.10"

    // android
    const val activity = "1.6.0"
    const val appcompat = "1.5.1"
    const val constraintLayout = "2.1.4"
    const val coreKtx = "1.9.0"
    const val drawer = "1.1.1"
    const val dynamicAnimation = "1.0.0-alpha03"                      // FIXME: do we use this?
    const val fragmentKtx = "1.5.3"
    const val lifecycle = "2.5.1"
    const val material = "1.6.1"
    const val preference = "1.2.0"
    const val room = "2.4.3"
    const val viewpager2 = "1.0.0"

    // third party
    const val bitmapPool = "0.0.1"

    // test
    const val espresso = "3.4.0"
    const val extJunit = "1.1.2"
    const val junit = "1.7.20"
    const val kotest = "5.4.2"
}
