// https://medium.com/android-dev-hacks/kotlin-dsl-gradle-scripts-in-android-made-easy-b8e2991e2ba

//app level config constants
object AppConfig {
    const val compileSdk = 33
    const val minSdk = 21
    const val targetSdk = 33
    const val ndkVersion = "25.1.8937393"
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val buildToolsVersion = "33.0.0"

    const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"
    const val proguardConsumerRules =  "consumer-rules.pro"
    const val dimension = "environment"

    const val lintAbortOnError = false
    const val lintWarningsAsErrors = false
}
